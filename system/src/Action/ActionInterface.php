<?php
/**
 * @file
 * Rural Stack System File.
 * Contains the System\Action\ActionInterface Interface
 *
 * @author Josh Parkinson
 */

namespace System\Action;

/**
 * Interface ActionInterface
 *
 * Defines methods an Action class must implement.
 *
 * @package System\Action
 */
interface ActionInterface {

    /**
     * Get/define the unique key associated to the action.
     *
     * @return string
     */
    public function key();

    /**
     * Execute the action.
     *
     * Should contain all processing/execution functionality.
     *
     * @return bool TRUE if execution successful
     */
    public function execute();

    /**
     * Get the actions message.
     *
     * Usually the success/on complete message.
     *
     * @return string
     */
    public function message();

}