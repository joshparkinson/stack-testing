<?php
/**
 * @file
 * Rural Stack System File.
 * Contains the System\Action\Exception\ActionRunnerNoActionsToRunException
 * Exception
 *
 * @author Josh Parkinson
 */

namespace System\Action\Exception;

use Exception;

/**
 * Class ActionRunnerNoActionsToRunException.
 *
 * Thrown when an action runner tries to run but has had no action objects
 * passed to it.
 *
 * @package System\Action\Exception
 */
class ActionRunnerNoActionsToRunException extends Exception { }