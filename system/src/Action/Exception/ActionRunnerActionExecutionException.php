<?php
/**
 * @file
 * Rural Stack System File.
 * Contains the System\Action\Exception\ActionRunnerActionExecutionException
 * Exception
 *
 * @author Josh Parkinson
 */

namespace System\Action\Exception;

use Exception;

/**
 * Class ActionRunnerActionExecutionException.
 *
 * Thrown when an action runner attempts to execute an action which itself
 * throws an exception.
 *
 * @package System\Action\Exception
 */
class ActionRunnerActionExecutionException extends Exception { }