<?php
/**
 * @file
 * Rural Stack System File.
 * Contains the System\Action\AbstractActionRunner Class
 *
 * @author Josh Parkinson
 */

namespace System\Action;

use Exception;
use System\Action\Exception\ActionRunnerActionExecutionException;
use System\Action\Exception\ActionRunnerNoActionsToRunException;

/**
 * Class AbstractActionRunner.
 *
 * Runs any actions that are added to it.
 *
 * @package System\Action
 */
abstract class AbstractActionRunner implements ActionRunnerInterface {

    /**
     * The actions to run.
     *
     * @var ActionInterface[]
     */
    private $actions;

    /**
     * An array of messages returned by the actions executed within this runner.
     *
     * @var array
     */
    private $messages = array();

    /**
     * The success message to be shown on action runner completion.
     *
     * @var string
     */
    private $successMessage = '';

    /**
     * Add an action to the runner.
     *
     * Store action under its key to avoid storing the same action multiple
     * times.
     *
     * @param ActionInterface $action
     */
    public function addAction(ActionInterface $action){
        $this->actions[$action->key()] = $action;
    }

    /**
     * Add multiple actions to the runner.
     *
     * Check each of the items in the passed array implement the
     * ActionInterface before adding to the runner's actions array.
     *
     * @param ActionInterface[] $actions
     *
     * @throws Exception
     */
    public function addActions(array $actions){
        foreach($actions as $action){
            $this->addAction($action);
        }
    }

    /**
     * Set the success message.
     *
     * @param string $message
     */
    public function setSuccessMessage($message){
        $this->successMessage = $message;
    }

    /**
     * Run the runner. Executes all attached actions.
     *
     * First ensure there are actions to run, if not throw an exception.
     *
     * Loop over all actions, attempting to execute them, catching any
     * exceptions that are thrown and wrapping them in the
     * ActionRunnerActionExecutionException.
     *
     * If all actions are executed successfully, call the onComplete method
     * allowing the extending class to do/add any extra functionality as
     * required.
     *
     * @throws ActionRunnerActionExecutionException
     * @throws ActionRunnerNoActionsToRunException
     */
    public function run(){
        if(empty($this->actions)){
            throw new ActionRunnerNoActionsToRunException('No actions to run.');
        }

        foreach($this->actions as $action){
            try{
                $action->execute();
                $this->messages[] = $action->message();
            }
            catch(Exception $e){
                throw new ActionRunnerActionExecutionException(strtr(
                    ':childRunnerName could not execute action: :actionKey. 
                    Failed with message: :exceptionMessage',
                    array(
                        ':childRunnerName' => get_class($this),
                        ':actionKey' => $action->key(),
                        ':exceptionMessage' => $e->getMessage()
                    )
                ));
            }
        }

        $this->onComplete($this->successMessage, $this->messages);
    }

    /**
     * Functionality to carry out once the action runner has completed
     * running the actions.
     *
     * This method will usually handle the display of sucess message but is
     * not limited to just that.
     *
     * @param string $successMessage The runner success message
     * @param array  $actionMessages An array of messages returned by the
     *                               executed actions
     */
    abstract protected function onComplete($successMessage, array $actionMessages);

}