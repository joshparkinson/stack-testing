<?php
/**
 * @file
 * Rural Stack System File.
 * Contains the System\Action\ActionRunnerInterface Interface
 *
 * @author Josh Parkinson
 */

namespace System\Action;

/**
 * Interface ActionRunnerInterface.
 *
 * Defines methods an action runner must implement.
 *
 * @package System\Action
 */
interface ActionRunnerInterface {

    /**
     * Run the actions associated with this runner.
     *
     * @return mixed
     */
    public function run();

}