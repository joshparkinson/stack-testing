<?php
/**
 * @file
 * Rural Stack System File.
 * Contains the System\Action\AbstractFileAction Class
 *
 * @author Josh Parkinson
 */

namespace System\Action;

use System\FileSystem\FileSystem;

/**
 * Class AbstractFileAction.
 *
 * Abstact action implemention for actions that will maniuplate files within
 * the system.
 *
 * @package System\Action
 */
abstract class AbstractFileAction implements ActionInterface {

    /**
     * The filesystem object.
     *
     * @var FileSystem
     */
    protected $fileSystem;

    /**
     * AbstractFileAction constructor.
     *
     * Set the filesystem to the action object for use in execution
     * functionality.
     *
     * @param FileSystem $fileSystem
     */
    public function __construct(FileSystem $fileSystem){
        $this->fileSystem = $fileSystem;
    }

}