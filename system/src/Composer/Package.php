<?php
/**
 * @file
 * Rural Stack System File.
 * Contains the System\Composer\Package Class
 *
 * @author Josh Parkinson
 */

namespace System\Composer;

use Composer\Composer;
use Composer\DependencyResolver\Operation\InstallOperation;
use Composer\DependencyResolver\Operation\OperationInterface;
use Composer\DependencyResolver\Operation\UpdateOperation;
use Composer\Installer\InstallationManager;
use Composer\Installer\PackageEvent;
use Composer\Package\PackageInterface;
use Composer\Repository\RepositoryManager;
use Composer\Repository\WritableRepositoryInterface;
use Exception;

/**
 * Class Package.
 *
 * Holds static helper methods for Composer packages.
 *
 * @package System\Composer
 */
final class Package{

    /**
     * Get the installation path for a specific package
     *
     * @param PackageEvent     $event   The composer package event
     * @param PackageInterface $package The package
     *
     * @return string The install path
     */
    public static function getInstallPath(PackageEvent $event, PackageInterface $package){
        /* @var InstallationManager $manager */
        $manager = $event->getComposer()->getInstallationManager();
        return $manager->getInstaller($package->getType())->getInstallPath($package);
    }

    /**
     * Get current package from a composer operation.
     *
     * @param OperationInterface $operation The operation
     *
     * @return PackageInterface When passed an unhandled operation
     *
     * @throws Exception
     */
    public static function getPackageFromOperation(OperationInterface $operation){
        switch(get_class($operation)){
            case 'Composer\DependencyResolver\Operation\InstallOperation':
            case 'Composer\DependencyResolver\Operation\UninstallOperation':
                /* @var InstallOperation $operation */
                return $operation->getPackage();
            case 'Composer\DependencyResolver\Operation\UpdateOperation':
                /* @var UpdateOperation $operation */
                return $operation->getTargetPackage();
            default:
                throw new Exception('Error: Unknown Operation');
        }
    }

    /**
     * Get all the Packages attached to a composer project.
     *
     * The following method extracts the localRepository from the Composer
     * class via the respoistoryManager before fetching and returning all the
     * packages from within it.
     *
     * @param Composer $composer The composer object
     *
     * @return array of Packages implementing PackageInterface
     */
    public static function getProjectPackages(Composer $composer){
        /* @var RepositoryManager $repositoryManager */
        /* @var WritableRepositoryInterface $localRepository */
        $repositoryManager = $composer->getRepositoryManager();
        $localRepository = $repositoryManager->getLocalRepository();
        return $localRepository->getCanonicalPackages();
    }

}