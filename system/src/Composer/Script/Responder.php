<?php
/**
 * @file
 * Rural Stack System File.
 * Contains: System\Composer\Script\Responder Class
 *
 * @author Josh Parkinson
 */

namespace System\Composer\Script;

use Composer\Installer\PackageEvent;
use Composer\Script\Event;
use System\Composer\Package;
use System\Composer\Script\Action\DrupalDrupalAction;
use System\Composer\Script\Action\Studio42ElfinderAction;

/**
 * Class Responder.
 *
 * Contains static methods called via composer.json script declarations.
 * Methods should respond to these scripts/calls only, not action them. Can
 * not be instantiated.
 *
 * @see ./composer.json
 *
 * @package System\Composer\Script
 */
final class Responder {

    /**
     * Post Package Install Responder.
     *
     * The following is called after a package is installed within the stack.
     * Within this method based on package parameters (name for example)
     * relevant package actions should be triggered as defined within the
     * System\Composer\Script\Action namespace.
     *
     * @param PackageEvent $packageEvent The composer package event
     */
    public static function postPackageInstall(PackageEvent $packageEvent){
        $package = Package::getPackageFromOperation($packageEvent->getOperation());

        //Studio 42 Elfinder - Drupalise the library
        if($package->getName() == "studio-42/elfinder"){
            Studio42ElfinderAction::drupaliseElfinderLibrary($packageEvent);
        }

    }

    /**
     * Post Package Update Responder.
     *
     * The following is called after a package has been updated within the
     * stack. Within this method based on package parameters (name for
     * example) relevant package actions should be triggered as defined
     * within the System\Composer\Script\Action namespace.
     *
     * @param PackageEvent $packageEvent The composer package event
     */
    public static function postPackageUpdate(PackageEvent $packageEvent){
        $package = Package::getPackageFromOperation($packageEvent->getOperation());

        //Drupal Core Update - Prepare system to rebuild drupal
        if($package->getName() == "drupal/drupal"){
            DrupalDrupalAction::prepareDrupalRebuild($packageEvent);
        }

        //Studio 42 Elfinder - Drupalise the library
        if($package->getName() == "studio-42/elfinder"){
            Studio42ElfinderAction::drupaliseElfinderLibrary($packageEvent);
        }
    }

    /**
     * Post Package Uninstall Responder.
     *
     * The following is called a pack has been uninstalled from the stack.
     * Within this method based on package parameters (name for example)
     * relevant package action shpould be triggered as defined within the
     * System\Composer\Script\Action namespace.
     *
     * @param PackageEvent $packageEvent The composer package event
     */
    public static function postPackageUninstall(PackageEvent $packageEvent){
        $package = Package::getPackageFromOperation($packageEvent->getOperation());

        //Studio 42 Elfinder - Remove drupalised library
        if($package->getName() == "studio-42/elfinder"){
            Studio42ElfinderAction::removeDrupalElfinderLibrary($packageEvent);
        }
    }

    /**
     * Post Update Command Reponder.
     *
     * The following is called when an update command has finished executing
     * against the stack. Within this method based on various requirements we
     * can run the relevant actions defined with the
     * System\Composer\Script\Action namespace.
     *
     * @param Event $event The composer script event
     */
    public static function postUpdate(Event $event){
        //Check if we need to rebuild drupal, if so do it
        if(DrupalDrupalAction::checkDrupalRebuildRequired($event)){
            DrupalDrupalAction::rebuildDrupal($event);
        }
    }

}