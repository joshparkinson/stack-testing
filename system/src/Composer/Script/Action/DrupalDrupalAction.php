<?php
/**
 * @file
 * Rural Stack System File.
 * Contains: System\Composer\Script\Action\DrupalDrupalAction Class
 *
 * @author Josh Parkinson
 */

namespace System\Composer\Script\Action;

use Composer\Installer\PackageEvent;
use Composer\Package\PackageInterface;
use Composer\Package\RootPackageInterface;
use Composer\Script\Event;
use Composer\Util\ProcessExecutor;
use Exception;
use System\Composer\Package;

/**
 * Class DrupalDrupalAction.
 *
 * Contains all static methods used to carry out actions related to the
 * drupal/drupal package.
 *
 * The following methods are called within the Responder class.
 * System\Composer\Script\Responder.
 *
 * @see Responder
 *
 * @package System\Composer\Script\Action
 */
final class DrupalDrupalAction {

    /**
     * The rebuild action terminal output information text.
     *
     * Displayed as an intro to all output related to a rebuild of Drupal.
     *
     * @var string
     */
    private static $drupalRebuildInfoText = '  <info>Rebuild Drupal Core:</info>';

    /**
     * The rebuild key to be set within the project package's extra definition.
     *
     * @var string
     */
    private static $drupalRebuildExtraKey = 'rs_rebuildDrupal';

    /**
     * Prepare Drupal Rebuild Action.
     *
     * The following action is triggered when the drupal/drupal package has
     * been updated. This is caught via a package name check in:
     * \System\Composer\Script\Responder::postPackageUpdate
     *
     * The following method sets an 'extra' definition to the root (project -
     * Rural Stack) package within composer telling the system that after the
     * update command has been finished working we need to rebuild drupal.
     *
     * A drupal rebuild is required after a core update due to package
     * nesting within the rural stack project. Contrib modules etc are stored
     * within drupal core's directory structure and so when the core is
     * updated any 'third-party' files that have not been explicitly defined
     * within the 'extra' 'preserve-paths' project definition will be
     * discarded. The rebuild is used to re-add any of these lost files that
     * are required as part of the stack.
     *
     * @see ./composer.json
     * @see Responder::postPackageUpdate()
     *
     * @param PackageEvent $packageEvent
     */
    public static function prepareDrupalRebuild(PackageEvent $packageEvent){
        $io = $packageEvent->getIO();
        $io->write(self::$drupalRebuildInfoText.' <comment>Preparing</comment>');

        $composer = $packageEvent->getComposer();
        $rootPackage = $composer->getPackage();
        self::setRebuildState($rootPackage, true);

        $io->write(self::$drupalRebuildInfoText.' <comment>Prepared</comment>');
    }

    /**
     * Check Drupal Needs Rebuild.
     *
     * The following action is triggered when the composer update command has
     * finished executing.
     *
     * This method will check that the expected root package extra definition
     * exists and is set to true. If this definition exists and is set to
     * true then the project is in a state where a drupal rebuild is required.
     * This extra definition is set as part of the method:
     * System\Composer\Script\Action\DrupalDrupalAction::prepareDrupalRebuild()
     *
     * A drupal rebuild is required after a core update due to package
     * nesting within the rural stack project. Contrib modules etc are stored
     * within drupal core's directory structure and so when the core is
     * updated any 'third-party' files that have not been explicitly defined
     * within the 'extra' 'preserve-paths' project definition will be
     * discarded. The rebuild is used to re-add any of these lost files that
     * are required as part of the stack.
     *
     * @see Responder::postUpdate()
     * @see DrupalDrupalAction::prepareDrupalRebuild()
     *
     * @param Event $event
     *
     * @return bool Whether or not we need to rebuild Drupal
     */
    public static function checkDrupalRebuildRequired(Event $event){
        $composer = $event->getComposer();
        $rootPackage = $composer->getPackage();
        return self::getRebuildState($rootPackage, false);
    }

    /**
     * Rebuild Drupal Action.
     *
     * The following action is triggered when the composer update command has
     * finished executing and the project is in a state where Drupal needs
     * rebuilding. This state is set if necessary as part of a
     * 'drupal/drupal' post-package-update script actioned by:
     * System\Composer\Script\Action\DrupalDrupalAction::prepareDrupalRebuild()
     * and is checked as part of:
     * System\Composer\Script\Action\DrupalDrupalAction::checkDrupalRebuildRequired()
     * within the responder.
     *
     * A drupal rebuild is required after a core update due to package
     * nesting within the rural stack project. Contrib modules etc are stored
     * within drupal core's directory structure and so when the core is
     * updated any 'third-party' files that have not been explicitly defined
     * within the 'extra' 'preserve-paths' project definition will be
     * discarded. The rebuild is used to re-add any of these lost files that
     * are required as part of the stack.
     *
     * The following method will gather all drupal related packages excluding
     * the drupal core package (drupal/drupal) and run an update command for
     * each of these. This will readd the lost 'third-party' files to the
     * drupal core directory structure.
     *
     * Note: The drupal rebuild has to be dont at the end of an update
     * command but initialised after the package update action has triggered
     * to ensure there are no errors surrounding composer lock files and
     * dependencies that can not be met. This is due to the fact (assumption)
     * composer lock files arent updated until update comannds are completed
     * and all previous updates etc have been registered. A few infinite
     * loops we're encountered in the development of the drupal rebuilder.
     *
     * @see ./composer.json
     * @see Responder::postUpdate()
     * @see DrupalDrupalAction::prepareDrupalRebuild()
     * @see DrupalDrupalAction::checkDrupalRebuildRequired()
     *
     * @param Event $packageEvent
     */
    public static function rebuildDrupal(Event $packageEvent){
        $composer = $packageEvent->getComposer();
        $rootPackage = $composer->getPackage();
        self::setRebuildState($rootPackage, false);

        $drupalPackages = array();
        $projectPackages = Package::getProjectPackages($composer);
        foreach($projectPackages as $package){
            /* @var PackageInterface $package */
            if($package->getName() == 'drupal/drupal'){
                continue;
            }
            if(strpos($package->getName(), 'drupal') !== FALSE){
                $drupalPackages[] = $package->getName();
                continue;
            }
            if(strpos($package->getType(), 'drupal') !== FALSE){
                $drupalPackages[] = $package->getName();
                continue;
            }
        }

        if(!empty($drupalPackages)){
            $io = $packageEvent->getIO();
            $executor = new ProcessExecutor($io);

            $updateCommand = 'composer update '.implode(' ', $drupalPackages).' --quiet';

            $io->write(self::$drupalRebuildInfoText.' <comment>Started</comment>');
            $executor->execute($updateCommand);
            $io->write(self::$drupalRebuildInfoText.' <comment>Completed</comment>');
        }
    }

    /**
     * Set the drupal rebuild state against the root package.
     *
     * The following method is used to set the drupal rebuild state against
     * the root package. The rebuild state can be set to either true or false
     * with an exception being thrown if any variable type apart from boolean
     * is passed as a $value parameter.
     *
     * The status boolean is set against the 'extra' definition within the
     * root package under the set $drupalRebuildExtraKey definition property.
     *
     * @param RootPackageInterface $rootPackage The root composer package
     *                                          (project)
     * @param bool                 $value       The status boolean value
     *
     * @throws Exception
     */
    private static function setRebuildState(RootPackageInterface $rootPackage, $value){
        if(!is_bool($value)){
            throw new Exception('Invalid Drupal Rebuild State Recieved: '.$value);
        }
        $rootPackage->setExtra(array_merge(
            $rootPackage->getExtra(),
            array(self::$drupalRebuildExtraKey => $value)
        ));
    }

    /**
     * Get the drupal rebuild state from the root package.
     *
     * The following method is used to get the drupal rebuild state from the
     * root package extra definition (as set within the setRebuildState
     * method). The method will search for the setting under the
     * $drupalRebuildExtra key and return a passed default if it not found.
     *
     * @param RootPackageInterface $rootPackage The root composer package
     *                                          (project)
     * @param mixed $default                    The default to return if not
     *                                          found
     *
     * @return bool|mixed
     */
    private static function getRebuildState(RootPackageInterface $rootPackage, $default = false){
        $rootPackageExtraDefinition = $rootPackage->getExtra();
        return (!empty($rootPackageExtraDefinition[self::$drupalRebuildExtraKey])) ? $rootPackageExtraDefinition[self::$drupalRebuildExtraKey] : $default;
    }

}