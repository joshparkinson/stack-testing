<?php
/**
 * @file
 * Rural Stack System File.
 * Contains: System\Composer\Script\Action\Studio42ElfinderAction Class
 *
 * @author Josh Parkinson
 */

namespace System\Composer\Script\Action;

use Composer\Installer\PackageEvent;
use Composer\Util\ProcessExecutor;
use System\Composer\Package;

/**
 * Class Studio42ElfinderAction.
 *
 * Contains all static methods used to carry out actions related to the
 * studio-42/elfinder package.
 *
 * The following methods are called within the Responder class.
 * System\Composer\Script\Responder
 *
 * @see Responder
 *
 * @package System\Composer\Script\Action
 */
final class Studio42ElfinderAction{

    /**
     * What the elfinder library directory should be named.
     *
     * This should match the expected/searched for name in the elfinder
     * drupal module. Do not include leading or trailing '/'
     *
     * @var string
     */
    private static $directoryName = 'elfinder';

    /**
     * The install location for drupal libraries.
     *
     * Path defined from the stack's root directory. Do not include trailing '/'
     *
     * @var string
     */
    private static $drupalLibraryDirectory = 'public/sites/all/libraries';

    /**
     * Drupalise Elfinder Library Action.
     *
     * The following action is triggered when the studio-42/elfinder package
     * has been installed or updated. This is caught via a package name check
     * in the following responder methods:
     * \System\Composer\Script\Responder::postPackageInstall()
     * \System\Composer\Script\Responder::postPackageUpdate()
     *
     * The following action will obtain the package (in this case
     * studio-42/elfinder) from the composer PackageEvent before extracting
     * it's install location with the use of the System class:
     * \System\Composer\Package. After obtaining this information the
     * composer input/output helper and command executor are used to display
     * status messages and execute commands to achieve the following:
     * - Remove any previous elfinder library files
     * - Move the newly downloaded library into drupal's core
     * - Santise the moved library to remove files posing security risks.
     *
     * @see ./composer.json
     * @see Responder::postPackageInstall()
     * @see Responder::postPackageUpdate()
     *
     * @param PackageEvent $packageEvent
     */
    public static function drupaliseElfinderLibrary(PackageEvent $packageEvent){
        $package = Package::getPackageFromOperation($packageEvent->getOperation());
        $installPath = Package::getInstallPath($packageEvent, $package);

        $io = $packageEvent->getIO();
        $executor = new ProcessExecutor($io);

        $io->write('  <info>Drupalise studio-42/elfinder:</info> <comment>Started</comment>');
        self::commandRemoveDrupalElfinderLibrary($executor);
        self::commandMoveElfinderLibraryToDrupal($installPath, $executor);
        self::commandSanitiseDrupalElfinderLibrary($executor);
        $io->write('  <info>Drupalise studio-42/elfinder:</info> <comment>Completed</comment>');
    }

    /**
     * Remove Drupal Elfinder Library Action.
     *
     * The following action is triggered when the studio-42/elfinder package
     * has been removed from the Rural Stack. This is caught via a package
     * name check in the following responder methods:
     * \System\Composer\Script\Responder::postPackageUninstall()
     *
     * The following method will instantiate the composer input/output helper
     * and process executor before display status messages and running a
     * command to remove the elfinder library from the drupal core.
     *
     * @param PackageEvent $packageEvent
     */
    public static function removeDrupalElfinderLibrary(PackageEvent $packageEvent){
        $io = $packageEvent->getIO();
        $executor = new ProcessExecutor($io);

        $io->write('  <info>Cleanup Drupalised studio-42/elfinder:</info> <comment>Started</comment>');
        self::commandRemoveDrupalElfinderLibrary($executor);
        $io->write('  <info>Cleanup Drupalised studio-42/elfinder:</info> <comment>Completed</comment>');
    }

    /**
     * Studio42ElfinderAction Helper Method.
     *
     * The following method is used to return the installation path for the
     * elfinder library within a drupal context.
     *
     * The value returned here is dependent on the two private properties
     * stored within this class:
     * - $drupalLibraryDirectory
     * - $directoryName
     *
     * @return string The path to install the elfinder library in
     */
    private static function getDrupalElfinderLibraryInstallPath(){
        return self::$drupalLibraryDirectory.'/'.self::$directoryName;
    }

    /**
     * Studio42ElfinderAction Command Method.
     *
     * The following method is used to fire the terminal command which copies
     * the composer packaged elfinder library to the drupal library install
     * path.
     *
     * @param string          $installPath The location of the packaged
     *                                     elfinder library
     * @param ProcessExecutor $executor    The ProcessExecutor used to execute
     *                                     the command.
     */
    private static function commandMoveElfinderLibraryToDrupal($installPath, ProcessExecutor $executor){
        $executor->execute(strtr('cp -a :installPath :drupalLibraryPath', array(
            ':installPath' => $installPath,
            ':drupalLibraryPath' => self::getDrupalElfinderLibraryInstallPath()
        )));
    }

    /**
     * Studio42ElfinderAction Command Method.
     *
     * The following method is used to fire the terminal command which will
     * remove the 'drupalised' elfinder library from the drupal library
     * install path
     *
     * @param ProcessExecutor $executor The ProcessExecutor used to execute
     *                                  the command.
     */
    private static function commandRemoveDrupalElfinderLibrary(ProcessExecutor $executor){
        $executor->execute(strtr('rm -rf :drupalLibraryPath', array(
            ':drupalLibraryPath' => self::getDrupalElfinderLibraryInstallPath()
        )));
    }

    /**
     * Studio42ElfinderAction Command Method.
     *
     * The following method is used to cleanup a raw packaged elfinder
     * library so that it is Drupal safe. This is done by defining an array
     * of files that are deemed to pose a security risk when inside a Drupal
     * environment. These files are defined as relative paths from the
     * libraries base directory. Each one of the files defined is looped over
     * an a remove file command is executed to delete from the Drupalised
     * library. The -f option is used so that no errors are thrown if the
     * specified file does not exist (if studio-42 change names/locations of
     * security risk files for example).
     *
     * @param ProcessExecutor $executor The ProcessExecutor used to execute
     *                                  the command.
     */
    private static function commandSanitiseDrupalElfinderLibrary(ProcessExecutor $executor){
        $unsafeLibraryFiles = array(
            'elfinder.html',
            'elfinder.php.html',
            'connectors/php/connector.php',
            'php/connector.php',
            'php/connector.php-dist',
            'php/connector.minimal.php',
            'php/connector.minimal.php-dist'
        );

        foreach($unsafeLibraryFiles as $file){
            $executor->execute(strtr('rm -f :filePath', array(
                ':filePath' => self::getDrupalElfinderLibraryInstallPath().'/'.$file
            )));
        }
    }

}