<?php
/**
 * @file
 * Rural Stack System File.
 * Contains the System\FileSystem\FileSystem Class.
 *
 * @author Josh Parkinson
 */

namespace System\FileSystem;

use FilesystemIterator;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Filesystem\Filesystem as SymfonyFilesystem;

/**
 * Class FileSystem.
 *
 * Provides a wrapper for the Symfony Filesystem component with extra
 * functionality.
 *
 * @package System\FileSystem
 */
class FileSystem extends SymfonyFilesystem {

    /**
     * Get the CHMOD value for a file or directory.
     *
     * First check the passed file path is valie (ie the file or directory
     * exists), if not throw an exception. If the file does exists returns
     * its CHMOD (octal) value.
     *
     * @param string $filePath File/directory path to get the CHMOD for
     *
     * @return string The CHMOD value (octal)
     */
    public function getChmod($filePath){
        if(!$this->exists($filePath)){
            throw new FileNotFoundException(strtr(
                'Error getting CHMOD value, no file found at: :filePath',
                array(':filePath' => $filePath)
            ));
        }

        return octdec(substr(sprintf('%o', fileperms($filePath)), -4));
    }

    /**
     * Check if a Directory is empty.
     *
     * Using the path to directory attempt to create a FilesystemIterator and
     * check it is valid. This call will return FALSE if the directory is empty
     * so this return value is inversed (!) to return TRUE if the directory
     * is in face empty.
     *
     * @param string $directoryPath The path to the directory to check
     *
     * @return bool Whether the directory is empty or not
     */
    public function directoryIsEmpty($directoryPath){
        return !(new FilesystemIterator($directoryPath))->valid();
    }

}