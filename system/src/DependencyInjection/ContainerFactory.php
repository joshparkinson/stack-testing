<?php
/**
 * @file
 * Rural Stack System File.
 * Contains the System\DependencyInjection\ContainerFactory Class.
 *
 * @author Josh Parkinson
 */

namespace System\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class ContainerFactory {

    public static function build(){
        $container = new ContainerBuilder();
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../../config'));
        $loader->load('services.yml');
        return $container;
    }

}