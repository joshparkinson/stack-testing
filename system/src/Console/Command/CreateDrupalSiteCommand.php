<?php
/**
 * @file
 * Rural Stack System File.
 * Contains the System\Console\Command\CreateDrupalSiteCommand Class
 *
 * @author Josh Parkinson
 */

namespace System\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class CreateDrupalSiteCommand.
 *
 * Contains methods required to execute the create drupal site command. The
 * command is used to create the directory for a new drupal site within the
 * Rural stack multi site architecture.
 *
 * @package System\Console\Command
 */
class CreateDrupalSiteCommand extends Command {

    /**
     * The Symfony FileSystem object.
     *
     * Used to execute file system commands required to build a Drupal site.
     *
     * @var \Symfony\Component\Filesystem\Filesystem
     */
    private $fileSystem;

    /**
     * The Stack root path.
     *
     * @var string
     */
    private $stackRoot;

    /**
     * The Drupal web root path.
     *
     * @var string
     */
    private $webRoot;

    /**
     * The name of the Drupal web root directory.
     *
     * @var string
     */
    private $webDirectory;

    /**
     * The composer.json file location from the Stack root.
     *
     * Do not include leading/trailing slashes.
     *
     * @var string
     */
    private $composerJsonLocation = 'composer.json';

    /**
     * The composer.json object extra definition key.
     *
     * @var string
     */
    private $composerExtraKey = 'extra';

    /**
     * The composer.json object preserve paths definition key.
     *
     * @var string
     */
    private $composerPreservePathsKey = 'preserve-paths';

    /**
     * The default.settings.php file location from the Drupal web root.
     *
     * Do not include leading/trailing slashes.
     *
     * @var string
     */
    private $drupalDefaultSettingsLocation = 'sites/default/default.settings.php';

    /**
     * The Drupal sites directory location from the Drupal web root.
     *
     * Do not include leading/trailing slashes.
     *
     * @var string
     */
    private $drupalSitesDirectoryLocation = 'sites';

    /**
     * The Drupal sites.php file location from the Drupal web root.
     *
     * Do not include leading/trailing slashes.
     *
     * @var string
     */
    private $drupalSitesFileLocation = 'sites/sites.php';

    /**
     * CreateDrupalSiteCommand constructor.
     *
     * Send the passed name to to the parent Command class. Initialise the
     * require site building command's properties.
     *
     * @param string     $name         The name of the command (used to execute it from terminal)
     * @param string     $stackRoot    The stack root path
     * @param string     $webRoot      The drupal web root path
     * @param string     $webDirectory The name of the web root directory
     * @param Filesystem $fileSystem   The Symfony FileSystem object
     */
    public function __construct($name, $stackRoot, $webRoot, $webDirectory, Filesystem $fileSystem){
        parent::__construct($name);
        $this->fileSystem = $fileSystem;
        $this->stackRoot = rtrim($stackRoot, '/');
        $this->webRoot = rtrim($webRoot, '/');
        $this->webDirectory = trim($webDirectory, '/');
    }

    /**
     * Configure the CreateDrupalSiteCommand command.
     *
     * Sets the description and the help text for the command.
     */
    protected function configure(){
        $this->setAliases(array('cds'));
        $this->setDescription('Creates a ready to install/configure drupal site directory boilerplate.');
        $this->setHelp('This command allows you to create a drupal site directory within the core.');
    }

    /**
     * Execute the CreateDrupalSiteCommand command.
     *
     * The following method is used to interact with the user to take in
     * information about the site we are creating and use the data provided
     * to create the new site directory structure with the help of the local
     * FileSystem object. The SymfonyStyle IO class is used to create the
     * terminal output object.
     *
     * Command Introduction:
     * Create the terminal commands introduction section outlining to the
     * user what this command will do once completed.
     *
     * Directory/Files Configuration:
     * Create the Directory/Files confingurtion section which is used as a
     * checker and setting for the drupal site files. First display the
     * assumed defaults to the user asking them to confirm whether or not
     * these defaults are correct for their stack directory structure if they
     * are then begin proceed to the next section. If not we require input on
     * where these files are actually located, on data input for these paths
     * we check whether the files exists at the location specified, if not,
     * we show an error and re-ask for the data. The check and re-ask loop
     * uses the private method: outputAndCheckPathSettings(). After recieving
     * all of the paths one final check is done to ensure they all exists
     * against the path specified. If they do proceed, if not kick user out
     * of the command and require re-calling of it.
     *
     * Site Configuration:
     * First include the Drupal sites.php file so that the current $sites
     * definition array can be used for validation checks again the site
     * information provided by the user. Ask the user for the name of the
     * directory this site will site under, on user input we check within the
     * (previously configured) drupal sites directory that a directory with
     * the name provided doesnt already exists. If it does throw an error. If
     * not we're good, move onto the next question. Ask for the URL that
     * should be used to access the site being created, on data input check
     * the $sites array from the included sites.php file and make sure that
     * key (url) doesn't already exist within it, it does throw error and ask
     * again. After getting an answer to this move onto the site building
     * commands
     *
     * Creating New Site:
     * - Create the new site directory with the provided site name
     * - Create the new site's files directory, chmod to 0777
     * - Create the new site's settings.php file, chmod to 077
     * - Add a new entry to Drupal's sites.php file for this site
     *   Example:
     *     $siteName       = 'test.site.co.uk';
     *     $siteUrl        = 'test.site.dev';
     *     Sites.php Entry = $sites['test.site.dev'] = 'test.site.co.uk';
     * - Add a new entry to composer.json to preserve site paths (if possible)
     *
     * Command execution complete.
     *
     * @see outputAndCheckPathSettings()
     *
     * @param InputInterface   $input
     * @param OutputInterface $output
     *
     * @return bool Did the command execute successfully.
     */
    protected function execute(InputInterface $input, OutputInterface $output){
        $io = new SymfonyStyle($input, $output);

        //Show command introduction
        $io->title('Drupal Site Creator');
        $io->text('This tool will automatically:');
        $io->listing(array(
            /** @lang text */ 'Create the site directory',
            /** @lang text */ 'Automatically add and chmod the <info>settings.php</info> file',
            /** @lang text */ 'Automatically add and chmod the <info>files</info> directory',
            /** @lang text */ 'Add the sites entry to the global <info>sites.php</info> file',
            /** @lang text */ 'Update <info>composer.json</info> to preserve new site paths'
        ));

        //Show directory/files configuration section
        $io->section('Directory/Files Configuration');

        //Loop until dependent drupal files paths are confirmed.
        $installPathsConfirmed = false;
        while($installPathsConfirmed == false){
            $installPathsConfirmed = $this->outputAndCheckPathSettings($io);
        }

        //Final check to ensure all dependent files exist at specified location
        if(!$this->fileSystem->exists(array($this->getDrupalDefaultSettingsLocation(), $this->getDrupalSitesDirectoryLocation(), $this->getDrupalSitesFileLocation()))){
            $io->error(array(
                'One of more of the defined files could not be found at the path provided.',
                'Please check the paths provided are correct.',
                'Has the drupal core package been installed?'
            ));
            return false;
        }

        //Show site configuration section
        $io->section('Site Configuration');

        /** @noinspection PhpIncludeInspection */
        include($this->getDrupalSitesFileLocation());

        //Get site name, throw error if dir with name already exists
        $siteName = $io->ask('Site (directory) name', NULL, function($siteName){
            if($this->fileSystem->exists($this->getDrupalSitesDirectoryLocation().'/'.$siteName)){
                throw new \RuntimeException('Site directory already exists with name: '.$siteName);
            }
            return $siteName;
        });

        //Get site url, throw error is exists within sites.php $sites array already
        $siteUrl = $io->ask('Site URL', NULL, function($siteUrl) use ($sites){
            if(!empty($sites[$siteUrl])){
                throw new \RuntimeException('Site URL already exists for site in sites.php');
            }
            return $siteUrl;
        });

        //Build drupal site
        $io->section('Creating New Site');

        //Define new site locations
        $newSiteDirectory = $this->getDrupalSitesDirectoryLocation().'/'.$siteName;
        $newSiteFilesDirectory = $newSiteDirectory.'/files';
        $newSiteSettingsFile = $newSiteDirectory.'/settings.php';

        //Make new site directory
        $this->fileSystem->mkdir($newSiteDirectory);

        //Make new site's files directory, chmod
        $this->fileSystem->mkdir($newSiteFilesDirectory);
        $this->fileSystem->chmod($newSiteFilesDirectory, 0777);

        //Make new sites settings.php file, chmod
        $this->fileSystem->copy($this->getDrupalDefaultSettingsLocation(), $newSiteSettingsFile);
        $this->fileSystem->chmod($newSiteSettingsFile, 0777);

        //Add entry for new site to sites.php file
        $siteArrayDefinitionString = PHP_EOL.'$sites[\''.$siteUrl.'\'] = \''.$siteName.'\';';
        file_put_contents($this->getDrupalSitesFileLocation(), $siteArrayDefinitionString, FILE_APPEND | LOCK_EX);

        //Update composer.json file
        $composerJson = json_decode(file_get_contents($this->getComposerJsonLocation()));
        if(isset($composerJson->{$this->composerExtraKey}->{$this->composerPreservePathsKey}) && is_array($composerJson->{$this->composerExtraKey}->{$this->composerPreservePathsKey})){
            $composerJson->{$this->composerExtraKey}->{$this->composerPreservePathsKey}[] = $this->webDirectory.'/'.$this->getDrupalSitesDirectoryLocation(false).'/'.$siteName;
            $this->fileSystem->remove($this->getComposerJsonLocation());
            $this->fileSystem->dumpFile($this->getComposerJsonLocation(), json_encode($composerJson, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
        }

        $io->success(array(
            'DONE - Drupal Site Created Successfully.',
            'If required create a blank database for the site and install at: '.$siteUrl
        ));

        return true;
    }

    /**
     * CreateDrupalSiteCommand command execution helper.
     *
     * The following method is used as part of the execution method to check
     * and verify the dependent Drupal file locations. The files/directory
     * locations checked are:
     * - The default.settings.php file
     * - The sites.php file
     * - The sites driectory
     *
     * Firstly output the currently stored locations for these files to the
     * user before asking them to confirm whether or not these locations are
     * correct or not. If the locations are not correct we then ask them to
     * input the locations of each of these dependent files. On entry of each
     * of these location we check that they exist within the stacks directory
     * structure, if they dont throw an error and make the user re-enter the
     * value untill its found. Once all values have been entered and
     * validated we run this method to show the confirmation text and ask
     * them to confirm the values entered. Until they confirm they will be
     * stuck in this loop of checking and entering values. Once confirmed we
     * return true to the execution method so it can break out of it's 'pause'
     * on this section.
     *
     * @see execute() - Directory/Files Configuration section.
     *
     * @param SymfonyStyle $io
     *
     * @return bool
     */
    private function outputAndCheckPathSettings(SymfonyStyle $io){
        $io->text('<comment>composer.json location:</comment> '.$this->getComposerJsonLocation());
        $io->text('<comment>Drupal sites direction location:</comment> '.$this->getDrupalSitesDirectoryLocation());
        $io->text('<comment>Drupal sites.php location:</comment> '.$this->getDrupalSitesFileLocation());
        $io->text('<comment>Drupal default.settings.php location:</comment> '.$this->getDrupalDefaultSettingsLocation());

        $io->newLine();

        if(!$io->confirm('Are the above paths correct?')){
            $io->text('<comment>Reconfigure System Paths</comment>');
            $io->text('<comment>Set paths from the base: </comment>'.$this->stackRoot.'/');

            $this->setComposerJsonLocation($io->ask('composer.json file location', NULL, function($location){
                if(!$this->fileSystem->exists($this->stackRoot.'/'.$location)){
                    throw new \RuntimeException(
                        'Could not find composer.json directory at provided location. Path provided: '.$this->stackRoot.'/'.$location
                    );
                }
                return $location;
            }));

            $io->text('<comment>Reconfigure Drupal Paths</comment>');
            $io->text('<comment>Set paths from the base: </comment>'.$this->webRoot.'/');

            $this->setDrupalSitesDirectoryLocation($io->ask('Drupal sites directory location', NULL, function($location){
                if(!$this->fileSystem->exists($this->webRoot.'/'.$location)){
                    throw new \RuntimeException(
                        'Could not find sites directory at provided location. Have you installed drupal core? Path provided: '.$this->webRoot.'/'.$location
                    );
                }
                return $location;
            }));

            $this->setDrupalSitesFileLocation($io->ask('Drupal sites.php location', NULL, function($location){
                if(!$this->fileSystem->exists($this->webRoot.'/'.$location)){
                    throw new \RuntimeException('Could not find sites.php at provided location. Have you installed drupal core? Path provided: '.$this->webRoot.'/'.$location);
                }
                return $location;
            }));

            $this->setDrupalDefaultSettingsLocation($io->ask('Drupal default.settings.php location', NULL, function($location){
                if(!$this->fileSystem->exists($this->webRoot.'/'.$location)){
                    throw new \RuntimeException('Could not find default.settings.php at provided location. Have you installed drupal core? Path provided: '.$this->webRoot.'/'.$location);
                }
                return $location;
            }));

            $this->outputAndCheckPathSettings($io);
        }

        return true;
    }

    /**
     * Get the composer.json file location.
     *
     * Can return the full (absolute) path or the path relative to the Stack
     * root depending on the value of the $fullPath parameter passed.
     * Defaults to return the full (absolute) path,
     *
     * @param bool $fullPath Whether to return the full (absolute) path or not.
     *
     * @return string The path to the composer.json file
     */
    private function getComposerJsonLocation($fullPath = true){
        return ($fullPath) ? $this->stackRoot.'/'.$this->composerJsonLocation : $this->composerJsonLocation;
    }

    /**
     * Set the composer.json location.
     *
     * @param string $location The composer.json file location
     *
     * @return $this
     */
    private function setComposerJsonLocation($location){
        $this->composerJsonLocation = $location;
        return $this;
    }

    /**
     * Get the Drupal default.settings.php location.
     *
     * Can return the full (absolute) path or the path relative to the Drupal
     * web root depending on the value of the $fullPath parameter passed.
     * Defaults to return the full (absolute) path.
     *
     * @param bool $fullPath Whether to return the full (absolute) path or not.
     *
     * @return string The path to the default.settings.php
     */
    private function getDrupalDefaultSettingsLocation($fullPath = true){
        return ($fullPath) ? $this->webRoot.'/'.$this->drupalDefaultSettingsLocation : $this->drupalDefaultSettingsLocation;
    }

    /**
     * Set the Drupal default.settings.php location.
     *
     * @param string $location The default.settings.php location
     *
     * @return $this
     */
    private function setDrupalDefaultSettingsLocation($location){
        $this->drupalDefaultSettingsLocation = $location;
        return $this;
    }

    /**
     * Get the Drupal sites directory location.
     *
     * Can return the full (absolute) path or the path relative to the Drupal
     * web root depending on the value of the $fullPath parameter passed.
     * Defaults to return the full (absolute) path.
     *
     * @param bool $fullPath Whether to return the full (absolute) path or not.
     *
     * @return string The path to the Drupal sites directory
     */
    private function getDrupalSitesDirectoryLocation($fullPath = true){
        return ($fullPath) ? $this->webRoot.'/'.$this->drupalSitesDirectoryLocation : $this->drupalSitesDirectoryLocation;
    }

    /**
     * Set the Drupal sites directory location.
     *
     * @param string $location The Drupal sites directory location
     *
     * @return $this
     */
    private function setDrupalSitesDirectoryLocation($location){
        $this->drupalSitesDirectoryLocation = rtrim($location, '/');
        return $this;
    }

    /**
     * Get the Drupal sites.php file location.
     *
     * Can return the full (absolute) path or the path relative to the Drupal
     * web root depending on the value of the $fullPath parameter passed.
     * Defaults to return the full (absolute) path.
     *
     * @param bool $fullPath Whether to return the full (absolute) path or not.
     *
     * @return string The path to the Drupal sites.php file
     */
    private function getDrupalSitesFileLocation($fullPath = true){
        return ($fullPath) ? $this->webRoot.'/'.$this->drupalSitesFileLocation : $this->drupalSitesFileLocation;
    }

    /**
     * Set the Drupals sites.php file location.
     *
     * @param string $location  The Drupals sites.php file location
     *
     * @return $this
     */
    private function setDrupalSitesFileLocation($location){
        $this->drupalSitesFileLocation = $location;
        return $this;
    }

}