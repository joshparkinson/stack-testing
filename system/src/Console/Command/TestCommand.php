<?php
/**
 * Created by PhpStorm.
 * User: joshparkinson
 * Date: 10/01/2017
 * Time: 10:40
 */

namespace System\Console\Command;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;

class TestCommand extends Command {

    private $stackRoot;

    private $fileSystem;

    public function __construct($name, $stackRoot, Filesystem $fileSystem){
        parent::__construct($name);
        $this->stackRoot = rtrim($stackRoot, '/');
        $this->fileSystem = $fileSystem;
    }

    /**
     * Configure the CreateDrupalSiteCommand command.
     *
     * Sets the description and the help text for the command.
     */
    protected function configure(){
        $this->setDescription('Creates a ready to install/configure drupal site directory boilerplate.');
        $this->setHelp('This command allows you to create a drupal site directory within the core.');
    }

    protected function execute(InputInterface $input, OutputInterface $output){
        $io = new SymfonyStyle($input, $output);

        $io->text('bingo');

        $composerJson = json_decode(file_get_contents($this->stackRoot.'/composer.json'));
        //$composerJson->extra->{'preserve-paths'}[] = 'path/to/add';

        if(isset($composerJson->extra->{'preserve-paths'})){
            $io->text('exists');
        }

        //$this->fileSystem->rename($this->stackRoot.'/composer.json', $this->stackRoot.'/composer.json.orig', TRUE);
        //$this->fileSystem->dumpFile($this->stackRoot.'/composer.json', json_encode($composerJson, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));




        //$io->text(print_r($composerJson));

    }

}