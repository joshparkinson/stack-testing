<?php
/**
 * @file
 * Rural Stack System File.
 * Contains the System\Database\Tools\DatabaseUrlGeneratorInterface Interface
 *
 * @author Josh Parkinson
 */

namespace System\Database\Tools;

/**
 * Interface DatabaseUrlGeneratorInterface
 *
 * Defines methods a Database URL Generator must implement.
 *
 * @package System\Database\Tools
 */
interface DatabaseUrlGeneratorInterface {

    /**
     * Generate the database url.
     *
     * @return string
     */
    public function generateUrl();

}