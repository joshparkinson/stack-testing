<?php
/**
 * @file
 * Rural Stack System File.
 * Contains the System\Config\NestedConfigInterface Interface
 *
 * @author Josh Parkinson
 */

namespace System\Config;

/**
 * Interface NestedConfigInterface
 *
 * Defines required methods for accessing data from a nested config object.
 *
 * @package System\Config
 */
interface NestedConfigInterface {

    /**
     * Get a nested config value using its key string.
     *
     * A default value can be passed to the method which will be returned if
     * the value with the specified key could not be found.
     *
     * @param string $keyString          The config value key string
     * @param string $keyStringSeperator The key string seperator (seperate nests)
     * @param bool   $default            The default value to return if not found
     *
     * @return mixed
     */
    public function getNestedValue($keyString, $keyStringSeperator = '.', $default = FALSE);

}