<?php
/**
 * @file
 * Rural Stack System File.
 * Contains the System\Config\ConfigInterface Interface
 *
 * @author Josh Parkinson
 */

namespace System\Config;

/**
 * Interface ConfigInterface.
 *
 * Defines required methods for accessing data from a config object.
 *
 * @package System\Config
 */
interface ConfigInterface {

    /**
     * Get a config value using its key.
     *
     * A default value can be passed to the method which will be returned if
     * the value with the specified key could not be found.
     *
     * @param string $key     The config value key
     * @param bool   $default The default value to return if not found
     *
     * @return mixed
     */
    public function getValue($key, $default = FALSE);

    /**
     * Get all config values stored within the config object.
     *
     * Usually returns an array of key'd config data.
     *
     * @return mixed
     */
    public function getAllValues();

}