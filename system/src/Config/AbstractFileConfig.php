<?php
/**
 * @file
 * Rural Stack System File.
 * Contains the System\Config\AbstractFileConfig Class
 *
 * @author Josh Parkinson
 */

namespace System\Config;

use Symfony\Component\Config\Loader\LoaderInterface;
use System\Config\Exception\FileConfigConstructionException;
use System\Utility\Filter\ArrayFilter;

/**
 * Class AbstractFileConfig.
 *
 * Base file configuration class. Allows configuration data to be loaded
 * using a path to the file as provided by the extending class that the
 * configuration data is contained within. Should be extended by System
 * configuration handlers to provide context around configuration files and
 * to validate the recieved resource.
 *
 * @package System\Config
 */
abstract class AbstractFileConfig extends AbstractConfig {

    /**
     * AbstractFileConfig constructor.
     *
     * Loads a configuration file as provided by the extending class which is
     * then passed to the parent configuration handler for further processing
     * and configuration data validation etc.
     *
     * An exception is thrown if the extending class provides an invalid
     * resource file path. (ie no file is found at the recieved location)
     *
     * @param LoaderInterface $fileLoader The file loader
     * @param ArrayFilter $arrayFilter The array filter
     *
     * @throws FileConfigConstructionException
     */
    public function __construct(LoaderInterface $fileLoader, ArrayFilter $arrayFilter){
        $resourceFilePath = $this->getResourceFilePath();

        if(!file_exists($resourceFilePath)){
            throw new FileConfigConstructionException(strtr(
                'Could not create configuration object from :className class 
                as the received configuration resource could not be found at 
                path: :resourceFilePath',
                array(
                    ':className' => get_class($this),
                    ':resourceFilePath' => $resourceFilePath
                )
            ));
        }

        parent::__construct($fileLoader->load($resourceFilePath), $arrayFilter);
    }

    /**
     * Get the resource file path from the extending configuration class.
     *
     * @return string
     */
    abstract protected function getResourceFilePath();

}
