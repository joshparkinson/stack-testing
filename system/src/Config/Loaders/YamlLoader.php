<?php
/**
 * @file
 * Rural Stack System File.
 * Contains the System\Config\Loaders\YamlLoader Class
 *
 * @author Josh Parkinson
 */

namespace System\Config\Loaders;

use Symfony\Component\Config\Loader\FileLoader;
use Symfony\Component\Yaml\Yaml;

/**
 * Class YamlLoader.
 *
 * Loads and parses supplied YAML config files.
 *
 * @package System\Config\Loaders
 */
class YamlLoader extends FileLoader{

    /**
     * Parse a YAML file to a PHP array.
     *
     * @param mixed $resource The yaml config resource
     * @param null  $type     Ignored
     *
     * @return array The parsed YAML config
     */
    public function load($resource, $type = NULL){
        return Yaml::parse(file_get_contents($resource));
    }

    /**
     * Check this loader supports the passed resource.
     *
     * This method is leveraged by parent classes (delegators) to determine
     * whether this loader should be used to parse the provided config
     * resource file.
     *
     * @param mixed $resource The config resource
     * @param null  $type     Ignored
     *
     * @return bool Does this loader support the received resource.
     */
    public function supports($resource, $type = NULL){
        return is_string($resource) && 'yml' === pathinfo(
            $resource,
            PATHINFO_EXTENSION
        );
    }


}