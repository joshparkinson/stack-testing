<?php
/**
 * @file
 * Rural Stack System File.
 * Contains the System\Config\AbstractConfig Class
 *
 * @author Josh Parkinson
 */

namespace System\Config;

use Exception;
use System\Config\Exception\ConfigValidationException;
use System\Utility\Filter\ArrayFilter;
use System\Utility\Filter\Exception\ArrayFilterCouldNotExtractNestedValueException;

/**
 * Class AbstractConfig.
 *
 * Base configuration class. Should be extended by System configuration
 * handlers to provide context around configuration files and to validate the
 * recieved resource.
 *
 * @package System\Config
 */
abstract class AbstractConfig implements ConfigInterface, NestedConfigInterface {

    /**
     * The local ArrayFilter object.
     *
     * @var ArrayFilter
     */
    protected $arrayFilter;

    /**
     * The config data bin.
     *
     * All configuration data stored under here.
     *
     * @var array
     */
    private $configData;

    /**
     * AbstractConfig constructor.
     *
     * Store the configuration data against the object. Attempt to validate
     * and store the recieved configuration resource catching all exceptions
     * this produces (if any). If an exception is fired wrap this in the
     * ConfigValidationException exception.
     *
     * @param array       $resource    The config data array
     * @param ArrayFilter $arrayFilter The array filter utility object
     *
     * @throws ConfigValidationException
     */
    public function __construct(array $resource, ArrayFilter $arrayFilter){
        $this->arrayFilter = $arrayFilter;

        try{
            $this->validateResource($resource);
            $this->configData = $resource;
        }
        catch(Exception $e){
            $childClassName = get_class($this);
            $exceptionMessage = $childClassName.' config resource validation failed with message: '.$e->getMessage();
            throw new ConfigValidationException($exceptionMessage);
        }
    }

    /**
     * Validate a recieved config resource.
     *
     * Extending classes must provide context around a configuration resource
     * to ensure the configuration resource is fit for use within this context
     * it must define a validation function to ensure the data held within
     * it is in the required format and holds the necessary data.
     *
     * Any problems with the recieved configuration should result in an
     * exception being thrown which will be caught in this classes
     * constructor and be wrapped in the ConfigValidationExeption class.
     *
     * @param array $resource The config resource
     */
    abstract protected function validateResource(array $resource);

    /**
     * Get a value from the config bin.
     *
     * @param string $key     The config value's key
     * @param mixed  $default The value to return if not found
     *
     * @return mixed The found value or the provided default if not found
     */
    public function getValue($key, $default = FALSE){
        return (!empty($this->configData[$key])) ? $this->configData[$key] : $default;
    }

    /**
     * Get all values from the config bin.
     *
     * @return array
     */
    public function getAllValues(){
        return $this->configData;
    }

    /**
     * Get a nested value from the config bin.
     *
     * Attempt to use the array filter to fetch the nested value.
     * If it throws an exception return the provided default.
     *
     * @param string $keyString
     * @param string $keyStringSeperator
     * @param bool $default
     *
     * @return mixed The found value or the provided default if not found
     */
    public function getNestedValue($keyString, $keyStringSeperator = '.', $default = FALSE){
        try{
            return $this->arrayFilter->getNestedValueFromKeyString($this->configData, $keyString, $keyStringSeperator);
        }
        catch(ArrayFilterCouldNotExtractNestedValueException $e){
            return $default;
        }
    }

}