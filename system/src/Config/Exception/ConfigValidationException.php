<?php
/**
 * @file
 * Rural Stack System File.
 * Contains the System\Config\Exception\ConfigValidationException Exception
 *
 * @author Josh Parkinson
 */

namespace System\Config\Exception;

use Exception;

/**
 * Class ConfigValidationException.
 *
 * Thrown when the AbstractConfig class is used as parent to a context
 * specific configuration class but the validation on the recieved
 * configuration resource has failed.
 *
 * This exception acts as a wrapper for all caught exceptions that are
 * created during the validation of a configuration resource.
 *
 * @package System\Config\Exception
 */
class ConfigValidationException extends Exception { }