<?php
/**
 * @file
 * Rural Stack System File.
 * Contains the System\Config\Exception\FileConfigConstructionException Class
 *
 * @author Josh Parkinson
 */

namespace System\Config\Exception;

use Exception;
use System\Config\AbstractFileConfig;

/**
 * Class FileConfigConstructionException
 *
 * The following exception is thrown when a Configuration class that extends the
 * AbstractFileConfig class attempts to create a configuration object from a
 * file that could not be found within the System.
 *
 * @see AbstractFileConfig
 *
 * @package System\Config\Exception
 */
class FileConfigConstructionException extends Exception { }