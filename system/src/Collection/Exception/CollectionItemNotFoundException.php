<?php
/**
 * @file
 * Rural Stack System File.
 * Contains the System\Collection\Exception\CollectionItemNotFoundException Exception
 *
 * @author Josh Parkinson
 */

namespace System\Collection\Exception;

use Exception;

/**
 * Class CollectionItemNotFoundException.
 *
 * Thrown by collection methods that require an item to exist but could not
 * find it.
 *
 * @package System\Collection\Exception
 */
class CollectionItemNotFoundException extends Exception { }