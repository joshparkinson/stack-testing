<?php
/**
 * @file
 * Rural Stack System File.
 * Contains the System\Collection\CollectionInterface Interface
 *
 * @author Josh Parkinson
 */

namespace System\Collection;
use System\Utility\Dumper\DumperInterface;

/**
 * Interface CollectionInterface.
 *
 * Defines base manipulation/mutation methods that must be implemented by
 * collection classes. It is expected that classes implementing this interface
 * (thus defining a collection) will provide a context specific 'addition
 * interface' enforcing the limitations (if any) on what can be added to that
 * specific collection.
 *
 * @package System\Collection
 */
interface CollectionInterface extends DumperInterface  {

    /**
     * Find an item with the provided key.
     *
     * If an item can not be found with the provided key a default should be
     * returned without throwing an error. Allow this default to be defined by
     * the caller.
     *
     * @param string $key     The key of the item to find
     * @param mixed  $default The default to return if item not found
     *
     * @return mixed The found item or the default
     */
    public function find($key, $default = NULL);

    /**
     * Get an item with the provided key.
     *
     * If an item can not be found with the provided key an error must be thrown
     * by the implementing class.
     *
     * @param string $key The key of the item to get
     *
     * @return mixed The found item
     */
    public function get($key);

    /**
     * Check if an item exists within the collection under the provided key.
     *
     * Return a boolean indiciating whether or not the item exists.
     * Do not return the item.
     *
     * @param string $key The item key
     *
     * @return bool Whether the item exists or not.
     */
    public function has($key);

    /**
     * Check if the collection is empty.
     *
     * Return a boolean indicating whether this collection has had any items
     * added to it.
     * True if no items have been added to it (ie. is empty).
     * False if items have been added to it (ie. not empty).
     *
     * @return bool Whether the collection is empty or not
     */
    public function isEmpty();

    /**
     * Remove an item from the collection with the provided key.
     *
     * If an item does not exist in the collection with the specified key an
     * error must be thrown by the implementing class.,
     *
     * @param string $key The key of the item to remove
     *
     * @return $this
     */
    public function remove($key);

}