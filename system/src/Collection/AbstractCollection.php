<?php
/**
 * @file
 * Rural Stack System File.
 * Contains the System\Collection\AbstractCollection Class
 *
 * @author Josh Parkinson
 */

namespace System\Collection;

use ArrayIterator;
use IteratorAggregate;
use System\Collection\Exception\CollectionItemNotFoundException;

/**
 * Class AbstractCollection.
 *
 * Base collection class which needs to be extended to provide mechanism for
 * the addition of collection items
 *
 * @package System\Collection
 */
abstract class AbstractCollection implements CollectionInterface, IteratorAggregate {

    /**
     * The collection items.
     *
     * @var array
     */
    protected $items = array();

    /**
     * Find an item with the provided key.
     *
     * If an item can not be found with the provided key a default should be
     * returned without throwing an error. Allow this default to be defined by
     * the caller.
     *
     * @param string $key     The key of the item to find
     * @param mixed  $default The default to return if item not found
     *
     * @return mixed The found item or the default
     */
    public function find($key, $default = NULL){
        return ($this->has($key)) ? $this->items[$key] : $default;
    }

    /**
     * Get an item with the provided key.
     *
     * If an item can not be found with the provided key an error must be thrown
     * by the implementing class.
     *
     * @param string $key The key of the item to get
     *
     * @return mixed The found item
     *
     * @throws CollectionItemNotFoundException
     */
    public function get($key){
        if(!$this->has($key)){
            $this->throwItemNotFoundException($key);
        }
        return $this->items[$key];
    }

    /**
     * Check is an item exists within the collection under the provided key.
     *
     * Return a boolean indiciating whether or not the item exists.
     * Do not return the item.
     *
     * @param string $key The item key
     *
     * @return bool Whether the item exists or not.
     */
    public function has($key){
        return array_key_exists($key, $this->items);
    }

    /**
     * Check if the collection is empty.
     *
     * Return a boolean indicating whether this collection has had any items
     * added to it.
     * True if no items have been added to it (ie. is empty).
     * False if items have been added to it (ie. not empty).
     *
     * @return bool Whether the collection is empty or not
     */
    public function isEmpty(){
        return empty($this->items);
    }

    /**
     * Remove an item from the collection with the provided key.
     *
     * If an item does not exist in the collection with the specified key an
     * error must be thrown by the implementing class.,
     *
     * @param string $key The key of the item to remove
     *
     * @return $this
     */
    public function remove($key){
        if(!$this->has($key)){
            $this->throwItemNotFoundException($key);
        }
        unset($this->items[$key]);

        return $this;
    }

    /**
     * Get all of the items currently stored within the collection.
     *
     * Return an associative array of collection items key'd by the collection
     * item's key. If this method is called on an empty collection an empty
     * array should be returned.
     *
     * @return array An array of collection items (empty if empty collection)
     */
    public function dump(){
        return $this->items;
    }

    /**
     * Define the iterator to use to Traverse the contents of this object.
     *
     * The data returned via this method is identical to that of the dump
     * method.
     *
     * @return ArrayIterator
     */
    public function getIterator(){
        return new ArrayIterator($this->dump());
    }

    /**
     * Set an item to the collection.
     *
     * This method should be implementated by extending classes as a mechanism
     * to add items to the collection.
     *
     * @param string $key  The item key
     * @param mixed  $item The item
     *
     * @return $this
     */
    protected function setItem($key, $item){
        $this->items[$key] = $item;

        return $this;
    }

    /**
     * Throw an item not found exception.
     *
     * Using the provided key throw an item not found exception.
     *
     * @param string $key The item
     *
     * @throws CollectionItemNotFoundException
     */
    private function throwItemNotFoundException($key){
        throw new CollectionItemNotFoundException(strtr(
            'Item could not be found with key \':itemKey\' in collection: :collection',
            array(':itemKey' => $key, ':collection' => get_class($this))
        ));
    }


}