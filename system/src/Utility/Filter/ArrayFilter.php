<?php
/**
 * @file
 * Rural Stack System File.
 * Contains the System\Utility\Filter\ArrayFilter Class
 *
 * @author Josh Parkinson
 */

namespace System\Utility\Filter;

use System\Utility\Filter\Exception\ArrayFilterCheckyArrayHasKeysMissingKeysException;
use System\Utility\Filter\Exception\ArrayFilterCouldNotExtractNestedValueException;

/**
 * Class ArrayFilter.
 *
 * Provides methods to manipulate array variables.
 *
 * @package System\Utility\Filter
 */
class ArrayFilter {

    /**
     * Converts the passed value to an array if necessary.
     *
     * This method is usally called before implementing 'blind loops'
     * avoiding the need to check before hand that the variable to loop over
     * is an array.
     *
     * @param mixed $value Value to be converted to an array
     *
     * @return array Passed value as an array
     */
    public function forceArray($value){
        return (!is_array($value)) ? array($value) : $value;
    }

    /**
     * Get a nested value from within an array using a key string.
     *
     * Using the passed key string and a defined seperator get the nested
     * data values from the passed array. The key string is converted to a
     * search array via an explode call on the defined key using the
     * $keyStringSeperator.
     *
     * Example: Get value under 'nestedKey' element
     *          $array = array(
     *              'data' => array(
     *                  'sub-data' => array(
     *                      'nestedKey' => 'nestedValue'
     *                  )
     *              )
     *          );
     *          $keySting = 'data.sub-data.nestedKey';
     *          $keyStringSeperator = '.';
     *
     *          returns 'nestedValue'
     *
     *
     * @param array  $array              The array to get the nested value from
     * @param string $keyString          The key string for the nested value
     * @param string $keyStringSeperator The key string seperator (seperate
     *                                   nest keys)
     *
     * @return mixed The nested data
     *
     * @throws ArrayFilterCouldNotExtractNestedValueException
     */
    public function getNestedValueFromKeyString(array $array, $keyString, $keyStringSeperator = '.'){
        $keyStringSegments = explode($keyStringSeperator, $keyString);
        $nestedData = $array;
        foreach($keyStringSegments as $segment){
            if(empty($nestedData[$segment])){
                throw new ArrayFilterCouldNotExtractNestedValueException('Could not get nested value with key: '.$keyString);
            }
            $nestedData = $nestedData[$segment];
        }
        return $nestedData;
    }

    /**
     * Check a passed array has the specified keys.
     *
     * An array of keys to check is passed and then looped over, every key's
     * existance is then checked within the passed array if its not found its
     * added to a list of missing keys. If all keys passed are found the list
     * of missing keys will be empty, if it is not empty (keys are missing)
     * then an exception is thrown.
     *
     * Return TRUE, only if all specified keys exist within the array.
     *
     * @param string|array $arrayKeys An array of keys to check for the
     *                                existance of.
     * @param array        $array     The data array to search in.
     *
     * @return bool TRUE when all keys are found
     *
     * @throws ArrayFilterCheckyArrayHasKeysMissingKeysException
     */
    public function checkArrayHasKeys($arrayKeys, array $array){
        $arrayKeys = $this->forceArray($arrayKeys);
        $missingKeys = array();
        foreach($arrayKeys as $key){
            if(!array_key_exists($key, $array)){
                $missingKeys[] = $key;
            }
        }
        if(!empty($missingKeys)){
            throw new ArrayFilterCheckyArrayHasKeysMissingKeysException($missingKeys);
        }
        return TRUE;
    }

}