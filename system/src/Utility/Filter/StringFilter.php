<?php
/**
 * @file
 * Rural Stack System File.
 * Contains the System\Utility\Filter\StringFilter Class
 *
 * @author Josh Parkinson
 */

namespace System\Utility\Filter;

/**
 * Class StringFilter.
 *
 * Provides methods to manipulate string variables.
 *
 * @package System\Utility\Filter
 */
class StringFilter{

    /**
     * Strip tags from a single string or an array of strings
     *
     * @param string|array $value
     *     The value to strip the tags from.
     *
     * @return string|array
     *     A cleaned string or an array of cleaned strings if an array is
     *     passed as parameter
     */
    public function stripTags(&$value){
        if(is_array($value)){
            array_walk_recursive($value, array($this, 'stripTags'));
        }
        else{
            $value = strip_tags($value);
        }
        return $value;
    }

    /**
     * Transliterate a string.
     *
     * Clean up a string and trim to a max length if necessary. A seperator
     * can be provided to use in replacement of 'space' characters.
     *
     * @param string $string    The string to transliterate
     * @param string $seperator The space replacement
     * @param int    $maxLength The max length of the string
     *
     * @return string The transliterated string
     */
    public function transliterate($string, $seperator = '-', $maxLength = 96){
        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $string);
        $clean = preg_replace("%[^-/+|\w ]%", '', $clean);
        $clean = strtolower(trim(substr($clean, 0, $maxLength), '-'));
        $clean = preg_replace("/[\/_|+ -]+/", $seperator, $clean);
        return $clean;
    }


}