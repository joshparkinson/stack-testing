<?php
/**
 * @file
 * Rural Stack System File.
 * Contains the System\Utility\Filter\Exception\ArrayFilterCheckyArrayHasKeysMissingKeysException Exception
 *
 * @author Josh Parkinson
 */

namespace System\Utility\Filter\Exception;

use Exception;
use System\Utility\Filter\ArrayFilter;

/**
 * Class ArrayFilterCheckyArrayHasKeysMissingKeysException
 *
 * Thrown when the ArrayFilter::checkArrayHasKeys method is called and some
 * of the specified keys are missing from the passed data array.
 *
 * @see ArrayFilter::checkArrayHasKeys()
 *
 * @package System\Utility\Filter\Exception
 */
class ArrayFilterCheckyArrayHasKeysMissingKeysException extends Exception {


    /**
     * ArrayFilterCheckyArrayHasKeysMissingKeysException constructor.
     *
     * Construct the exception message using the keys that were missing from
     * the data array.
     *
     * @param array $missingKeys The keys missing form the data array
     */
    public function __construct(array $missingKeys){
        parent::__construct('Array missing the following keys: '.implode(', ', $missingKeys));
    }

}