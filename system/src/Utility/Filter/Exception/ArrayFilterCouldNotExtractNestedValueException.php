<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the System\Utility\Filter\Exception\ArrayFilterCouldNotExtractNestedValueException Exception
 *
 * @author Josh Parkinson
 */

namespace System\Utility\Filter\Exception;

use Exception;
use System\Utility\Filter\ArrayFilter;

/**
 * Class ArrayFilterCouldNotExtractNestedValueException.
 *
 * Thrown when the ArrayFilter class is used to get a nested value from a
 * passed array but the nested value could not be found with the provided key
 * string.
 *
 * @see ArrayFilter::getNestedValueFromKeyString();
 *
 * @package System\Utility\Filter\Exception
 */
class ArrayFilterCouldNotExtractNestedValueException extends Exception { }