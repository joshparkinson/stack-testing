<?php
/**
 * @file
 * Rural Stack System File.
 * Contains the System\Utility\Dumper\DumperInterface Interface
 *
 * @author Josh Parkinson
 */

namespace System\Utility\Dumper;

/**
 * Interface DumperInterface
 *
 * Defines methods for object that contain data which is able to be dumped by
 * the caller.
 *
 * @package System\Utility\Dumper
 */
interface DumperInterface {

    /**
     * Dump the data held within an object.
     *
     * When this method is called all usable data held within the object should
     * be returned to the caller.
     *
     * @return mixed
     */
    public function dump();

}