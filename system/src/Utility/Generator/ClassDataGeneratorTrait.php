<?php
/**
 * @file
 * Rural Stack System File.
 * Contains the System\Utility\Generator\ClassDataGeneratorTrait Class
 *
 * @author Josh Parkinson
 */

namespace System\Utility\Generator;

/**
 * Class ClassDataGeneratorTrait.
 *
 * Provides methods that can be inserted into classes that require them to
 * generate data around classes in the system.
 *
 * @package System\Utility\Generator
 */
trait ClassDataGeneratorTrait {

    /**
     * Get the name of a class from it's file path.
     *
     * Pass the recieved file path to the utility method (processClassFile)
     * which will process it, returning the received class's name.
     *
     * Note: The following method assumes the use of PRS-0/PSR-4 standards of
     * class file naming.
     *
     * @see ClassDataGeneratorTrait::processClassFile()
     *
     * @param string $filePath The path to the file which contains the class.
     *
     * @return string|null The name of the class extracted from it's file path
     *                     or null if the class name could not be extracted from
     *                     the received file path.
     */
    protected function getClassNameFromFilePath($filePath){
        return $this->processClassFile($filePath, TRUE, FALSE)['name'];
    }

    /**
     * Get the namespace of class from it's file path.
     *
     * Pass the received file path to the utility method (processClassFile)
     * which will process it, returning the received class's namespace,
     *
     * Note: The following method assumes the use of PRS-0/PSR-4 standards of
     * class file naming.
     *
     * @see ClassDataGeneratorTrait::processClassFile()
     *
     * @param string $filePath The path to the file which contains the class.
     *
     * @return string|null The namespace of the class extracted from it's file
     *                     path or null if the class namespace could be
     *                     extracted from the received file path.
     */
    protected function getClassNamespaceFromFilePath($filePath){
        return $this->processClassFile($filePath, FALSE, TRUE)['namespace'];
    }

    /**
     * Get the fully qualified class name of a class from it's file path.
     *
     * Pass the received file path to the utility method (processClassFile)
     * which will process it, retuning both the received class's namespace and
     * name which are then joined to create the fully qualified class name.
     *
     * Note: The following method assumes the use of PRS-0/PSR-4 standards of
     * class file naming.
     *
     * @see ClassDataGeneratorTrait::processClassFile()
     *
     * @param string $filePath The path to the file which contains the class.
     *
     * @return string|null The fully qualified class name of the class extracted
     *                     from it's file path or null if the if the class's
     *                     fully qualified name could not be extracted from the
     *                     received file path. A malformed class name could be
     *                     returned by this method if one of either the class
     *                     name or namespace could not be fetched.
     */
    protected function getClassFqcnFromFilePath($filePath){
        $classData = $this->processClassFile($filePath, TRUE, TRUE);
        return $classData['namespace'].'\\'.$classData['name'];
    }

    /**
     * Utility method for processing class files.
     *
     * The following method is capable of extracting the name of class and its
     * namespace from the the file it resides in. Assuming that is that PSR-0
     * or PSR-4 standards are used in developement of it.
     *
     * The method first initialise a NULL array of $classData which is used to
     * store the extracted name and namespace. If for whatever reason this
     * method is called but neither the class name of namespace are asked to be
     * returned then the NULL array of $classData is returned to the caller.
     *
     * If some data is requested from the file it is opened in a read only state
     * and a file buffer is initialised. A loop is then created which for each
     * run loads 512 bytes of the class file adds it to the buffer, parses the
     * buffer for it's tokens and using these attempts to grab the requested
     * data for the class. The loop is ended once all requsted data is fetched.
     *
     * On closing of the loop (ie. end of processing) the populated $classData
     * array is returned to the caller containing valid values for the requested
     * class data attributes.
     *
     * @param string $filePath          The path to the class to process.
     * @param bool   $getClassName      Attempt to fetch the class's name?
     * @param bool   $getClassNamespace Attempt to fetch the class's namespace?
     *
     * @return array
     */
    private function processClassFile($filePath, $getClassName = FALSE, $getClassNamespace = FALSE){
        $classData = array(
            'name' => NULL,
            'namespace' => NULL
        );

        if(!$getClassName && !$getClassNamespace){
            return $classData;
        }

        $classFile = fopen($filePath, 'r');
        $fileBuffer = '';

        while($getClassName || $getClassNamespace){
            if(feof($classFile)){
                break;
            }

            $fileBuffer .= @fread($classFile, 512);
            if(strpos($fileBuffer, '{') == FALSE){
                continue;
            }

            $tokens = @token_get_all($fileBuffer);

            if($getClassName){
                for ($i = 0;$i<count($tokens);$i++) {
                    if ($tokens[$i][0] === T_CLASS) {
                        for ($j=$i+1;$j<count($tokens);$j++) {
                            if ($tokens[$j] === '{') {
                                $classData['name'] = $tokens[$i+2][1];
                                $getClassName = FALSE;
                                break;
                            }
                        }
                    }
                }
            }

            if($getClassNamespace){
                for ($i = 0; $i < count($tokens); $i++){
                    if ($tokens[$i][0] === T_NAMESPACE){
                        for ($j = $i + 1; $j < count($tokens); $j++){
                            if ($tokens[$j][0] === T_STRING){
                                $classData['namespace'] .= '\\' . $tokens[$j][1];
                            }
                            else{
                                if ($tokens[$j] === '{' || $tokens[$j] === ';'){
                                    $getClassNamespace = FALSE;
                                    break;
                                }
                            }
                        }
                    }
                }
            }

        }

        return $classData;
    }

}