<?php
/**
 * @file
 * Rural Stack System File.
 * Contains the System\Finder\FileFinder Class
 *
 * @author Josh Parkinson
 */

namespace System\Finder;

use Doctrine\Common\Annotations\Annotation\Required;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use SplFileInfo;

/**
 * Class RecursiveFileFinder.
 *
 * Recursively find files within the specified directories
 *
 * @package System\Finder
 */
class RecursiveFileFinder extends AbstractFinder implements RecursiveFinderInterface {

    /**
     * Configure the finder object.
     *
     * The following method calls on the parent finders configuration methods
     * to set limitation parameters to be used as part of the find process.
     *
     * @see AbstractFinder::doFind()
     */
    protected function configure(){
        $this->configureSetFindFiles(TRUE);
        $this->configureSetFindDotFiles(FALSE);
        $this->configureSetFindDirectories(FALSE);
    }

    /**
     * Build finder iterators.
     *
     * Using the search paths set against the finder create the required
     * Recursive RecursiveDirectoryIterator's needed to process the find
     * functionality.
     *
     * @see AbstractFinder::doFind()
     *
     * @param array $searchPaths An array of paths to search in
     *
     * @return array An array of iterators to be used in the 'find' call
     */
    protected function buildIterators(array $searchPaths){
        $iterators = array();
        foreach($searchPaths as $searchPath){
            $iterators[] = new RecursiveIteratorIterator(
                new RecursiveDirectoryIterator($searchPath)
            );
        }
        return $iterators;
    }

    /**
     * Process a found item.
     *
     * Return the found item's full path to the found items array.
     *
     * @see AbstractFinder::doFind()
     *
     * @param SplFileInfo $item The found item
     *
     * @return string The found item's path
     */
    protected function processFoundItem(SplFileInfo $item){
        return $item->getPathname();
    }

    /**
     * Recursively find the items for this finder.
     *
     * Leverages the parent AbstractFinder class to do the actual finding of the
     * items for this finder.
     *
     * @return array The processed found items
     */
    public function findRecursive(){
        return $this->doFind();
    }

}