<?php
/**
 * @file
 * Rural Stack System File.
 * Contains the System\Finder\FileFinder Class
 *
 * @author Josh Parkinson
 */

namespace System\Finder;

use SplFileInfo;

/**
 * Class RecursiveFileLoader.
 *
 * Recursively load (require) files within the specified directories.
 *
 * @package System\Finder
 */
class RecursiveFileLoader extends RecursiveFileFinder {

    /**
     * Process a found item.
     *
     * Require the found item before passing it back to it's parent processor
     * for found item array building.
     *
     * @param SplFileInfo $item The found item
     *
     * @return string The result of the parent processing
     */
    protected function processFoundItem(SplFileInfo $item){
        /** @noinspection PhpIncludeInspection */
        require_once($item->getPathname());
        return parent::processFoundItem($item);
    }

}