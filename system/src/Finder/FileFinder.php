<?php
/**
 * @file
 * Rural Stack System File.
 * Contains the System\Finder\FileFinder Class
 *
 * @author Josh Parkinson
 */

namespace System\Finder;

use DirectoryIterator;
use SplFileInfo;

/**
 * Class FileFinder.
 *
 * Find files within the specified directories.
 *
 * @package System\Finder
 */
class FileFinder extends AbstractFinder implements FinderInterface {

    /**
     * Configure the finder object.
     *
     * The following method calls on the parent finders configuration methods
     * to set limitation parameters to be used as part of the find process.
     *
     * @see AbstractFinder::find()
     */
    protected function configure(){
        $this->configureSetFindFiles(TRUE);
        $this->configureSetFindDotFiles(FALSE);
        $this->configureSetFindDirectories(FALSE);
    }

    /**
     * Build finder iterators.
     *
     * Using the search paths set against the finder create the required
     * DirectoryIterator's needed to process the find functionality.
     *
     * @see AbstractFinder::find()
     *
     * @param array $searchPaths An array of paths to search in
     *
     * @return array An array of iterators to be used in the 'find' call
     */
    protected function buildIterators(array $searchPaths){
        $iterators = array();
        foreach($searchPaths as $searchPath){
            $iterators[] = new DirectoryIterator($searchPath);
        }
        return $iterators;
    }

    /**
     * Process a found item.
     *
     * Return the found item's filename to the found items array.
     *
     * @see AbstractFinder::find()
     *
     * @param SplFileInfo $item The found item
     *
     * @return string The found item's filename
     */
    protected function processFoundItem(SplFileInfo $item){
        return $item->getFilename();
    }

    /**
     * Find items for this Finder class.
     *
     * Leverages the parent AbstractFinder class to do the actual finding of the
     * items for this finder.
     *
     * @return array The processed found item's
     */
    public function find(){
        return $this->doFind();
    }

}