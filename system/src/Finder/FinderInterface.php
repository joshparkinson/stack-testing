<?php
/**
 * @file
 * Rural Stack System File.
 * Contains the System\Finder\FinderInterface Interface
 *
 * @author Josh Parkinson
 */

namespace System\Finder;

/**
 * Interface FinderInterface.
 *
 * Defines methods a Finder class implement.
 *
 * @package System\Finder
 */
interface FinderInterface {

    /**
     * Find any files/directories that fit config definitions set against the
     * finder class implementing this interface.
     *
     * @return mixed The found items
     */
    public function find();

}