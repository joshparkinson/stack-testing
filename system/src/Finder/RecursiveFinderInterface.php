<?php
/**
 * @file
 * Rural Stack System File.
 * Contains the System\Finder\RecursiveFinderInterface Interface
 *
 * @author Josh Parkinson
 */

namespace System\Finder;

/**
 * Interface RecursiveFinderInterface.
 *
 * Defines methods a Recursive finder must implement.
 *
 * @package System\Finder
 */
interface RecursiveFinderInterface {

    /**
     * Recursively find any files/directories that config definitions set
     * against the finder class implementing this interface.
     *
     * @return mixed The found items
     */
    public function findRecursive();

}