<?php
/**
 * @file
 * Rural Stack System File.
 * Contains the System\Finder\AbstractFinder Class
 *
 * @author Josh Parkinson
 */

namespace System\Finder;

use Iterator;
use SplFileInfo;
use System\Finder\Exception\FinderInvalidIteratorException;
use System\Finder\Exception\FinderNoSearchPathsSetException;
use System\Utility\Filter\ArrayFilter;

/**
 * Class AbstractFinder.
 *
 * Provides a base utility class to be used by Finder classes within the system.
 * Extending classes are able to implement context specific configuration
 * settings aswell as defining the PHP iterators that should be used to find
 * the items specific to the Finders use. Aswell as allowing extending classes
 * to define how and which items should be found it allows for processing of
 * each of these found items to be carried before being added to the final
 * returned result set of the find.
 *
 * @see AbstractFinder::configure()
 * @see AbstractFinder::buildIterators();
 * @see AbstractFinder::processFoundItem();
 *
 * @package System\Finder
 */
abstract class AbstractFinder {

    /**
     * An array filter object used to process values provided to the property
     * mutators
     *
     * @var ArrayFilter
     */
    private $arrayFilter;

    /**
     * An array of base paths to search from.
     *
     * @var array
     */
    private $searchPaths = array();

    /**
     * An array of file extensions to limit 'find' results to. If empty all
     * extensions will be included in the search results.
     *
     * @var array
     */
    private $fileExtensions = array();

    /**
     * An array of file name patterns to be excluded from the 'find' search
     * results.
     *
     * @var array
     */
    private $filenameExclusions = array();

    /**
     * Find files flag. Are files to be included in the 'find' search results.
     *
     * @var bool
     */
    private $findFiles = FALSE;

    /**
     * Find dot files flag. Are dot files to be included in the 'find' search
     * results.
     *
     * @var bool
     */
    private $findDotFiles = FALSE;

    /**
     * Find directories flag. Are directories to be included in the 'find'
     * search results.
     *
     * @var bool
     */
    private $findDirectories = FALSE;

    /**
     * AbstractFinder constructor.
     *
     * Initialised the arrayFilter local object for use as part of the
     * property mutation methods. It also calls the configuration method on
     * the extending class to set.
     *
     * @param \System\Utility\Filter\ArrayFilter $arrayFilter
     */
    public function __construct(ArrayFilter $arrayFilter){
        $this->arrayFilter = $arrayFilter;
    }

    /**
     * Configure the finder object.
     *
     * Extending classes must implement this to provide a human friendly way
     * of determining what this finder is actually looking for. The
     * configuration takes places as the first step of the 'find' method
     * therefore this configuration can not be modified publicly through
     * object method calls.
     *
     * @return mixed
     */
    abstract protected function configure();

    /**
     * Build finder's iterators.
     *
     * Extending classes must convert the finders set search paths into the
     * required PHP iterators used to carry out the relevant find. These
     * iterators are built as part of the find method therefore they can
     * not be modified publicly through object method calls.
     *
     * @param array $searchPaths An array of paths to search in
     *
     * @return Iterator[] An array of file iterators
     */
    abstract protected function buildIterators(array $searchPaths);

    /**
     * Process found items.
     *
     * Extending classes must process the found items SplFileInfo obejct to
     * return the relevant information with regards to the extendind classes
     * objectives. If necessary an extending class can 'ignore' this
     * processing phase by return the original $item object.
     *
     * All processed items as part of this method call will be returned as
     * the final step of the 'find' method call.
     *
     * @param \SplFileInfo $item
     *
     * @return mixed
     */
    abstract protected function processFoundItem(SplFileInfo $item);

    /**
     * Set a path or paths to begin the search from.
     *
     * @param string|array $searchPaths
     *
     * @return static
     */
    public function setSearchPaths($searchPaths){
        $this->searchPaths = $this->arrayFilter->forceArray($searchPaths);
        return $this;
    }

    /**
     * Set one or more extenstions to limit 'find' results to
     *
     * @param string|array $fileExtenstions
     *
     * @return static
     */
    public function setFileExtensions($fileExtenstions){
        $this->fileExtensions = $this->arrayFilter->forceArray($fileExtenstions);
        return $this;
    }

    /**
     * Set one or more filename patterns to exlude from the 'find' results.
     *
     * @param string|array $filenameExclusions
     *
     * @return static
     */
    public function setFilenameExclusions($filenameExclusions){
        $this->filenameExclusions = $this->arrayFilter->forceArray($filenameExclusions);
        return $this;
    }

    /**
     * Configure the finder object to determine whether it is searching for
     * files. (Some finders may only search for directories).
     *
     * This method can be called only by extending classes and it usually
     * called as part of the finder's configure method.
     *
     * @param boolean $findFiles Whether to find files or not
     *
     * @return static
     */
    protected function configureSetFindFiles($findFiles){
        $this->findFiles = $findFiles;
        return $this;
    }

    /**
     * Configure the finder object to determine whether it is searching for
     * dot files.
     *
     * This method can be called only by extending classes and it usually
     * called as part of the finder's configure method.
     *
     * @param boolean $findDotFiles Whether to find dot files or not
     *
     * @return static
     */
    protected function configureSetFindDotFiles($findDotFiles){
        $this->findDotFiles = $findDotFiles;
        return $this;
    }

    /**
     * Configure the finder object to determine whether it is searching for
     * directories.
     *
     * This method can be called only by extending classes and it usually
     * called as part of the finder's configure method.
     *
     * @param boolean $findDirectories
     *
     * @return static
     */
    protected function configureSetFindDirectories($findDirectories){
        $this->findDirectories = $findDirectories;
        return $this;
    }

    /**
     * Find files for the extending finder.
     *
     * This method will first ensure that there are search paths set against
     * the finder (done publicly during object instantiation). If not an
     * exception is thrown.
     *
     * The extending class is then called to configure the 'find' method's
     * functionality/parameters/limitations etc. We also call on the
     * extending class to build the iterators required to complete the find
     * of items. This achieved with calls to: configure() and buildIterators()
     *
     * Using the provided iterators we loop throught them checking that the
     * iterator is (as expected) an Iterator. For each iterator we process
     * the items found within it and check against the finders configuration
     * settings to limit the returned found items.
     *
     * Found items (as part of the iterator processing) are passed to the
     * extending class so that any further processing required on a single
     * item within the context of the finder can be carried out before it is
     * added to the array of found items to be returned to the user. This
     * achieved through a call to: processFoundItem()
     *
     * @return array An array of processed found items
     *
     * @throws FinderInvalidIteratorException
     * @throws FinderNoSearchPathsSetException
     */
    protected function doFind(){
        if(empty($this->searchPaths)){
            throw new FinderNoSearchPathsSetException();
        }

        $this->configure();
        $iterators = $this->arrayFilter->forceArray($this->buildIterators($this->searchPaths));

        $found = array();
        foreach($iterators as $iterator){
            if(!$iterator instanceof Iterator){
                throw new FinderInvalidIteratorException($iterator);
            }
            foreach($iterator as $item){
                if(method_exists($item, 'isFile') && $item->isFile()){
                    if(!$this->findFiles) continue;
                    if(!empty($this->fileExtensions) && !in_array($item->getExtension(), $this->fileExtensions)) continue;
                    if(!empty($this->filenameExclusions) && preg_match_all('/'.implode('|', $this->filenameExclusions).'/i', $item->getFilename())) continue;
                }
                if(method_exists($item, 'isDot') && !$this->findDotFiles && $item->isDot()) continue;
                if(method_exists($item, 'isDir') && !$this->findDirectories && $item->isDir()) continue;
                $found[] = $this->processFoundItem($item);
            }
        }

        return $found;
    }

}