<?php
/**
 * @file
 * Rural Stack System File.
 * Contains the System\Finder\Exception\FinderNoSearchPathsSetException Class
 *
 * @author Josh Parkinson
 */

namespace System\Finder\Exception;

use Exception;
use System\Finder\AbstractFinder;

/**
 * Class FinderNoSearchPathsSetException.
 *
 * Thrown when the find method is called on a finder object when no search
 * paths have been set against it.
 *
 * @see AbstractFinder::doFind()
 *
 * @package System\Finder\Exception
 */
class FinderNoSearchPathsSetException extends Exception {

    /**
     * FinderNoSearchPathsSetException constructor.
     *
     * Sends the Exception error messagr to the parent exception
     */
    public function __construct(){
        parent::__construct('Search aborted. No search paths set.');
    }

}