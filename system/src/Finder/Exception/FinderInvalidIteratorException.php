<?php
/**
 * @file
 * Rural Stack System File.
 * Contains the System\Finder\AbstractFinder Class
 *
 * @author Josh Parkinson
 */


namespace System\Finder\Exception;

use Exception;
use System\Finder\AbstractFinder;

/**
 * Class FinderInvalidIteratorException.
 *
 * Thrown when the find method is called on a finder object and the results
 * of the call to the build iterators returns an unexpected thus invalid
 * iterator type.
 *
 * @see AbstractFinder::doFind()
 * @see AbstractFinder::buildIterators()
 *
 * @package System\Finder\Exception
 */
class FinderInvalidIteratorException extends Exception {

    /**
     * FinderInvalidIteratorException constructor.
     *
     * Set the exception message using the type of the invlaid 'Iterator'
     *
     * @param string $passedIterator
     */
    public function __construct($passedIterator){
        parent::__construct(strtr('Search Aborted. Invalid iterator received. Iterator type: :type', array(
            ':type' => gettype($passedIterator)
        )));
    }

}