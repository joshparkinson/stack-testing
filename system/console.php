#!/usr/bin/env php
<?php
/**
 * @file
 * Rural Stack Console Application Entry Point
 *
 * @author Josh Parkinson
 */

//Include compser autoload so we can leverage system/vendor classes
require_once __DIR__.'/../vendor/autoload.php';

use Symfony\Component\Console\Application;
use Symfony\Component\Filesystem\Filesystem;
use System\Console\Command\CreateDrupalSiteCommand;

//Define Directory Names
$webDirectory = 'public';

//Define Stack Paths
$stackRoot = realpath(__DIR__.'/..');
$webRoot = realpath(__DIR__.'/../'.$webDirectory);

//Initialise Application dependencies
$fileSystem = new Filesystem();

//Create console application
$application = new Application();
$application->addCommands(array(
    new CreateDrupalSiteCommand('create-drupal-site', $stackRoot, $webRoot, $webDirectory, $fileSystem),
    new \System\Console\Command\TestCommand('test', $stackRoot, $fileSystem)
));
$application->run();