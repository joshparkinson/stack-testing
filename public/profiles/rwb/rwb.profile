<?php
/**
 * @file
 * Rural Stack: Drupal Installation Profile.
 * Contains drupal hooks for the RWB profile.
 *
 * @author Josh Parkinson
 */

/**
 * Alter the site configuration form for a Drupal site
 * install.
 *
 * Assuming this hook has not already been defined within the system
 * (it shouldn't have been) we will define it and leverage it to
 * alter the new site configuration form setting the default site
 * name to the name of this installation profile: RWB. We also set
 * the sites default country to the United Kingdom (GB).
 *
 * Note: we must use the 'system_form' hook prefix as at this stage
 * of a drupal site build we are not bootstrapped fully so module
 * and (in this case) profile specific hooks are not found nor
 * processed.
 *
 * Implements hook_form_FORM_ID_alter().
 */
if(!function_exists("system_form_install_configure_form_alter")){
    function system_form_install_configure_form_alter(&$form, $form_state) {
        $form['site_information']['site_name']['#default_value'] = 'RWB';
        $form['server_settings']['site_default_country']['#default_value'] = 'GB';
    }
}

/**
 * Alter the 'select install profile' form for a Drupal
 * site install.
 *
 * Assuming this hook has not already been definined within the system
 * (it shouldn't have been) we will define it and leverage it to
 * alter the select an install profile form setting this profile as
 * the default option.
 *
 * Note: we must use the 'system_form' hook prefix as at this stage
 * of a drupal site build we are not bootstrapped fully so module
 * and (in this case) profile specific hooks are not found nor
 * processed.
 *
 * Implements hook_form_alter().
 */
if(!function_exists("system_form_install_select_profile_form_alter")){
    function system_form_install_select_profile_form_alter(&$form, $form_state) {
        foreach ($form['profile'] as $key => $element) {
            $form['profile'][$key]['#value'] = 'rwb';
        }
    }
}