#!/usr/bin/env php
<?php
/**
 * @file
 * Rural Site Drupal Doctrine CLI Entry Point file.
 *
 * The following file should be used as a command line entry point for the
 * Drupal sites that leverage Doctrine functionality in the form of the DBAL,
 * ORM (including the Entity Manager) and the Migrations packages. This file
 * can be used to trigger the various commands attached to each of these
 * Doctrine packages.
 *
 * Despite being a command line file it is run within a fully bootstrapped
 * Drupal environment. This allows the file to gather information on the
 * currently active site from drupal aswell as leverage all other Drupal
 * functionality (hooks etc) in the classes it uses.
 *
 * Configuration:
 * - Define the database key string used to access the site's database
 *   configuration array stored within it's settings.php file. THe majority
 *   of the time the default (default.default) key provided here will be the
 *   correct setting.
 * - There is nothing to edit past this point.
 *
 * Boostrap Drupal:
 * - Define the DRUPAL_ROOT constact required in the Drupal bootstrap
 *   function (drupal_bootstrap())
 * - For multi site Drupal instances we must set a global
 *   $_SERVER['HTTP_HOST'] variable. Drupal uses this value to determine what
 *   the currently active site is. Therefore a check is made to determine where
 *   the file is currently sat. If it is not inside the 'default' site folder
 *   and a 'sites.php' file exists within the Drupal core it is assumed a
 *   Multi-site instance of Drupal is being used. Extract the the active sites
 *   URL form the $sites array (from sites.php) and set this value as the
 *   $_SERVER['HTTP_HOST'] essentially telling Drupal which is the currently
 *   active site within a multi-site install.
 * - The boostrap function of drupal will throw warnings if other $_SERVER
 *   values are not set, this does not break the bootstrap, but will produce
 *   warnings/notices on every command run within this application. Set the
 *   followign to stop these warnings: $_SERVER['SCRIPT_NAME'] and
 *   $_SERVER['REMOTE_ADDR']
 * - Forcefully include the site's settings.php file to stop any breakages
 *   during the database bootstrap within the drupal_bootstrap() function.
 * - Include the Drupal bootstrap file so a call can be made to the
 *   drupal_bootstrap() function.
 * - Run the drupal_bootstrap() function, fully bootstrapping Drupal for this
 *   file.
 *
 * Doctrine CLI Application:
 * - Build the Doctrine ORM Enity Manager for use with the Doctrine CLI
 *   Application commands. Passing FALSE as the second parameter stops the
 *   throwing of an Exception if no Entities are currently defined for (or
 *   could be found by) the Entity Manager. This allows for commands not
 *   relying on the Entity Manager to be run successfully and errors only being
 *   thrown when the Entity Manager and its defined Entites are required
 *   (migrations:diff for example).
 * - Create the Console Applications HelperSet. Use the created Entity
 *   Manager and the Doctrine Console Helper Class (ConsoleRunner) to create
 *   the Entity Manager and Database Connection helpers. The Doctrine
 *   Migrations package requires the QuestionHelper so if it doesn't exists
 *   within the created HelperSet add it in.
 * - Create the Console's Events Dispatcher Adding in the Migrations Event
 *   Subscriber. This Subscriber intercepts any Migration Package commands and
 *   adds in the required configuration data so that it doesnt have to be added
 *   in by the user on every call. The configuration data added consits of:
 *   configuration file location as dicatated by the Rural Stack: Common module
 *   aswell the table name exclusion pattern so that only Entity datbase tables
 *   (ie. not drupal core/contrib database tables) are affected/included in the
 *   migrations. To achieve this all EntityTables are silently prefixed by the
 *   Rural Stack: Common module.
 * - Create the application. Using the Doctrine Console Helper class Console
 *   Runner create the application using the created Entity Manager. This adds
 *   the DBAL/ORM commands automatically to the Application. As Migrations
 *   Package commands can be automatically added, add them aswell as setting
 *   the Event Dispatcher that was created to intercept the Migrations commands.
 *
 * WARNING:
 * This file should sit within the root of a Drupal site's directory. If it
 * is moved it is more than likely that the boostrapping of Drupal aswell as
 * Application creation will fail.
 *
 * @author Josh Parkinson
 */

use Doctrine\ORM\Tools\Console\ConsoleRunner;
use RuralStack\Common\Doctrine\Events\MigrationsEventSubscriber;
use RuralStack\Common\Doctrine\RSEntityManager;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\EventDispatcher\EventDispatcher;

/**
 * Configuration
 */
$databaseKeyString = 'default.default';

/**
 * Boostrap Drupal
 */

//Define the drupal root, required in bootstrap function
define('DRUPAL_ROOT', realpath(__DIR__.'/../..'));

//Get and set the current site URL for multi-site installs.
if(($currenSiteDirectory = basename(__DIR__)) != 'default' && file_exists(($sitesFile = DRUPAL_ROOT.'/sites/sites.php'))){
    /** @noinspection PhpIncludeInspection */
    require_once($sitesFile);
    if(($siteUrl = array_search($currenSiteDirectory, $sites)) !== FALSE){
        $_SERVER['HTTP_HOST'] = $siteUrl;
    }
}

//Define final $_SERVER vars stopping errors
$_SERVER['SCRIPT_NAME'] = '/doctrine-cli.php';
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

//Include the sites settings.php - stops breakage in database bootstrap
/** @noinspection PhpIncludeInspection */
require_once(__DIR__.'/settings.php');

//Include the bootstrap file, contains the bootstrap function below.
/** @noinspection PhpIncludeInspection */
require_once DRUPAL_ROOT.'/includes/bootstrap.inc';

//Fully bootstrap Drupal.
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

/**
 * Doctrine CLI Application
 */

//Build entity manager, do not throw exception if no entity paths are defined.
$entityManager = RSEntityManager::create($databaseKeyString);

//Create ORM helperset, check has question if not add for Doctrine Migrations
$helperSet = ConsoleRunner::createHelperSet($entityManager);
if(!$helperSet->has('question')){
    $helperSet->set(new QuestionHelper(), 'question');
}

//Create the event dispatcher
$dispatcher = new EventDispatcher();
$dispatcher->addSubscriber(new MigrationsEventSubscriber());

//Create application, add migration commands, run it
$application = ConsoleRunner::createApplication($helperSet);
$application->setDispatcher($dispatcher);
$application->addCommands(array(
    new \Doctrine\DBAL\Migrations\Tools\Console\Command\DiffCommand(),
    new \Doctrine\DBAL\Migrations\Tools\Console\Command\ExecuteCommand(),
    new \Doctrine\DBAL\Migrations\Tools\Console\Command\GenerateCommand(),
    new \Doctrine\DBAL\Migrations\Tools\Console\Command\MigrateCommand(),
    new \Doctrine\DBAL\Migrations\Tools\Console\Command\StatusCommand(),
    new \Doctrine\DBAL\Migrations\Tools\Console\Command\VersionCommand()
));
$application->run();