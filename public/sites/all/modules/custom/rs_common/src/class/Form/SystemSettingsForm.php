<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Form\SystemSettingsForm Class.
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Form;

use System\Utility\Filter\ArrayFilter;

/**
 * Class SystemSettingsForm.
 *
 * Provides a wrapper for the Drupal system_settings_form function. Allows
 * the override of validation and submission hanlders aswell as the
 * overriding of the submit button text.
 *
 * @package RuralStack\Common\Form
 */
class SystemSettingsForm {

    /**
     * The local array filter object.
     *
     * @var ArrayFilter
     */
    private $arrayFilter;

    /**
     * The Drupal form definition array.
     *
     * @var array
     */
    private $form;

    /**
     * Custom validation handlers.
     *
     * Overrides the default system_settings_form() validation handlers if set.
     *
     * @var array
     */
    private $validationHandlers;

    /**
     * Custom submission handlers.
     *
     * Overrides the default system_settings_form() submission handlers if set.
     *
     * @var array
     */
    private $submissionHandlers;

    /**
     * Custom submit button text.
     *
     * Overrides the default system_settings_form() submit button text if set.
     *
     * @var string
     */
    private $submitButtonText;

    /**
     * SystemSettingsForm constructor.
     *
     * Store the Drupal form definition array and the array filter to the
     * SystemSettingsForm object.
     *
     * @param array       $form        The Drupal form definiton
     * @param ArrayFilter $arrayFilter The ArrayFilter object
     */
    public function __construct(array $form, ArrayFilter $arrayFilter){
        $this->form = $form;
        $this->arrayFilter = $arrayFilter;
    }

    /**
     * Set custom validation handlers.
     *
     * @param string|array $handlers A handler or array of handlers to use
     *                               for form validation.
     *
     * @return $this
     */
    public function setValidationHandlers($handlers){
        $this->validationHandlers = $this->arrayFilter->forceArray($handlers);

        return $this;
    }

    /**
     * Set custom submission handlers.
     *
     * @param string|array $handlers A handler or array of handlers to use
     *                               for form submission.
     *
     * @return $this
     */
    public function setSubmissionHandlers($handlers){
        $this->submissionHandlers = $this->arrayFilter->forceArray($handlers);

        return $this;
    }

    /**
     * Set the submit button text
     *
     * @param string $text The text to use for the submit button
     *
     * @return $this
     */
    public function setSubmitButtonText($text){
        $this->submitButtonText = $text;

        return $this;
    }

    /**
     * Build and return the system settings form.
     *
     * The following method will make any necessary overrides (as defined in
     * the classes setters) before returning the form to the caller.
     *
     * @return array The Drupal form definition array
     */
    public function build(){
        $systemSettingsForm = system_settings_form($this->form);
        if(!empty($this->validationHandlers)){
            $systemSettingsForm['#validate'] = $this->validationHandlers;
        }
        if(!empty($this->submissionHandlers)){
            $systemSettingsForm['#submit'] = $this->submissionHandlers;
        }
        if(!empty($this->submitButtonText)){
            $systemSettingsForm['actions']['submit']['#value'] = $this->submitButtonText;
        }
        return $systemSettingsForm;
    }


}