<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\RSC\Action\RsCommonRemoveDoctrineProxiesDataAction Class
 *
 * Note: Classes under the RuralStack\Common\RSC base namepsaces are not intended for
 * use outside of this module. They are specific to this module and are to be used
 * within it only.
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\RSC\Action;

use RuralStack\Common\Doctrine\Config\DoctrineEntityConfig;
use RuralStack\Common\Site\ActiveSiteTrait;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Config\Loader\DelegatingLoader;
use Symfony\Component\Config\Loader\LoaderResolver;
use System\Action\AbstractFileAction;
use System\Config\Loaders\YamlLoader;
use System\Utility\Filter\ArrayFilter;

/**
 * Class RsCommonRemoveDoctrineProxiesDataAction.
 *
 * Contains the definition for the removal of Doctrine Proxy data action.
 *
 * Uses the ActiveSiteTrait to enable usage of methods to get information on
 * the currently active Drupal site.
 *
 * @package RuralStack\Common\RSC\Action
 */
class RsCommonRemoveDoctrineProxiesDataAction extends AbstractFileAction {

    use ActiveSiteTrait;

    /**
     * The Doctrine Proxy class storage directory.
     *
     * Used to avoid unnecessary reparsing of configuration files.
     *
     * @var string
     */
    private $proxyDir;

    /**
     * Define the unique key for this action.
     *
     * @return string
     */
    public function key(){
        return 'rs_common_remove_doctrine_proxies_data';
    }

    /**
     * Execute the remove of Dotrine Proxies action.
     *
     * The follwoing action removes any generated Doctrine Proxy classes from
     * the currently active site.
     *
     * Action elements:
     * - Generate the DoctrineEntityConfiguration object
     * - Store necessary configuration values against action object
     * - Store and CHMOD active site directory to writeable 755
     * - Remove the proxy storage directory and all classes inside it
     * - Revert active site directory CHMOD value to original
     *
     * @return bool TRUE on successful execution.
     */
    public function execute(){
        $arrayFilter = new ArrayFilter();
        $fileLocator = new FileLocator();
        $loaderResolver = new LoaderResolver(array(
            new YamlLoader($fileLocator)
        ));
        $delegatingLoader = new DelegatingLoader($loaderResolver);
        $doctrineConfig = new DoctrineEntityConfig($delegatingLoader, $arrayFilter);

        $this->proxyDir = $this->getActiveSitePath().'/'.$doctrineConfig->getProxyDir();

        $origChmod = $this->fileSystem->getChmod($this->getActiveSitePath());
        $this->fileSystem->chmod($this->getActiveSitePath(), 0755);
        $this->fileSystem->remove($this->proxyDir);
        $this->fileSystem->chmod($this->getActiveSitePath(), $origChmod);

        return TRUE;
    }

    /**
     * Return the success message for this action.
     *
     * @return string The action message
     */
    public function message(){
        return 'Removed Doctrine Proxy classes from: <strong>'.$this->proxyDir.'</strong>';
    }

}