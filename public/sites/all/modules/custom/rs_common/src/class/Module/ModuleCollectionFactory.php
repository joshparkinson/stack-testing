<?php
/**
 * @file
 * Rural Stack System File.
 * Contains the RuralStack\Common\Module\ModuleCollection Class
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Module;

/**
 * Class ModuleCollectionFactory.
 *
 * Contains static factory methods that can be implemented to create
 * ModuleCollection objects.
 *
 * @package RuralStack\Common\Module
 */
final class ModuleCollectionFactory {

    /**
     * Create a ModuleCollection object from an array of Module keys.
     *
     * The following creation method will (for each passed Module key) create
     * a Module object which then added into the returned ModuleCollection
     * object.
     *
     * @param array $moduleKeys
     *     An array of Module keys to create the ModuleCollection object from.
     *
     * @return ModuleCollection The populate ModuleCollection object.
     */
    public static function createFromModuleKeys(array $moduleKeys){
        $moduleCollection = new ModuleCollection();
        foreach($moduleKeys as $moduleKey){
            $moduleCollection->add(self::createModuleFromKey($moduleKey));
        }
        return $moduleCollection;
    }

    /**
     * Create a ModuleCollection object from Modules that implement the passed
     * hook from their Module keys.
     *
     * The following creation method will (for each passed Module key) create
     * a Module object which is then checked to see if it implements the
     * provided hook. If the Module does implement the hook it is added to the
     * collection if not it is disgarded/skipped.
     *
     * @param array $moduleKeys
     *     An array of Module keys to create the ModuleCollection object from.
     * @param string $hookName
     *     The name of the hook each Module must implement to be added to the
     *     returned ModuleCollection object.
     *
     * @return ModuleCollection
     */
    public static function createFromModuleKeysImplementingHook(array $moduleKeys, $hookName){
        $moduleCollection = new ModuleCollection();
        foreach($moduleKeys as $moduleKey){
            $module = self::createModuleFromKey($moduleKey);

            if(!$module->implementsHook($hookName)){
                unset($module);
                continue;
            }

            $moduleCollection->add($module);
        }
        return $moduleCollection;
    }

    /**
     * Create a Module object.
     *
     * The following method is used to centralise the creation of Module objects
     * that are used int he creation of the ModuleCollection object that this
     * factory class provides. Centralising external object creation reduces
     * the amount of development effort required if Module object creation
     * changes.
     *
     * @param $moduleKey
     *     The key of the module to create the Module object for.
     *
     * @return ModuleInterface
     */
    private static function createModuleFromKey($moduleKey){
        return Module::create($moduleKey);
    }

}