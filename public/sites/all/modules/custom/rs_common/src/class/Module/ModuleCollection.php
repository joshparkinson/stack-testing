<?php
/**
 * @file
 * Rural Stack System File.
 * Contains the RuralStack\Common\Module\ModuleCollection Class
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Module;

use System\Collection\AbstractCollection;

/**
 * Class ModuleCollection.
 *
 * Holds an accessible list of Drupal Modules.
 *
 * @package RuralStack\Common\Module
 */
class ModuleCollection extends AbstractCollection implements ModuleCollectionInterface {

    /**
     * Add a module to the collection.
     *
     * @param ModuleInterface $module The module object
     *
     * @return $this
     */
    public function add(ModuleInterface $module){
        $this->setItem($module->getMachineName(), $module);

        return $this;
    }

    /**
     * Get all of the Drupal Module's stored within this collection.
     *
     * Returns an associative array of Drupal Module's key'd by the Module's
     * machine name.
     *
     * @return ModuleInterface[] An array of Drupal Module's
     */
    public function dump(){
        return parent::dump();
    }


}