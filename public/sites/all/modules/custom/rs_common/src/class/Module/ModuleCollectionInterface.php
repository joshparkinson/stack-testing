<?php
/**
 * @file
 * Rural Stack System File.
 * Contains the RuralStack\Common\Module\ModuleCollectionInterface Interface
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Module;

use System\Collection\CollectionInterface;

/**
 * Interface ModuleCollectionInterfac.
 *
 * Defines method of entry to Module Collection classes.
 *
 * @package RuralStack\Common\Module
 */
interface ModuleCollectionInterface extends CollectionInterface  {

    /**
     * Add a module to the collection.
     *
     * @param ModuleInterface $module The module object
     *
     * @return static
     */
    public function add(ModuleInterface $module);

}