<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Doctrine\Entity\Provider\EntityClass\Exception\EntityClassProviderException Class
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Doctrine\Entity\Provider\EntityClass\Exception;

use Exception;

/**
 * Class EntityClassProviderException
 *
 * Thrown when an EntityClassProvider is unable to provide Entity Class FQCN's
 * using one of it's access methods.
 *
 * @package RuralStack\Common\Doctrine\Entity\Provider\Path\Exception
 */
class EntityClassProviderException extends Exception { }