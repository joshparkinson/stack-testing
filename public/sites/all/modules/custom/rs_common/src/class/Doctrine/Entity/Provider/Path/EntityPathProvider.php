<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Doctrine\Entity\Provider\Path\EntityPathProvider Class.
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Doctrine\Entity\Provider\Path;

use RuralStack\Common\Doctrine\Entity\Provider\Path\Exception\EntityPathProviderException;
use RuralStack\Common\Module\Hook\Exception\HookInvocationException;
use RuralStack\Common\Module\Hook\HookInvokerInterface;
use RuralStack\Common\Module\ModuleInterface;
use RuralStack\Common\RSC\Config\RSCConfigConstants;

/**
 * Class EntityPathProvider.
 *
 * Contains methods of obtaining Doctrine ORM Entity Path definitions from
 * the system.
 *
 * @package RuralStack\Common\Doctrine\Entity\Provider\Path
 */
class EntityPathProvider implements EntityPathProviderInterface {

    /**
     * The HookInvoker object
     *
     * @var HookInvokerInterface
     */
    protected $hookInvoker;

    /**
     * AbstractEntityPathProvider constructor.
     *
     * Set the Hook Invocation object to the instantiated object.
     *
     * @param HookInvokerInterface $hookInvoker
     */
    public function __construct(HookInvokerInterface $hookInvoker){
        $this->hookInvoker = $hookInvoker;
    }

    /**
     * Get all the available Doctrine ORM Entity path definitions.
     *
     * @return array A flat array of Enity path definitions.
     */
    public function getEntityPaths(){
        return $this->hookInvoker->invokeAll($this->getEntityPathHookName());
    }

    /**
     * Get Doctrine ORM Entity path definitions from a Drupal Module
     *
     * If the passed Module does not implement the Entity Path definition hook
     * provided by this module (rs_common) an EntityPathProviderException will
     * be thrown.
     *
     * @param ModuleInterface $module
     *     The Module to get the path definitions from.
     *
     * @return array A flat array of Enity path definitions.
     *
     * @throws EntityPathProviderException
     */
    public function getEntityPathsFromModule(ModuleInterface $module){
        try{
            return $this->hookInvoker->invokeStrict(
                $this->getEntityPathHookName(),
                $module
            );
        }
        catch(HookInvocationException $e){
            $exceptionMessage = strtr(
                'Could not get Entity Paths from Module :moduleName as it does 
                not implement the required hook: :hookName',
                array(
                    ':moduleName' => $module->getMachineName(),
                    ':hookName' => $this->getEntityPathHookName()
                )
            );

            throw new EntityPathProviderException($exceptionMessage, 0, $e);
        }
    }

    /**
     * Get the name of the Hook Drupal Modules should implement to define their
     * Doctrine ORM Entity paths.
     *
     * @return string
     */
    protected function getEntityPathHookName(){
        return RSCConfigConstants::RSC_HOOK_DOCTRINE_ENTITY_PATHS;
    }

}