<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Database\DrupalDatabaseConfigurationInterface Interface.
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Database\Schema;

/**
 * Interface DrupalSchemaDefinitionInterface.
 *
 * Defines methods that must be implemented by classes that can provide a
 * Drupal database schema definition array.
 *
 * @package RuralStack\Common\Database\Schema
 */
interface DrupalSchemaDefinitionInterface {

    /**
     * Get the schema definition array.
     *
     * Return an array which contains inside the schema definition.
     * Using the link below basically swap the $schema variable for array();
     *
     * @see https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_schema/7.x
     *
     * @return array The table schema definition
     */
    public function getSchemaDefinition();

}