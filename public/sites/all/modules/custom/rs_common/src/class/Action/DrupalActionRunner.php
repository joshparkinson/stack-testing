<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Action\DrupalActionRunner Class.
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Action;

use System\Action\AbstractActionRunner;

/**
 * Class DrupalActionRunner.
 *
 * Runs any actions that are added to it.
 *
 * @package RuralStack\Common\Action
 */
class DrupalActionRunner extends AbstractActionRunner {

    /**
     * DrupalActionRunner on complete functionality.
     *
     * Format the sucess and acton messages as a Drupal status message.
     *
     * @param string $successMessage The runner success message
     * @param array  $actionMessages An array of messages returned by the
     *                               executed actions
     */
    protected function onComplete($successMessage, array $actionMessages){
        drupal_set_message(
            $successMessage.' '.theme('item_list', array('items' => $actionMessages))
        );
    }

}