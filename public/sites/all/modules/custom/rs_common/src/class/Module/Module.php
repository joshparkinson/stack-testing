<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Module\Module Class.
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Module;

/**
 * Class Module.
 *
 * Holds methods to extract data from a drupal module.
 *
 * @package RuralStack\Common\Module
 */
class Module implements ModuleInterface {

    /**
     * The Drupal module key.
     * Used in the classes methods to extract module data.
     *
     * @var string
     */
    private $moduleKey;

    /**
     * Module constructor.
     *
     * Set the passed module key to the object
     *
     * @param string $modulekey The module key
     */
    public function __construct($modulekey){
        $this->moduleKey = $modulekey;
    }

    /**
     * Get the human readable name of the module.
     *
     * Note:
     * This method will only return a name if the module the object
     * belongs to exists and is enabled within the system.
     *
     * @param mixed $default The return value if name could not be fetched.
     *
     * @return mixed
     */
    public function getName($default = FALSE){
        $moduleInformation = system_get_info('module', $this->moduleKey);
        return (!empty($moduleInformation['name'])) ? $moduleInformation['name'] : FALSE;
    }

    /**
     * Get the machine name of the module.
     *
     * @return mixed
     */
    public function getMachineName(){
        return $this->moduleKey;
    }

    /**
     * Get the module path.
     *
     * By default this method will return the path relative to Drupal install
     * directory. The absolute path to this directory can be returned by
     * setting the $absolute parameter to TRUE.
     *
     * @param bool $absolute Whether to return the absolute path
     *
     * @return mixed
     */
    public function getPath($absolute = FALSE){
        $modulePath = drupal_get_path('module', $this->moduleKey);
        return ($absolute) ? DRUPAL_WEB_ROOT.'/'.$modulePath : $modulePath;
    }

    /**
     * Determine if the module is enabled within the current Drupal site.
     *
     * @return bool TRUE if enabled, FALSE if not
     */
    public function isEnabled(){
        return module_exists($this->moduleKey);
    }

    /**
     * Determine if the module implemenets the passed hook.
     *
     * Note: If the module is currently disabled this method will return FALSE
     * even if the module does implement the passed hook.
     *
     * @param string $hookName The name of the hook to check implementation of
     *
     * @return bool TRUE if this module implements it, FALSE if not
     */
    public function implementsHook($hookName){
        return module_hook($this->getMachineName(), $hookName);
    }

    /**
     * Create an instance of the Module class using the passed array key.
     *
     * Use the static keyword so that if this method is called on a class that
     * extends this one the extending class will be returned rather than its
     * parent (this).
     *
     * @param string $moduleKey
     *     The module key (machine name).
     *
     * @return static
     */
    public static function create($moduleKey){
        return new static($moduleKey);
    }

}