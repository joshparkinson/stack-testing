<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Database\Exception\DrupalDatabaseConfigurationInvalidDatabaseDriverException Exception.
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Database\Exception;

use Exception;

/**
 * Class DrupalDatabaseConfigurationInvalidDatabaseDriverException.
 *
 * Thrown when a DrupalDatabaseConfiguration object is attempted to be
 * created but the recieved configuration contains an invalid driver.
 *
 * @package RuralStack\Common\Database\Exception
 */
class DrupalDatabaseConfigurationInvalidDatabaseDriverException extends Exception {

    /**
     * DrupalDatabaseConfigurationInvalidDatabaseDriverException constructor.
     *
     * Format the exception message. Pass it parent.
     *
     * @param string $driver The invalid driver that triggered the exception
     */
    public function __construct($driver){
        parent::__construct(strtr(
            'Could not create DrupalDatabaseConfiguration. Invalid driver (:driver) received. Expected: mysql,pgsql,sqlite.',
            array(':driver' => $driver)
        ));
    }

}