<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\RSC\Action\RsCommonRemoveDoctrineMigrationsDataAction Class
 *
 * Note: Classes under the RuralStack\Common\RSC base namepsaces are not intended for
 * use outside of this module. They are specific to this module and are to be used
 * within it only.
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\RSC\Action;

use RuralStack\Common\RSC\Config\RSCConfigConstants;
use RuralStack\Common\Site\ActiveSiteTrait;
use Symfony\Component\Yaml\Yaml;
use System\Action\AbstractFileAction;

/**
 * Class RsCommonRemoveDoctrineMigrationsDataAction
 *
 * Constains the defintion for the removal of Doctrine Migration data action.
 *
 * Uses the ActiveSiteTrait to enable usage of methods to get information on
 * the currently active Drupal site.
 *
 * @package RuralStack\Common\RSC\Action
 */
class RsCommonRemoveDoctrineMigrationsDataAction extends AbstractFileAction {

    use ActiveSiteTrait;

    /**
     * The Migrations versioning database table name.
     *
     * Used to avoid unnecessary reparsing of configuration files.
     *
     * @var string
     */
    private $migrationsDbTable;

    /**
     * The Migration classes storage directory.
     *
     * Used to avoid unnecessary reparsing of configuration files.
     *
     * @var string
     */
    private $migrationsDirectory;

    /**
     * Define the unique key for this action.
     *
     * @return string
     */
    public function key(){
        return 'rs_common_remove_doctrine_migrations_data';
    }

    /**
     * Execute the removal of Doctrine Migrations action.
     *
     * The following action removes any generated Doctrine Migration classes
     * as well as the migration versioning database table from the currently
     * active site.
     *
     * Action elements:
     * - Load and parse the migrations configuration file
     * - Store necessary configuration values against action object
     * - Drop the migrations version database table
     * - Store and CHMOD active site directory to writeable 755
     * - Remove the migrations storage directory and all classes inside it
     * - Revert active site directory CHMOD value to original
     *
     * @return bool True on successful execution.
     */
    public function execute(){
        $migrationsConfig = Yaml::parse(file_get_contents($this->getActiveSitePath().'/'.RSCConfigConstants::RSC_DOCTRINE_MIGRATIONS_CONFIG_FILE_FINAL_LOCATION));

        $this->migrationsDbTable = $migrationsConfig['table_name'];
        $this->migrationsDirectory = $this->getActiveSitePath().'/'.$migrationsConfig['migrations_directory'];

        db_drop_table($this->migrationsDbTable);

        $origChmod = $this->fileSystem->getChmod($this->getActiveSitePath());
        $this->fileSystem->chmod($this->getActiveSitePath(), 0755);
        $this->fileSystem->remove($this->migrationsDirectory);
        $this->fileSystem->chmod($this->getActiveSitePath(), $origChmod);

        return TRUE;
    }

    /**
     * Return the success message for this action.
     *
     * @return string The action message
     */
    public function message(){
        return 'Removed Doctrine Migrations data.
        <ul>
            <li>Migration version table <strong>'.$this->migrationsDbTable.'</strong> removed from database.</li>
            <li>Migration classes removed from: <strong>'.$this->migrationsDirectory.'</strong></li>
        </ul>';
    }

}