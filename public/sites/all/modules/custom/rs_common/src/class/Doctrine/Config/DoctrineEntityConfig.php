<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Doctrine\Config\DoctrineEntityConfigInterface Interface.
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Doctrine\Config;

use Doctrine\Common\Cache\Cache;
use Exception;
use RuralStack\Common\Config\AbstractDrupalFileConfig;
use RuralStack\Common\RSC\Config\RSCConfigConstants;

/**
 * Class DoctrineEntityConfig.
 *
 * Contains methods for extracting Doctrine configuration data from a
 * doctrine config file.
 *
 * Extends AbstractDrupalFileConfig allowing sites within a Drupal multisite
 * install to define their own independent Doctrine configurations.
 *
 * @package RuralStack\Common\Doctrine\Config
 */
class DoctrineEntityConfig extends AbstractDrupalFileConfig implements DoctrineEntityConfigInterface {

    /**
     * Get the Doctrine metadate cache driver.
     *
     * Must return fully qualified class name of a class that implements the
     * Doctrine\Common\Cache\Cache Interface.
     *
     * @return Cache
     */
    public function getCacheMetadata(){
        return $this->getNestedValue('cache.metadata');
    }

    /**
     * Get the Doctrine query cache driver.
     *
     * Must return fully qualified class name of a class that implements the
     * Doctrine\Common\Cache\Cache Interface.
     *
     * @return Cache
     */
    public function getCacheQuery(){
        return $this->getNestedValue('cache.query');
    }

    /**
     * Get the Doctrine result cache driver.
     *
     * Must return fully qualified class name of a class that implements the
     * Doctrine\Common\Cache\Cache Interface.
     *
     * @return Cache
     */
    public function getCacheResult(){
        return $this->getNestedValue('cache.result');
    }

    /**
     * Get the path to the directory that will act as storage for the
     * generated Doctrine proxy classes.
     *
     * @return string
     */
    public function getProxyDir(){
        return $this->getValue('proxyDir');
    }

    /**
     * Get the namespace to store the generated Doctrine proxy classes under.
     *
     * @return string
     */
    public function getProxyNamespace(){
        return $this->getValue('proxyNamespace');
    }

    /**
     * Get the configuration file path relative to a Drupal site installation
     * path.
     *
     * This method returns the path to the Doctrine Entity configuration file
     * relative to a Drupal site directory.
     *
     * @return string
     */
    protected function getDrupalSiteConfigFilePath(){
        return RSCConfigConstants::RSC_DOCTRINE_ENTITY_CONFIG_FILE_FINAL_LOCATION;
    }

    /**
     * Validate a recieved doctrine config resource.
     *
     * Ensure the required config keys exist within the recieved config
     * resource (as loaded/parsed by the AbstractDrupalFileConfig class).
     *
     * Check that all the required cache drivers/handlers have been defined
     * and exist and implement the interface: Doctrine\Common\Cache\Cache
     *
     * Note: This validation method does not ensure provided cache drivers have
     * the required PHP packages compiled/enabled (if necessary) this is the
     * responsibility of the developer implementing this sytem.
     *
     * Any exceptions thrown here are caught and wrapped in a
     * ConfigValidationException exception by the config base class:
     * System\Config\AbstractConfig.
     *
     * @param array $resource The config resource
     *
     * @throws Exception
     */
    protected function validateResource(array $resource){
        $this->arrayFilter->checkArrayHasKeys(array('cache', 'proxyDir', 'proxyNamespace'), $resource);

        $cacheConfig = $resource['cache'];
        $this->arrayFilter->checkArrayHasKeys(array('query', 'metadata', 'result'), $cacheConfig);
        foreach($cacheConfig as $cacheType => $cacheClass){
            if(!class_exists($cacheClass)){
                throw new Exception('Defined '.$cacheType.' cache class ('.$cacheClass.') could not be found.');
            }

            if(!in_array('Doctrine\Common\Cache\Cache', class_implements($cacheClass))){
                throw new Exception('Defined '.$cacheType.' cache does not implement Doctrine\Common\Cache\Cache Interface');
            }
        }
    }

}