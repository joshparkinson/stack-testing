<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Doctrine\Entity\Storage\Query\EntityQueryGeneratorInterface Interface.
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Doctrine\Entity\Storage\Query;

/**
 * Interface EntityQueryGeneratorInterface.
 *
 * Defines methods that must be implemented by classes that provide methods for
 * generating SQL queries for Doctrine ORM Entities.
 *
 * @package RuralStack\Common\Doctrine\Entity\Storage\Query
 */
interface EntityQueryGeneratorInterface {

    /**
     * Get the queries that need to be run inorder to install the data structure
     * for the received Doctrine ORM Entity classes.
     *
     * @param array $entityClasses An array of Doctrine ORM Entity FQCN's
     *
     * @return array An array of SQL queries that need to be ran to install the
     *               data structure for the received Entity classes.
     */
    public function getInstallQueries(array $entityClasses);

    /**
     * Get the queries that need to be run inorder to update the data structure
     * to reflect metadata changes for the recieved Doctrine ORM Entity classes.
     *
     * @param array $entityClasses An array of Doctrine ORM Entity FQCN's
     *
     * @return array An array of SQL queries that need to be ran to update the
     *               data structure for the received Entity classes.
     */
    public function getUpdateQueries(array $entityClasses);

    /**
     * Get the queries that need to be run inroder to remove the data structure
     * behind the recieved Doctrine ORM Entity classes.
     *
     * @param array $entityClasses An array of Doctrine ORM Entity FQCN's
     *
     * @return array An array of SQL queries that need to be ran to remove the
     *               data structure behind the received Entity classes.
     */
    public function getRemoveQueries(array $entityClasses);

}