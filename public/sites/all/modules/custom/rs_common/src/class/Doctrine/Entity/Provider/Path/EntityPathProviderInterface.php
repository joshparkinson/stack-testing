<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Doctrine\Entity\Provider\Path\EntityPathProviderInterface Interface
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Doctrine\Entity\Provider\Path;

use RuralStack\Common\Doctrine\Entity\Provider\Path\Exception\EntityPathProviderException;
use RuralStack\Common\Module\ModuleInterface;

/**
 * Interface EntityPathProviderInterface.
 *
 * Defines methods that must be implemented by classes that provide Entity
 * file path definitions for Doctrine ORM Entities.
 *
 * @package RuralStack\Common\Doctrine\Entity\Provider
 */
interface EntityPathProviderInterface {

    /**
     * Get all the available Doctrine ORM Entity path definitions.
     *
     * Returns a flat array of Enity path definitions.
     *
     * @return array
     */
    public function getEntityPaths();

    /**
     * Get Doctrine ORM Entity path definitions from a Drupal Module
     *
     * Returns a flat array of Enity path definitions.
     *
     * @param ModuleInterface $module
     *
     * @return array
     *
     * @throws EntityPathProviderException
     */
    public function getEntityPathsFromModule(ModuleInterface $module);

}