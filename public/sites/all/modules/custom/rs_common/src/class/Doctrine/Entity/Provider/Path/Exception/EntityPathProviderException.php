<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Doctrine\Entity\Provider\Path\Exception\EntityPathProviderException Class.
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Doctrine\Entity\Provider\Path\Exception;

use Exception;

/**
 * Class EntityPathProviderException
 *
 * Thrown when an EntityPathProvider is unable to provide Entity paths using
 * one of it's access methods.
 *
 * @package RuralStack\Common\Doctrine\Entity\Provider\Path\Exception
 */
class EntityPathProviderException extends Exception { }