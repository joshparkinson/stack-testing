<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Doctrine\Entity\Provider\EntityClass\EntityClassProvider Class
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Doctrine\Entity\Provider\EntityClass;

use RuralStack\Common\Doctrine\Entity\Provider\EntityClass\Exception\EntityClassProviderException;
use RuralStack\Common\Doctrine\Entity\Provider\Path\EntityPathProviderInterface;
use RuralStack\Common\Doctrine\Entity\Provider\Path\Exception\EntityPathProviderException;
use RuralStack\Common\Module\ModuleInterface;
use SplFileInfo;
use System\Finder\Exception\FinderNoSearchPathsSetException;
use System\Finder\RecursiveFileFinder;
use System\Utility\Filter\ArrayFilter;
use System\Utility\Generator\ClassDataGeneratorTrait;

class EntityClassProvider extends RecursiveFileFinder implements EntityClassProviderInterface {

    /**
     * Use the ClassDataGeneratorTrait to leverage methods that can covert
     * class file path's to data about the class it contains. This Trait
     * asummes the use of PSR-0 PSR-4 standards for class
     * naming/namespacing/storage.
     */
    use ClassDataGeneratorTrait;

    /**
     * The EntityPathProvider object that is used to provide the paths in which
     * the Doctrine ORM Entity classes live and are there loaded and provided
     * from.
     *
     * @var EntityPathProviderInterface
     */
    private $entityPathProvider;

    /**
     * EntityClassProvider constructor.
     *
     * Store the passed EntityClassProvider object against the class passing
     * the ArrayFilter object to the parent (RecursiveFileFinder) class.
     *
     * @param EntityPathProviderInterface $entityPathProvider
     *     The EntityPathProvider object which will be used as search start
     *     points during the getting/finding of EntityClasses using an
     *     instantiated object of this class.
     * @param ArrayFilter $arrayFilter
     *     The ArrayFilter object used in the parent AbstractFinder class.
     */
    public function __construct(EntityPathProviderInterface $entityPathProvider, ArrayFilter $arrayFilter){
        $this->entityPathProvider = $entityPathProvider;
        parent::__construct($arrayFilter);
    }

    /**
     * Get the Doctrine ORM Entity Classes FQCN's.
     *
     * Search for the Entity classes from within the Entity paths as provided
     * by the EntityPathProvider object. Before finding the Entity paths
     * ensure that only files with the extension .php are returned.
     *
     * @return array
     *     The found, processed Entity Classes.
     */
    public function getEntityClasses(){
        try{
            $this->setFileExtensions('php');
            $this->setSearchPaths($this->entityPathProvider->getEntityPaths());

            return $this->doFind();
        }
        catch(FinderNoSearchPathsSetException $e){
            return array();
        }
    }

    /**
     * Get the Doctrine ORM Entity Classes FQCN's.
     *
     * Search for the Entity classes from within the Entity paths as provided
     * by the EntityPathProvider object. Before finding the Entity paths
     * ensure that only files with the extension .php are returned.
     *
     * @param ModuleInterface $module The Module to get Entity Class FQCN's from.
     *
     * @return array The found, processed Entity Classes.
     *
     * @throws EntityClassProviderException
     */
    public function getEntityClassesFromModule(ModuleInterface $module){
        try{
            $this->setFileExtensions('php');
            $this->setSearchPaths($this->entityPathProvider->getEntityPathsFromModule($module));

            return $this->doFind();
        }
        catch(EntityPathProviderException $e){
            $exceptionMessage = strtr(
                'Could not get Entity Classes from Module :moduleName as it was
                unable to provide Entity Paths in which the Entity classes
                reside. Entity Path fetching failing with the following message:
                :exceptionMessage',
                array(
                    ':moduleName' => $module->getMachineName(),
                    ':exceptionMessage' => $e->getMessage()
                )
            );

            throw new EntityClassProviderException($exceptionMessage, 0, $e);
        }
    }

    /**
     * Recursively find the Doctrine ORM Entity classes.
     *
     * Required due to the extension of the RecursiveFileFinder class, route
     * this call to the getEntityClasses method.
     *
     * @return array
     *     The found, processed Entity Classes.
     */
    public function findRecursive(){
        return $this->getEntityClasses();
    }

    /**
     * Process the found Entity class files.
     *
     * Each Entity class file found during the find process will be required
     * so that it can be used via it's FQCN which is returned to the caller
     * as a result of the find.
     *
     * @param SplFileInfo $item
     *     The found Entity class item.
     *
     * @return string
     *     The FQCN (Fully Qualified Class Name) of the found Entity class.
     */
    public function processFoundItem(SplFileInfo $item){
        /** @noinspection PhpIncludeInspection */
        require_once($item->getPathname());
        return $this->getClassFqcnFromFilePath($item->getPathname());
    }

}