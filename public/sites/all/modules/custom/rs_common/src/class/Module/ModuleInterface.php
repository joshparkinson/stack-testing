<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Module\ModuleInterface Interface.
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Module;

/**
 * Interface ModuleInterface.
 *
 * Defines methods a Drupal Module class must implement.
 *
 * @package RuralStack\Common\Module
 */
interface ModuleInterface {

    /**
     * Get the human readable name of the module.
     *
     * Note: This method will only return a name if the module the object
     * belongs to exists and is enabled within the system.
     *
     * @param mixed $default The return value if name could not be fetched.
     *
     * @return mixed
     */
    public function getName($default = FALSE);

    /**
     * Get the machine name of the module.
     *
     * @return mixed
     */
    public function getMachineName();

    /**
     * Get the module path.
     *
     * By default this method will return the path relative to Drupal install
     * directory. The absolute path to this directory can be returned by
     * setting the $absolute parameter to TRUE.
     *
     * @param bool $absolute Whether to return the absolute path
     *
     * @return mixed
     */
    public function getPath($absolute = FALSE);

    /**
     * Determine if the module is enabled within the current Drupal site.
     *
     * @return bool TRUE if enabled, FALSE if not
     */
    public function isEnabled();

    /**
     * Determine if the module implemenets the passed hook.
     *
     * @param string $hookName The name of the hook to check implementation of
     *
     * @return bool TRUE if this module implements it, FALSE if not
     */
    public function implementsHook($hookName);

}