<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the Doctrine settings form definition.
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Output\Builder\Table;

use System\Utility\Filter\ArrayFilter;
use System\Utility\Filter\StringFilter;

/**
 * Class TableBuilder
 *
 * Builds a HTML Table using Drupal friendly definition arrays.
 *
 * todo: set empty text
 *
 * @package RuralStack\Common\Output\Builder\Table
 */
class TableBuilder {

    /**
     * The local ArrayFilter object.
     *
     * @var ArrayFilter
     */
    protected $arrayFilter;

    /**
     * The local StringFilter obejct.
     *
     * @var StringFilter
     */
    protected $stringFilter;

    /**
     * The table header definition array.
     *
     * @var array
     */
    protected $header;

    /**
     * The table rows definition array.
     *
     * @var array
     */
    protected $rows;

    /**
     * The table attributes definition array.
     *
     * @var array
     */
    protected $attributes;

    /**
     * Enabled sticky header flag.
     *
     * @var bool
     */
    protected $stickyHeader;

    /**
     * TableBuilder constructor.
     *
     * Store the ArrayFilter and StringFilter objects to the TableBuilder
     * object instance.
     *
     * Initialise the defaults for the various table builder properties.
     *
     * @see TableBuilder::setDefaults()
     *
     * @param ArrayFilter  $arrayFilter
     *     The ArrayFilter object to use in table building functionality
     * @param StringFilter $stringFilter
     *     The StringFilter object to use in table building functionality
     */
    public function __construct(ArrayFilter $arrayFilter, StringFilter $stringFilter){
        $this->arrayFilter = $arrayFilter;
        $this->stringFilter = $stringFilter;
        $this->setDefaults();
    }

    /**
     * Set a table header.
     *
     * User a headerColumn definition array to set a header column against the
     * table being built.
     *
     * The header definition should follow Drupal guides for theme('table', ...)
     * however this class allows for a few extra settings to be defined that
     * can be used by this class to build the table.
     *
     * Example:
     * $headerColumn = array(
     *     'data'     => 'Header Name',              (Drupal)
     *     'colspan'  => 2,                          (Drupal)
     *     'class'    => 'header-class',             (Drupal)
     *     'sortable' => TRUE,                       (TableBuilder)
     *     'sortType' => 'string'|'number'|'date',   (TableBuilder - Required if 'sortable'),
     * );
     *
     * @param array $headerColumn
     *     The Drupal header column definition array.
     *
     * @return $this
     */
    public function setHeader(array $headerColumn){
        $cleanName = $this->stringFilter->transliterate($headerColumn['data']);

        if(!empty($headerColumn['class'])){
            $headerColumn['class'] = $this->arrayFilter->forceArray($headerColumn['class']);
        }

        if(isset($headerColumn['sortable']) && $headerColumn['sortable'] === TRUE){
            $headerColumn['field'] = $cleanName;

            //todo: validate sort type
            $headerColumn['sortType'] = strtolower($headerColumn['sortType']);
            unset($headerColumn['sortable']);
        }

        $this->header[$cleanName] = $headerColumn;

        return $this;
    }

    /**
     * Set multiple table headers.
     *
     * Using an array of headerColumn definitions set multiple headers to the
     * table using one call.
     *
     * @see TableBuilder::setHeader() - Header column definition examples.
     *
     * @param array $headerColumns
     *     An array of headerColumn definitions to be set against the table.
     *
     * @return $this
     */
    public function setHeaderMultiple(array $headerColumns){
        foreach($headerColumns as $headerColumn){
            $this->setHeader($headerColumn);
        }

        return $this;
    }

    /**
     * Add a single row to the table
     *
     * The row definition should follow Drupal guides for theme('table', ...).
     *
     * Note: If the row being added is for use in a sortable date column (see
     * ::setHeader) then a timestamp element must be defined within the row
     * definition so that this builder can sort against it properly.
     *
     * Example:
     * $row = array(
     *     array(
     *         'data'      => 'Text column',              (Drupal)
     *         'class'     => 'text-column'               (Drupal)
     *     ),
     *     array(
     *         'data'      => '10/04/2016 19:30',         (Drupal)
     *         'colspan'   => 2,                          (Drupal)
     *         'class'     => 'date-column',              (Drupal)
     *         'timestamp' => '1460888227'                (TableBuilder)
     *     )
     * );
     *
     * @param array $row
     *     An Drupal row definition array
     *
     * @return $this
     */
    public function setRow(array $row){
        if(!empty($row['class'])){
            $row['class'] = $this->arrayFilter->forceArray($row['class']);
        }

        $this->rows[] = $row;

        return $this;
    }

    /**
     * Set multiple table rows.
     *
     * Using an array of row definitions set multiple rows to the table using
     * one call.
     *
     * @see TableBuilder::setRow() - Row definition example
     *
     * @param array $rows
     *     An array of row definitions to be set against thte table
     *
     * @return $this
     */
    public function setRowMultiple(array $rows){
        foreach($rows as $row){
            $this->setRow($row);
        }

        return $this;
    }

    /**
     * Set attributes against the table.
     *
     * Allows for 'class', 'id', data tags etc to be applied to generated table
     * HTML.
     *
     * @param string $type
     *     The type of attribute to be set agains tthe table. Example: 'class'
     * @param string|array $value
     *     A single attribute value or an array of values.
     *
     * @return $this
     */
    public function setAttribute($type, $value){
        $this->attributes[$type] = $this->arrayFilter->forceArray($value);

        return $this;
    }

    /**
     * Enable/disable set the sticky header for the table.
     *
     * @param bool $enabled
     *     TRUE to enable the sticky header, false to disable it.
     *
     * @return $this
     */
    public function setStickyHeader($enabled){
        $this->stickyHeader = $enabled;

        return $this;
    }

    /**
     * Build the table.
     *
     * Leaverage the Drupal themeing layer to generate the HTML for the table
     * being built and return it to the caller.
     *
     * @return string
     *     The table HTML
     */
    public function build(){
        return theme('table', array(
            'header' => $this->header,
            'rows' => $this->rows,
            'attributes' => $this->attributes,
            'sticky' => $this->stickyHeader
        ));
    }

    /**
     * Sort the rows within the table
     *
     * @return $this
     */
    public function sort(){
        $sortOrder = tablesort_get_order($this->header);
        $sortDirection = tablesort_get_sort($this->header);

        //If no sort column specified
        //or no sort direction specified
        //or invalid sort column specifed
        //or no sortType specified
        if(empty($sortOrder['sql'])
            || empty($sortDirection)
            || ($rowColumnIndex = array_search($sortOrder['sql'], array_keys($this->header))) === NULL
            || empty($this->header[$sortOrder['sql']]['sortType'])
        ){
            return $this;
        }

        //Attempt to sort columns based on sort type
        //If unexpected sort type kill sort call
        switch ($this->header[$sortOrder['sql']]['sortType']){
            case 'string':
                uasort($this->rows, function($a, $b) use ($rowColumnIndex){
                    return strcasecmp(
                        $this->stringFilter->stripTags($a[$rowColumnIndex]['data']),
                        $this->stringFilter->stripTags($b[$rowColumnIndex]['data'])
                    );
                });
                break;
            case 'number':
                uasort($this->rows, function($a, $b) use ($rowColumnIndex){
                    return (floatval($b[$rowColumnIndex]['data']) < floatval($a[$rowColumnIndex]['data']));
                });
                break;
            case 'date':
                uasort($this->rows, function($a, $b) use ($rowColumnIndex){
                    return ($b[$rowColumnIndex]['timestamp'] < $a[$rowColumnIndex]['timestamp']);
                });
                break;
            default:
                return $this;
        }

        //Reverse sort if necessary
        if($sortDirection == "desc"){
            $this->rows = array_reverse($this->rows);
        }

        return $this;
    }

    /**
     * Reset the TableBuilder object to its default state
     *
     * Leaverages the setDefaults method to reinitialise the TableBuilder
     * object.
     *
     * @see TableBuilder::setDefaults()
     *
     * @return $this
     */
    public function reset(){
        $this->setDefaults();

        return $this;
    }

    /**
     * Set the default property values for the TableBuilder object.
     *
     * Seperated from construction logic so that the object can be reinitialised
     * if being used to build multiple tables.
     */
    private function setDefaults(){
        $this->header = array();
        $this->rows = array();
        $this->attributes = array();
        $this->stickyHeader = FALSE;
    }


}