<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Module\Hook\Exception\HookInvocationException Exception.
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Module\Hook\Exception;

use Exception;

/**
 * Class HookInvocationException.
 *
 * Thrown when there is an error in invoking a hook against a Module or
 * collection of Modules. An example being when a hook is invoked strictly
 * against a Module that does not implement.
 *
 * @package RuralStack\Common\Module\Hook\Exception
 */
class HookInvocationException extends Exception { }