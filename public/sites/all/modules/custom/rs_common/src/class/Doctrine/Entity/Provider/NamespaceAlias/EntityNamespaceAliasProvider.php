<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Doctrine\Entity\Provider\NamespaceAlias\EntityNamespaceProvider Interface
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Doctrine\Entity\Provider\NamespaceAlias;

use RuralStack\Common\Doctrine\Entity\Provider\NamespaceAlias\Exception\EntityNamespaceAliasProviderException;
use RuralStack\Common\Module\Hook\Exception\HookInvocationException;
use RuralStack\Common\Module\Hook\HookInvokerInterface;
use RuralStack\Common\Module\ModuleInterface;
use RuralStack\Common\RSC\Config\RSCConfigConstants;

/**
 * Class EntityNamespaceAliasProvider.
 *
 * Can be used to obtain Entity Namespace Alias definitions currently defined
 * within the system.
 *
 * @package RuralStack\Common\Doctrine\Entity\Provider\NamespaceAlias
 */
class EntityNamespaceAliasProvider implements EntityNamespaceAliasProviderInterface {

    /**
     * The HookInvoker object
     *
     * @var HookInvokerInterface
     */
    protected $hookInvoker;

    /**
     * AbstractEntityPathProvider constructor.
     *
     * Set the Hook Invocation object to the instantiated object.
     *
     * @param HookInvokerInterface $hookInvoker
     */
    public function __construct(HookInvokerInterface $hookInvoker){
        $this->hookInvoker = $hookInvoker;
    }

    /**
     * Get all of the Entity Namespace Aliases currently defined within the
     * system.
     *
     * This method will leverage the Hook Invocation resource to fire the Entity
     * Namespace alias definition hook against all Modules in the system that
     * implement it and are currently enabled.
     *
     * If no currently enabled modules implement the Entity Namespace Alias
     * definition hook then this method may return an empty array.
     *
     * @return array
     *     A merged result set of all Module's Enity Namespace Alias definitions
     *     (alias => full namespace) or an empty array if no enabled Modules
     *     implement the hook.
     */
    public function getNamespaceAliases(){
        return $this->hookInvoker->invokeAll($this->getEntityNamespaceAliasHookName());
    }

    /**
     * Get the Namespace Aliases from a Drupal Module
     *
     * This method will leverage the Hook Invocation resource to fire the
     * Entity Namespace alias definition hook against the passed Drupal
     * Module
     *
     * If the passed Module does not the Entity Namespace Alias definition hook
     * provided by this module (rs_common) an
     * EntityNamespaceAliasProviderException will be thrown.
     *
     * @param ModuleInterface $module
     *     The Module to get the Namespace aliases from.
     *
     * @return array
     *     The Module's Enity Namespace Alias definitions
     *     (alias => full namespace) or an empty array if no enabled Modules
     *     implement the hook.
     *
     * @throws EntityNamespaceAliasProviderException
     */
    public function getNamespaceAliasesFromModule(ModuleInterface $module){
        try{
            return $this->hookInvoker->invokeStrict(
                $this->getEntityNamespaceAliasHookName(),
                $module
            );
        }
        catch(HookInvocationException $e){
            $exceptionMessage = strtr(
                'Could not get Entity Namespace Aliases from Module: :moduleName
                as it does not implement the required hook: :hookName',
                array(
                    ':moduleName' => $module->getMachineName(),
                    ':hookName' => $this->getEntityNamespaceAliasHookName()
                )
            );

            throw new EntityNamespaceAliasProviderException($exceptionMessage, 0, $e);
        }
    }

    /**
     * Get the name of the hook Drupal Modules should implement in order to
     * provide Namespaces for the Entities they define.
     *
     * @return string
     */
    protected function getEntityNamespaceAliasHookName(){
        return RSCConfigConstants::RSC_HOOK_DOCTRINE_ENTITY_NAMESPACES;
    }

}