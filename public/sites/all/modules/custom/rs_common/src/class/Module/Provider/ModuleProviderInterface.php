<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Module\Provider\ModuleProviderInterface Interface
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Module\Provider;

use RuralStack\Common\Module\ModuleInterface;

/**
 * Interface ModuleProviderInterface.
 *
 * Defines methods that classes that provide a Module object must implement.
 *
 * @package RuralStack\Common\Module\Provider
 */
interface ModuleProviderInterface {

    /**
     * Get the Module object from the class that contains it.
     *
     * @return ModuleInterface
     */
    public function getModule();

}