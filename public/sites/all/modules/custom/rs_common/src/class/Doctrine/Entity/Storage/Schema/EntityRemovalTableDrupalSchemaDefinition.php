<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Doctrine\Entity\Storage\Schema\EntityRemovalTableDrupalSchemaDefinition Class.
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Doctrine\Entity\Storage\Schema;

use RuralStack\Common\Database\Schema\AbstractDrupalSchemaDefinition;
use RuralStack\Common\RSC\Config\RSCConfigConstants;

class EntityRemovalTableDrupalSchemaDefinition extends AbstractDrupalSchemaDefinition {

    /**
     * Define the name of the database table the schema definition belongs to.
     *
     * @return string
     */
    protected function tableName(){
        return RSCConfigConstants::RSC_DOCTRINE_ENTITY_REMOVAL_QUERIES_TABLE_NAME;
    }

    /**
     * Define the inner schema definition for the database table.
     *
     * Provider the table definition array as an array. There is no need to Key
     * it with the name of table.
     *
     * Example:
     * return array(
     *     'description' => 'Table description',
     *     'fields' => array(
     *         ...
     *     )
     * )
     *
     * @see https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_schema/7.x
     *
     * @return array
     */
    protected function tableSchema(){
        $moduleColumnName = RSCConfigConstants::RSC_DOCTRINE_ENTITY_REMOVAL_QUERIES_TABLE_MODULE_COLUMN_NAME;
        $queriesColumnName = RSCConfigConstants::RSC_DOCTRINE_ENTITY_REMOVAL_QUERIES_TABLE_QUERIES_COLUMN_NAME;

        return array(
            'description' => 'Contains entity removal queries for modules.',
            'fields' => array(
                $moduleColumnName => array(
                    'description' => 'The name of the module that owns the entities.',
                    'type' => 'varchar',
                    'length' => '100',
                    'not null' => TRUE,
                ),
                $queriesColumnName => array(
                    'description' => 'Serialised removal queries for the module entities.',
                    'type' => 'blob',
                    'not null' => TRUE,
                )
            ),
            'primary key' => array($moduleColumnName)
        );
    }


}