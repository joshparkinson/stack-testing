<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\RSC\Action\RsCommonInitialiseSiteDoctrineImplementationAction Class
 *
 * Note: Classes under the RuralStack\Common\RSC base namepsaces are not intended for
 * use outside of this module. They are specific to this module and are to be used
 * within it only.
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\RSC\Action;

use RuralStack\Common\Action\AbstractDrupalModuleFileAction;
use RuralStack\Common\RSC\Config\RSCConfigConstants;
use RuralStack\Common\Site\ActiveSiteTrait;

/**
 * Class RsCommonInitialiseSiteDoctrineImplementationAction.
 *
 * Initialise the Drupal Doctrine Implementation this module provides. The
 * action will add in the various defualt configuration files and storage
 * directories within the current active site. All files/directories will
 * only be created if they dont already exist to avoid wiping and revert of
 * config data and contents.
 *
 * Uses the ActiveSiteTrait to enable usage of methods to get information on
 * the currently active Drupal site.
 *
 * @package RuralStack\Common\RSC\Action
 */
class RsCommonInitialiseSiteDoctrineImplementationAction extends AbstractDrupalModuleFileAction {

    use ActiveSiteTrait;

    /**
     * Messages specific to each action carried out in the execution method.
     * Used in the build of the final returned action message.
     *
     * @var array
     */
    private $actionMessages = array();

    /**
     * Define the unique key for this action.
     *
     * @return string
     */
    public function key(){
        return 'rs_common_initialise_site_doctrine_implementation';
    }

    /**
     * Initialise the Drupal site's Doctrine Implementation.
     *
     * Before creating any files/directories within the acitve site directory
     * chmod it so non of the copy/creation statements fail. The original
     * permission of the active site directory are stored prior to this and
     * reverted to after action execution has finished.
     *
     * Doctrine CLI Entry Point Creation:
     * If the Doctrine CLI file does not already exist within the active
     * site, copy it from this module to the active site directory. Add a
     * message to the $actionMessages array to signal thi 'sub-action' was
     * completed.
     *
     * Doctrine Entity Configuration Creation:
     * If the Doctrine Entity Configuration file does not already exist
     * within the active site copy it from this module to the active site.
     * Add a message to the $actionMessages array to signal this 'sub-action'
     * was completed.
     *
     * Doctrine Migrations Configuration Creation:
     * If the Doctrine Migrations Configuration file does not already exist
     * within the active site copy it from this module to the active site.
     * Add a message to the $actionMessages array to signal this 'sub-action'
     * was completed.
     *
     * Doctrine Migration Class Storage Directory Creation:
     * If the Doctrine Migration Storage Directory does not already exist
     * within the active site create it. Add a message to the $actionMessages
     * array to signal this 'sub-action' was completed.
     *
     * Doctrine Proxy Class Storage Directory Creation:
     * If the Doctrine Proxy Storage Directory does not already exist within
     * the active site create it. Add a message to the $actionMessages array
     * to signal this 'sub-action' was completed.
     *
     * @return bool TRUE if execution successful
     */
    public function execute(){
        $origChmod = $this->fileSystem->getChmod($this->getActiveSitePath());
        $this->fileSystem->chmod($this->getActiveSitePath(), 0755);

        if(!$this->fileSystem->exists($this->getActiveSitePath().'/'.RSCConfigConstants::RSC_DOCTRINE_CLI_FILE_FINAL_LOCATION)){
            $this->fileSystem->copy(
                $this->module->getPath(TRUE).'/'.RSCConfigConstants::RSC_DOCTRINE_CLI_FILE_DEFAULT_LOCATION,
                $this->getActiveSitePath().'/'.RSCConfigConstants::RSC_DOCTRINE_CLI_FILE_FINAL_LOCATION
            );
            $this->actionMessages[] = 'Created Doctrine CLI entry point file: <strong>'.RSCConfigConstants::RSC_DOCTRINE_CLI_FILE_FINAL_LOCATION.'</strong> in: '.$this->getActiveSitePath();
        }

        if(!$this->fileSystem->exists($this->getActiveSitePath().'/'.RSCConfigConstants::RSC_DOCTRINE_ENTITY_CONFIG_FILE_FINAL_LOCATION)){
            $this->fileSystem->copy(
                $this->module->getPath(TRUE).'/'.RSCConfigConstants::RSC_DOCTRINE_ENTITY_CONFIG_FILE_DEFAULT_LOCATION,
                $this->getActiveSitePath().'/'.RSCConfigConstants::RSC_DOCTRINE_ENTITY_CONFIG_FILE_FINAL_LOCATION
            );
            $this->actionMessages[] = 'Created Doctrine Entity configuration file: <strong>'.RSCConfigConstants::RSC_DOCTRINE_ENTITY_CONFIG_FILE_FINAL_LOCATION.'</strong> in: '.$this->getActiveSitePath();
        }

        if(!$this->fileSystem->exists($this->getActiveSitePath().'/'.RSCConfigConstants::RSC_DOCTRINE_MIGRATIONS_CONFIG_FILE_FINAL_LOCATION)){
            $this->fileSystem->copy(
                $this->module->getPath(TRUE).'/'.RSCConfigConstants::RSC_DOCTRINE_MIGRATIONS_CONFIG_FILE_DEFAULT_LOCATION,
                $this->getActiveSitePath().'/'.RSCConfigConstants::RSC_DOCTRINE_MIGRATIONS_CONFIG_FILE_FINAL_LOCATION
            );
            $this->actionMessages[] = 'Created Doctrine Migrations configuration file: <strong>'.RSCConfigConstants::RSC_DOCTRINE_MIGRATIONS_CONFIG_FILE_FINAL_LOCATION.'</strong> in: '.$this->getActiveSitePath();
        }

        if(!$this->fileSystem->exists($this->getActiveSitePath().'/'.RSCConfigConstants::RSC_DOCTRINE_MIGRATIONS_CLASS_STORAGE_DIR)){
            $this->fileSystem->mkdir($this->getActiveSitePath().'/'.RSCConfigConstants::RSC_DOCTRINE_MIGRATIONS_CLASS_STORAGE_DIR, 0755);
            $this->actionMessages[] = 'Created default Doctrine Migration storage directory: <strong>'.RSCConfigConstants::RSC_DOCTRINE_MIGRATIONS_CLASS_STORAGE_DIR.'</strong> in: '.$this->getActiveSitePath();
        }

        if(!$this->fileSystem->exists($this->getActiveSitePath().'/'.RSCConfigConstants::RSC_DOCTRINE_PROXY_CLASS_STORAGE_DIR)){
            $this->fileSystem->mkdir($this->getActiveSitePath().'/'.RSCConfigConstants::RSC_DOCTRINE_PROXY_CLASS_STORAGE_DIR, 0755);
            $this->actionMessages[] = 'Created default Doctrine Proxy storage directory: <strong>'.RSCConfigConstants::RSC_DOCTRINE_PROXY_CLASS_STORAGE_DIR.'</strong> in: '.$this->getActiveSitePath();
        }

        $this->fileSystem->chmod($this->getActiveSitePath(), $origChmod);
        return TRUE;
    }

    /**
     * Return the success message from this action.
     *
     * If no 'sub-actions' took place, signifed by an empty $actionMessages
     * array return a message station no files/directories were created.
     * Other wise return the implementation initialised message with a list
     * of all of the sub actions that took place.
     *
     * @return string The action message
     */
    public function message(){
        if(empty($this->actionMessages)){
            return 'Drupal Doctrine Implementation Already Initialised. No default files/directories were created.';
        }
        return 'Drupal Doctrine Implementation Initialised. <ul><li>'.implode('</li><li>', $this->actionMessages).'</li></ul>';
    }

    /**
     * Limit this action for use with the 'rs_common' module only.
     *
     * @return array The limited module keys
     */
    protected function supportedModules(){
        return array('rs_common');
    }


}