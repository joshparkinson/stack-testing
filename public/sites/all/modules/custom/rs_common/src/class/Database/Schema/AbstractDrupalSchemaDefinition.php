<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Database\Schema\AbstractDrupalSchemaDefinition Class.
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Database\Schema;

/**
 * Class AbstractDrupalSchemaDefinition.
 *
 * Provides an extendable class which can be used to correct Drupal database
 * table schema definition arrays in the correct form.
 *
 * @package RuralStack\Common\Database\Schema
 */
abstract class AbstractDrupalSchemaDefinition implements DrupalSchemaDefinitionInterface {

    /**
     * Define the name of the database table the schema definition belongs to.
     *
     * @return string
     */
    abstract protected function tableName();

    /**
     * Define the inner schema definition for the database table.
     *
     * Provider the table definition array as an array. There is no need to Key
     * it with the name of table.
     *
     * Example:
     * return array(
     *     'description' => 'Table description',
     *     'fields' => array(
     *         ...
     *     )
     * )
     *
     * @see https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_schema/7.x
     *
     * @return array
     */
    abstract protected function tableSchema();

    /**
     * Get the schema definition array.
     *
     * Using the values provided by the extending class build the database
     * schema definition array as expected according to the
     * DrupalSchemaDefinitionInterface
     *
     * @return array The table schema definition
     */
    public function getSchemaDefinition(){
        return array($this->tableName() => $this->tableSchema());
    }

}