<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Module\Hook\HookInvoker Class.
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Module\Hook;

use RuralStack\Common\Module\Hook\Exception\HookInvocationException;
use RuralStack\Common\Module\ModuleCollectionInterface;
use RuralStack\Common\Module\ModuleInterface;

/**
 * Class HookInvoker.
 *
 * Contains methods capable of invoking Drupal hooks against Modules within the
 * System.
 *
 * @package RuralStack\Common\Module\Hook
 */
class HookInvoker implements HookInvokerInterface {

    /**
     * Invoke a hook against a passed Module object.
     *
     * @param string $hookName
     *     The name of the hook to invoke.
     * @param ModuleInterface $module
     *     The Module object to invoke thie hook against.
     *
     * @return mixed
     *     The return value of the hook implementation or NULL if the passed
     *     Module does not implement the hook.
     */
    public function invoke($hookName, ModuleInterface $module){
        return module_invoke($module->getMachineName(), $hookName);
    }

    /**
     * Strictly invoke a hook against a passed Module object.
     *
     * If the passed Module object does not implement the hook to be invoked an
     * Exception (HookInvocationException) must be thrown.
     *
     * @param string $hookName
     *     The name of the hook to invoke.
     * @param ModuleInterface $module
     *     The Module object to invoke the hook against.
     *
     * @return mixed
     *     The return value of the hook implementation or NULL if the passed
     *     Module does not implement the hook.
     *
     * @throws HookInvocationException
     *     Thrown when the passed Module does not implement the hook.
     */
    public function invokeStrict($hookName, ModuleInterface $module){
        if(!$module->implementsHook($hookName)){
            $this->throwInvocationException($hookName, $module, __FUNCTION__);
        }

        return $this->invoke($hookName, $module);
    }

    /**
     * Invoke a hook aginst a passed ModuleCollection.
     *
     * Invokes the passed hook against all Modules stored within a
     * ModuleCollection returning an array of merged hook invocation results.
     * If any of the Modules stored within the passed ModuleCollection do
     * not implement the passed hook they will not contribute anything to the
     * set of returned results. If non of the Modules stored within the passed
     * ModuleCollection implement the passed hook an empty array
     *
     * @param string $hookName
     *     The name of the hook to invoke.
     * @param ModuleCollectionInterface $moduleCollection
     *     The collection of Module's to invoke the hook against.
     *
     * @return mixed
     *     The return value of the hook implementation or an empty array if non
     *     of the Modules within the passed ModuleCollection implement this
     *     hook.
     */
    public function invokeMultiple($hookName, ModuleCollectionInterface $moduleCollection){
        $hookData = array();

        foreach($moduleCollection->dump() as $module){
            $moduleHookData = $this->invoke($hookName, $module);
            $hookData = array_merge(
                $hookData,
                (!empty($moduleHookData)) ? $moduleHookData : array()
            );
        }

        return $hookData;
    }

    /**
     * Strictly invoke a hook aginst a passed ModuleCollection.
     *
     * Invokes the passed hook against all Modules stored within a
     * ModuleCollection returning an array of merged hook invocation results.
     * If any of the Modules stored within the passed ModuleCollection do not
     * implement the passed hook an Exception (HookInvocationException) must be
     * thrown.
     *
     * @param string $hookName
     *     The name of the hook to invoke.
     * @param ModuleCollectionInterface $moduleCollection
     *     The collection of Module's to invoke the hook against.
     *
     * @return mixed
     *     The merged return values of the hook implementation for all of the
     *     Modules within the passed ModuleCollection.
     *
     * @throws HookInvocationException
     *     Thrown when the passed Module does not implement the hook.
     */
    public function invokeMultipleStrict($hookName, ModuleCollectionInterface $moduleCollection){
        if($moduleCollection->isEmpty()){
            throw new HookInvocationException(strtr(
                'Could not invoke hook: :hookName. Recieved an empty 
                :collectionClass collection object',
                array(
                    ':collectionClass' => get_class($moduleCollection),
                    ':hookName' => $hookName
                )
            ));
        }

        $hookData = array();

        /* @var ModuleInterface $module */
        foreach($moduleCollection->dump() as $module){
            if(!$module->implementsHook($hookName)){
                $this->throwInvocationException($hookName, $module, __FUNCTION__);
            }

            $hookData = array_merge($hookData, $this->invoke($hookName, $module));
        }

        return $hookData;
    }

    /**
     * Invoke a hook for all enabled/implementing Modules.
     *
     * Invoke the passed hook against all enabled Modules that implement it.
     * If no Modules implement the passed hook then an empty array will be
     * returned by this method.
     *
     * @param string $hookName
     *     The name of the hook to invoke.
     *
     * @return array
     *     The merged return values for all the Modules that implement the
     *     passed hook or an empty array if no currently enabled Modules
     *     implement the hook.
     */
    public function invokeAll($hookName){
        return module_invoke_all($hookName);
    }

    /**
     * Strictly invoke a hook for all enabled/implement Modules.
     *
     * Invoke the passed hook against all enabled Modules that implement it.
     * If no Modules implement the passed hook then an Exception
     * (HookInvocationException) must be thrown.
     *
     * @param string $hookName
     *     The name of the hook to invoke.
     *
     * @return array
     *     The merged return values for all the Modules that implement the
     *     passed hook.
     *
     * @throws HookInvocationException
     *     Thrown when the passed Module does not implement the hook.
     */
    public function invokeAllStrict($hookName){
        $hookData = $this->invokeAll($hookName);

        if(empty($hookData)){
            throw new HookInvocationException(strtr(
                'Could not invoke hook using method invokeAllStrict as no 
                currently enabled Modules implement the hook: :hookName.',
                array(':hookName' => $hookName)
            ));
        }

        return $hookData;
    }

    /**
     * Throw a HookInvocationException.
     *
     * The following method is used as a helper method for the strict invocation
     * methods defined to make it easier to throw a standardised invocation
     * exception.
     *
     * @param string $hookName
     *     The name of the hook being invoked.
     * @param ModuleInterface $module
     *     The Module that doesn't implement the hook, causing the Exception.
     * @param $methodName
     *     The name of the invocation method used that triggered the Exception.
     *
     * @throws HookInvocationException
     */
    private function throwInvocationException($hookName, ModuleInterface $module, $methodName){
        throw new HookInvocationException(strtr(
            'Could not invoke hook using invocation method: :methodName. The 
            module: :moduleName does not the implement the hook: :hookName',
            array(
                ':methodName' => $methodName,
                ':moduleName' => $module->getMachineName(),
                ':hookName' => $hookName
            )
        ));
    }

}