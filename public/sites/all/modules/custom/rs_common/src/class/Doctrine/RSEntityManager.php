<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Doctrine\RSEntityManager Class
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Doctrine;

use Doctrine\Common\Cache\ArrayCache;
use Doctrine\Common\Cache\Cache;
use Doctrine\Common\EventManager;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Events;
use RuralStack\Common\Database\DrupalDatabaseConfiguration;
use RuralStack\Common\Database\DrupalDatabaseConfigurationInterface;
use RuralStack\Common\Database\Tools\DrupalDatabaseUrlGenerator;
use RuralStack\Common\Doctrine\Config\DoctrineEntityConfig;
use RuralStack\Common\Doctrine\Config\DoctrineEntityConfigInterface;
use RuralStack\Common\Doctrine\Entity\Provider\NamespaceAlias\EntityNamespaceAliasProvider;
use RuralStack\Common\Doctrine\Entity\Provider\NamespaceAlias\EntityNamespaceAliasProviderInterface;
use RuralStack\Common\Doctrine\Entity\Provider\Path\EntityPathProvider;
use RuralStack\Common\Doctrine\Entity\Provider\Path\EntityPathProviderInterface;
use RuralStack\Common\Doctrine\Extensions\TablePrefixExtension;
use RuralStack\Common\Module\Hook\HookInvoker;
use RuralStack\Common\RSC\Config\RSCConfigConstants;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Config\Loader\DelegatingLoader;
use Symfony\Component\Config\Loader\LoaderResolver;
use System\Config\Loaders\YamlLoader;
use System\Database\Tools\DatabaseUrlGeneratorInterface;
use System\Utility\Filter\ArrayFilter;

//todo: remove factory method, use container

//todo: recomment

/**
 * Class RSEntityManager.
 *
 * Provides a way of creating a useable Doctrine Entity Manager within the
 * context of a Drupal site.
 *
 * @package RuralStack\Common\Doctrine
 */
class RSEntityManager {

    /**
     * The development mode flag.
     *
     * This setting is configured via the admin interface within this
     * module's configuration UI.
     *
     * @var bool
     */
    private $developmentMode;

    /**
     * The Entity Path provider object
     *
     * @var EntityPathProviderInterface
     */
    private $entityPathProvider;

    /**
     * The Entity Namespace provider object.
     *
     * @var EntityNamespaceAliasProvider
     */
    private $entityNamespaceAliasProvider;

    /**
     * The local Doctrine configuration object.
     *
     * @var DoctrineEntityConfigInterface
     */
    private $doctrineConfig;

    /**
     * The local drupal database configuration object.
     *
     * @var DrupalDatabaseConfigurationInterface;
     */
    private $databaseConfig;

    /**
     * The local database url generator object.
     *
     * @var DatabaseUrlGeneratorInterface
     */
    private $databaseUrlGenerator;

    /**
     * RSEntityManager constructor.
     *
     * Store the passed database url generator to the object.
     *
     * @param EntityPathProviderInterface           $entityPathProvider
     * @param EntityNamespaceAliasProviderInterface $entityNamespaceProvider
     * @param DoctrineEntityConfigInterface         $doctrineConfig
     * @param DrupalDatabaseConfigurationInterface  $databaseConfig
     * @param DatabaseUrlGeneratorInterface         $databaseUrlGenerator
     */
    public function __construct(EntityPathProviderInterface $entityPathProvider, EntityNamespaceAliasProviderInterface $entityNamespaceAliasProvider, DoctrineEntityConfigInterface $doctrineConfig, DrupalDatabaseConfigurationInterface $databaseConfig, DatabaseUrlGeneratorInterface $databaseUrlGenerator){
        $this->developmentMode = variable_get(RSCConfigConstants::RSC_VAR_DOCTRINE_DEV_MODE_SETTING, FALSE);

        $this->entityPathProvider = $entityPathProvider;
        $this->entityNamespaceAliasProvider = $entityNamespaceAliasProvider;

        $this->doctrineConfig = $doctrineConfig;
        $this->databaseConfig = $databaseConfig;
        $this->databaseUrlGenerator = $databaseUrlGenerator;
    }

    /**
     * Get the Doctrine Entity Managaer.
     *
     * The following method is used to configure and return a Drupal ready
     * version of the Doctrine Entity Manager.
     *
     * Before the Doctrine entity manager can be configured and created the
     * Doctrine entity paths must be retrieved. This is achieved through the
     * use of the Drupal hook system allowing all enabled modules which
     * require it to define paths to the entity's they define. As a default
     * an exception will be thrown if no entity paths are found during this
     * fetch, this however can be disabled with a call to:
     * RSEntityManager::setThrowNoEntityPathsExceptions.
     *
     * After the fetching of the entity paths the entity manager is
     * configured. This leverages the DoctrineEntityConfigInterface class to
     * extract the relevant configuration values including cache's to use,
     * proxy directory/namespaces etc. Configuration also sets the metadata
     * driver to the doctrine annotation driver with simple annotations
     * disabled. Another call to a drupal hook is triggered here to allow
     * modules to define their own entity namespace aliases.
     *
     * A doctrine database configuration array is then created using the
     * passed DatabaseUrlGeneratorInterface class which along with main
     * configuration (outlined above) will be used to finalise the creation
     * of the entity manager. At this stage all the necessary configuration
     * data has been set/defined to create a fully working Doctrine entity
     * manager within a Drupal context.
     *
     * //todo: rewrite
     * If the passed Drupal database configuration class data contains
     * no table prefix value the entity manager is created and returned
     * to the caller. If however a prefix is found the TablePrefixExtension
     * must be created using this defined prefix. This extension is then
     * added to a newly created EventManager whichs form part of the
     * entity manager creation. After this extension via the EventManager
     * has been added to the entity manager it can be returned to the
     * caller.
     *
     * NOTE:
     * This mehthod and therefore this class is very tightly coupled to
     * various Doctrine classes, the Configuration, EntityManager and
     * EventManager classes for example. This has been deemed an acceptable
     * coupling due to the fact this class is used to create a Doctrine class.
     *
     * @return EntityManager The Drupal ready Entity Manager
     */
    public function getEntityManager(){
        $config = new Configuration();
        $config->setEntityNamespaces($this->entityNamespaceAliasProvider->getNamespaceAliases());
        $config->setMetadataCacheImpl($this->getMetadataCache());
        $config->setMetadataDriverImpl($config->newDefaultAnnotationDriver($this->entityPathProvider->getEntityPaths(), FALSE));
        $config->setQueryCacheImpl($this->getQueryCache());
        $config->setProxyDir($this->doctrineConfig->getProxyDir());
        $config->setProxyNamespace($this->doctrineConfig->getProxyNamespace());
        $config->setAutoGenerateProxyClasses(($this->developmentMode) ? TRUE: FALSE);
        $config->setResultCacheImpl($this->getResultCache());

        //todo: look into this
        //$config->setFilterSchemaAssetsExpression('/^(users|node|rsdet_.*)$/');

        $database = array('url' => $this->databaseUrlGenerator->generateUrl());

        $tablePrefix = new TablePrefixExtension($this->databaseConfig->getPrefix());
        $eventManager = new EventManager();
        $eventManager->addEventListener(Events::loadClassMetadata, $tablePrefix);

        return EntityManager::create($database, $config, $eventManager);
    }

    /**
     * Get the cache driver for use in devlopment mode.
     *
     * This method is used as a fallback in the calls to fetcing the
     * following caches is development mode:
     * - metadata, query, result
     *
     * @see RSEntityManager::getMetadataCache()
     * @see RSEntityManager::getQueryCache()
     * @see RSEntityManager::getResultCache()
     *
     * @return ArrayCache
     */
    private function getDevelopmentCache(){
        return new ArrayCache();
    }

    /**
     * Get the Doctrine metadata cache to be used with the entity manager.
     *
     * Defaults to the development cache if running in development mode.
     *
     * @see RSEntityManager::getEntityManager()
     * @see RSEntityManager::getDevelopmentCache()
     *
     * @return Cache
     */
    private function getMetadataCache(){
        if($this->developmentMode){
            return $this->getDevelopmentCache();
        }
        $metadataCacheClassName = $this->doctrineConfig->getCacheMetadata();

        return new $metadataCacheClassName;
    }

    /**
     * Get the Doctrine query cache to be used with the entity manager.
     *
     * Defaults to the development cache if running in development mode.
     *
     * @see RSEntityManager::getEntityManager()
     * @see RSEntityManager::getDevelopmentCache()
     *
     * @return Cache
     */
    private function getQueryCache(){
        if($this->developmentMode){
            return $this->getDevelopmentCache();
        }
        $queryCacheClassName = $this->doctrineConfig->getCacheQuery();

        return new $queryCacheClassName;
    }

    /**
     * Get the Doctrine result cache to be used with the entity manager.
     *
     * Defaults to the development cache if running in development mode.
     *
     * @see RSEntityManager::getEntityManager()
     * @see RSEntityManager::getDevelopmentCache()
     *
     * @return Cache
     */
    private function getResultCache(){
        if($this->developmentMode){
            return $this->getDevelopmentCache();
        }
        $resultCacheClassName = $this->doctrineConfig->getCacheResult();

        return new $resultCacheClassName;
    }

    /**
     * RSEntityManager Creation Factory Method.
     *
     * The following method is used to create a Drupal ready Doctrine Entity
     * Manager building the required classes in the process.
     *
     * @param bool   $entityPathsDefintionStrictMode Whether to require entity path definition implementation.
     * @param string $databaseKey The database key to set against the url generator
     *
     * @return EntityManager The Drupal ready Doctrine Entity Manager.
     */
    public static function create($databaseKey = 'default.default'){
        $arrayFilter = new ArrayFilter();

        $fileLocator = new FileLocator();
        $loaderResolver = new LoaderResolver(array(
            new YamlLoader($fileLocator)
        ));
        $delegatingLoader = new DelegatingLoader($loaderResolver);
        
        $hookInvoker = new HookInvoker();

        $entityPathProvider = new EntityPathProvider($hookInvoker);
        $entityNamespaceAliasProvider = new EntityNamespaceAliasProvider($hookInvoker);

        $doctrineConfig = new DoctrineEntityConfig($delegatingLoader, $arrayFilter);
        $drupalDatabaseConfig = new DrupalDatabaseConfiguration($arrayFilter, $databaseKey);
        $drupalDatabaseGenerator = new DrupalDatabaseUrlGenerator($drupalDatabaseConfig);

        $rsEntityManager = new self($entityPathProvider, $entityNamespaceAliasProvider, $doctrineConfig, $drupalDatabaseConfig, $drupalDatabaseGenerator);
        return $rsEntityManager->getEntityManager();
    }

}