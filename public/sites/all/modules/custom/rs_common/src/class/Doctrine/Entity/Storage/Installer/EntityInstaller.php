<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Doctrine\Entity\Storage\Installer\EntityInstaller Class.
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Doctrine\Entity\Storage\Installer;

use Exception;
use RuralStack\Common\Doctrine\Entity\Provider\EntityClass\EntityClassProviderInterface;
use RuralStack\Common\Doctrine\Entity\Storage\Installer\Exception\EntityInstallerQueryExecutionException;
use RuralStack\Common\Doctrine\Entity\Storage\Installer\Exception\EntityInstallerRemovalQueriesNotFoundException;
use RuralStack\Common\Doctrine\Entity\Storage\Installer\Exception\EntityInstallerUpdateQueriesNotFoundException;
use RuralStack\Common\Doctrine\Entity\Storage\Query\EntityQueryGeneratorInterface;
use RuralStack\Common\Module\ModuleInterface;
use RuralStack\Common\RSC\Config\RSCConfigConstants;

/**
 * Class EntityInstaller
 *
 * Contains installation actions methods for use with Drupal Module's
 * that implement the Doctrine ORM Package.
 *
 * @package RuralStack\Common\Doctrine\Entity\Storage\Installer
 */
class EntityInstaller {

    /**
     * The local EntityClassProvider object
     *
     * @var EntityClassProviderInterface
     */
    private $entityClassProvider;

    /**
     * The local EntityQueryGenerator objecgt
     *
     * @var EntityQueryGeneratorInterface
     */
    private $entityQueryGenerator;

    /**
     * The name of database table that will hold the Entity removal queries.
     *
     * @var string
     */
    private $removalQueryStorageTableName;

    /**
     * The name of the columnd that will hold the Module identifier.
     *
     * @var string
     */
    private $removalQueryStorageModuleColumn;

    /**
     * The name of the column that will hold the removal queries.
     *
     * @var string
     */
    private $removalQueryStorageQueriesColumn;

    /**
     * EntityInstaller constructor.
     *
     * Initialise the object setting the passed service objects and the removal
     * query database table parameters.
     *
     * @param EntityClassProviderInterface $entityClassProvider
     *     The EnityClassProvider to be used by this object.
     * @param EntityQueryGeneratorInterface $entityQueryGenerator
     *     The EntityQueryGenerator to be used by this object.
     */
    public function __construct(EntityClassProviderInterface $entityClassProvider, EntityQueryGeneratorInterface $entityQueryGenerator){
        $this->entityClassProvider = $entityClassProvider;
        $this->entityQueryGenerator = $entityQueryGenerator;
        $this->removalQueryStorageTableName = RSCConfigConstants::RSC_DOCTRINE_ENTITY_REMOVAL_QUERIES_TABLE_NAME;
        $this->removalQueryStorageModuleColumn = RSCConfigConstants::RSC_DOCTRINE_ENTITY_REMOVAL_QUERIES_TABLE_MODULE_COLUMN_NAME;
        $this->removalQueryStorageQueriesColumn = RSCConfigConstants::RSC_DOCTRINE_ENTITY_REMOVAL_QUERIES_TABLE_QUERIES_COLUMN_NAME;
    }

    /**
     * Install the Doctrine Entities data structure for a specific Drupal
     * Module.
     *
     * Extract the Entity classes from the passed Drupal Module.
     * (Leverage EntityClassProvider)
     *
     * From these Entity classes generate and run the Entity installation
     * queries. (Leverage EntityQueryGenerator) If the execution of one of the
     * installation queries fails throw an exception and rollback the database
     * transaction.
     *
     * Set the removal queries to the database. If storage of these queries
     * fails throw an exeception and rollback the database transaction.
     *
     * @param ModuleInterface $module
     *     The Drupal Module to install the Entities for.
     *
     * @return $this
     *
     * @throws EntityInstallerQueryExecutionException
     */
    public function installModuleEntities(ModuleInterface $module){
        $moduleEntityClasses = $this->entityClassProvider->getEntityClassesFromModule($module);
        $transaction = db_transaction($module->getMachineName().' Entity Installation');

        foreach($this->entityQueryGenerator->getInstallQueries($moduleEntityClasses) as $moduleInstallQuery){
            try{
                db_query($moduleInstallQuery);
            }
            catch(Exception $e){
                $transaction->rollback();

                throw new EntityInstallerQueryExecutionException(strtr(
                    'Could not install Doctrine Entities for Module: :moduleKey
                    as execution of one of the installation queries failed with
                    the exception message: :exceptionMessage',
                    array(
                        ':moduleKey' => $module->getMachineName(),
                        ':exceptionMessage' => $e->getMessage()
                    )
                ), $e->getCode(), $e);
            }
        }

        try{
            $this->setEntityRemovalQueries(
                $module->getMachineName(),
                $this->entityQueryGenerator->getRemoveQueries($moduleEntityClasses)
            );
        }
        catch(Exception $e){
            $transaction->rollback();

            throw new EntityInstallerQueryExecutionException(strtr(
                'Could not install Doctrine Entities for Module: :moduleKey
                as storage of the removal queries associated to these Entites
                could not be executed successfully. Storage execution failed
                with exception message: :exceptionMessage',
                array(
                    ':moduleKey' => $module->getMachineName(),
                    ':exceptionMessage' => $e->getMessage()
                )
            ), $e->getCode(), $e);
        }

        return $this;
    }

    /**
     * Update the Doctrine Entities data structure for a specific Drupal Module.
     *
     * Fetch the update queries that need to be executed for the passed
     * Drupal Module. If no update queries are required to be run then throw
     * a EntityInstallerUpdateQueriesNotFoundException.
     *
     * Assuming update queries are found create a database transaction and
     * attempt to execute all fo the fetched queries. If execution of one of
     * the queries fails throw an EntityInstallerQueryExecutionException.
     *
     * After sucessfully running all of the update queries reset the Entity
     * removal queries again catching any execution exception and throwing an
     * EntityInstallerQueryExecutionException. Resetting of the Entity removal
     * queries is required as any Entity updates that introduce new
     * relationships will generate new foreign keys, join tables etc etc and
     * these may be left behind on removal if not rechecked and stored in the
     * database.
     *
     * @param ModuleInterface $module
     *     The Module to update the Entities for.
     *
     * @return $this
     *
     * @throws EntityInstallerQueryExecutionException
     * @throws EntityInstallerUpdateQueriesNotFoundException
     */
    public function updateModuleEntities(ModuleInterface $module){
        $moduleEntityClasses = $this->entityClassProvider->getEntityClassesFromModule($module);

        $updateQueries = $this->entityQueryGenerator->getUpdateQueries($moduleEntityClasses);

        if(empty($updateQueries)){
            throw new EntityInstallerUpdateQueriesNotFoundException(strtr(
                'Could not update Entity classes provided by Module :moduleKey.
                No Entity update queries could be fetched, assuming nothing to
                do.',
                array(':moduleKey' => $module->getMachineName())
            ));
        }

        $transaction = db_transaction($module->getMachineName().' Entity Updates');
        foreach($updateQueries as $updateQuery){
            try{
                db_query($updateQuery);
            }
            catch(Exception $e){
                $transaction->rollback();

                throw new EntityInstallerQueryExecutionException(strtr(
                    'Error updating Doctrine Entities for Module: :moduleKey. 
                    Execution of one of the update queries failed with the 
                    exception message: :exceptionMessage',
                    array(
                        ':moduleKey' => $module->getMachineName(),
                        ':exceptionMessage' => $e->getMessage()
                    )
                ), $e->getCode(), $e);
            }
        }

        try{
            $this->setEntityRemovalQueries(
                $module->getMachineName(),
                $this->entityQueryGenerator->getRemoveQueries($moduleEntityClasses)
            );
        }
        catch(Exception $e){
            $transaction->rollback();

            throw new EntityInstallerQueryExecutionException(strtr(
                'Could not update Doctrine Entities for Module: :moduleKey as
                storage of the updated removal queries associated to these 
                Entities could not be executed successfully. Storage execution
                failed with exception message: :exceptionMessage',
                array(
                    ':moduleKey' => $module->getMachineName(),
                    ':exceptionMessage' => $e->getMessage()
                )
            ), $e->getCode(), $e);
        }

        return $this;
    }

    /**
     * Uninstall the Doctrine Entities data structure for a specific Drupal
     * Module.
     *
     * Fetch the removal queries that belong to the specified Module. If none
     * are found throw an Exception.
     *
     * Create a new database transaction for the uninstallation process.
     * Attempt to run all of the fetched removal queries, if execution of any
     * of these queries fails rollback the transaction and throw an Exception.
     *
     * Remove the stored removal queries for the passed Drupal Module. If these
     * queries could not be removed rollback the database transaction and throw
     * an Exception.
     *
     * @param ModuleInterface $module
     *     The Module to uninstall the Entities for.
     *
     * @return $this
     *
     * @throws EntityInstallerQueryExecutionException
     * @throws EntityInstallerRemovalQueriesNotFoundException
     */
    public function uninstallModuleEntities(ModuleInterface $module){
        $removalQueries = $this->getEntityRemovalQueries($module->getMachineName());

        if(empty($removalQueries)){
            throw new EntityInstallerRemovalQueriesNotFoundException(strtr(
                'Error uninstalling Doctrine Entities for Module: :moduleKey. 
                No Entity removal queries could be found.',
                array(':moduleKey' => $module->getMachineName())
            ));
        }

        $transaction = db_transaction($module->getMachineName().' Entity Uninstallation');
        foreach($removalQueries as $removalQuery){
            try{
                db_query($removalQuery);
            }
            catch(Exception $e){
                $transaction->rollback();

                throw new EntityInstallerQueryExecutionException(strtr(
                    'Error uninstalling Doctrine Entities for Module: 
                    :moduleKey. Execution of one of the removal queries
                    associated with this Module\'s Entities failed with the 
                    exception message: :exceptionMessage',
                    array(
                        ':moduleKey' => $module->getMachineName(),
                        ':exceptionMessage' => $e->getMessage()
                    )
                ), $e->getCode(), $e);
            }
        }

        try{
            $this->deleteEntityRemovalQueries($module->getMachineName());
        }
        catch(Exception $e){
            $transaction->rollback();

            throw new EntityInstallerQueryExecutionException(strtr(
                'Error uninstalling Doctrine Entities for Module: :moduleKey.
                Deletion of the removal queries associated to the Entities
                for this Module failed with the exception message: 
                :exceptionMessage',
                array(
                    ':moduleKey' => $module->getMachineName(),
                    ':exceptionMessage' => $e->getMessage()
                )
            ), $e->getCode(), $e);
        }

        return $this;
    }

    /**
     * Check if Entity data structure updates are required.
     *
     * If no Entity classes could be found, or no currently enabled Module
     * provides Doctrine Entity classes this method call will return FALSE.
     *
     * Otherwise using the Entity classes found, attempt to fetch update
     * queries for all of them. If update queries are found Entity updates are
     * required and this method will return TRUE
     *
     * @return bool
     *     Whether the data structure for Drupal Module definied Entities need
     *     updating to match the metadata contained within them.
     */
    public function entityUpdatesRequired(){
        $entityClasses = $this->entityClassProvider->getEntityClasses();

        if(empty($entityClasses)){
            return FALSE;
        }

        return (!empty($this->entityQueryGenerator->getUpdateQueries($entityClasses)));
    }

    /**
     * Get the Entity removal queries that belong to a Drupal Module using its
     * key.
     *
     * @param string $moduleKey
     *     The key of the Module to fetch the Entity removal queries for.
     *
     * @return array
     *     A flat of array of Entity removal query statements of an empty array
     *     if no removal queries were found for the passed Module.
     */
    private function getEntityRemovalQueries($moduleKey){
        $removalQueries = db_select($this->removalQueryStorageTableName, 't');
        $removalQueries->addField('t', $this->removalQueryStorageQueriesColumn);
        $removalQueries->condition($this->removalQueryStorageModuleColumn, $moduleKey);
        $removalQueries = $removalQueries->execute();

        if($removalQueries->rowCount() == 0){
            return array();
        }

        $removalQueriesData = $removalQueries->fetchAssoc();
        return unserialize($removalQueriesData[$this->removalQueryStorageQueriesColumn]);
    }

    /**
     * Set a Module's Entity removal queries to the storage database table.
     *
     * Using the Module's key and an array of removal queries merges these into
     * the database table. If removal queries are already stored against the
     * passed module key these will be replaced by those provided during this
     * method call.
     *
     * @param string $moduleKey
     *     The key of the Module the removal queries belong to.
     * @param array $removalQueries
     *     A flat array of removal query statements.
     *
     * @return $this
     */
    private function setEntityRemovalQueries($moduleKey, array $removalQueries){
        db_merge($this->removalQueryStorageTableName)
            ->key(array($this->removalQueryStorageModuleColumn => $moduleKey))
            ->fields(array(
                $this->removalQueryStorageQueriesColumn => serialize($removalQueries)
            ))
            ->execute();

        return $this;
    }

    /**
     * Delete any stored Entity removal queries for the passed Module using it's
     * key
     *
     * @param string $moduleKey
     *     The key of the Module the removal queries belong to.
     *
     * @return $this
     */
    private function deleteEntityRemovalQueries($moduleKey){
        db_delete($this->removalQueryStorageTableName)
            ->condition($this->removalQueryStorageModuleColumn, $moduleKey)
            ->execute();

        return $this;
    }

}