<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Action\AbstractDrupalModuleFileAction Class.
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Action;

use RuralStack\Common\Action\Exception\ModuleActionUnsupportedModuleException;
use RuralStack\Common\Module\ModuleInterface;
use System\Action\AbstractFileAction;
use System\FileSystem\FileSystem;

/**
 * Class AbstractDrupalModuleFileAction.
 *
 * Base action class for actions that require drupal module information
 * aswell as the ability to access and manipulate the file system.
 *
 * @package RuralStack\Common\Action
 */
abstract class AbstractDrupalModuleFileAction extends AbstractFileAction {

    /**
     * The local module object.
     *
     * @var \RuralStack\Common\Module\ModuleInterface
     */
    protected $module;

    /**
     * AbstractDrupalModuleFileAction constructor.
     *
     * Set the module object for use by extending actions. Use the parents
     * constructor to initialise the file system object for use by extending
     * actions.
     *
     * @param ModuleInterface $module
     * @param FileSystem      $fileSystem
     *
     * @throws ModuleActionUnsupportedModuleException
     */
    public function __construct(ModuleInterface $module, FileSystem $fileSystem){
        if(!empty($this->supportedModules()) && !in_array($module->getMachineName(), $this->supportedModules())){
            throw new ModuleActionUnsupportedModuleException($this, $module, $this->supportedModules());
        }
        $this->module = $module;
        parent::__construct($fileSystem);
    }

    /**
     * Get the supported modules for the action.
     *
     * Return an array of module key's the action is supported for. To
     * support all modules return an empty array.
     *
     * @return array
     */
    abstract protected function supportedModules();

}