<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Doctrine\RSEntityManager Class
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Doctrine\Events;

use RuralStack\Common\RSC\Config\RSCConfigConstants;
use Symfony\Component\Console\ConsoleEvents;
use Symfony\Component\Console\Event\ConsoleCommandEvent;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class MigrationsEventSubscriber.
 *
 * Provides an Event Subscriper for the Doctrine CLI Application provided by
 * this Module.
 *
 * @package RuralStack\Common\Doctrine\Events
 */
class MigrationsEventSubscriber implements EventSubscriberInterface{

    /**
     * Event subscriptions.
     *
     * Define the events this class is subscribed to.
     *
     * @return array Subscriptions
     */
    public static function getSubscribedEvents(){
        return array(
            ConsoleEvents::COMMAND => 'migrationCommandOptions'
        );
    }

    /**
     * Provide a Command Event listener.
     *
     * Capture any Doctrine Migrations commands and add in a default value for
     * the configuration options: --configuration, --filter-expression if no
     * value has been set against them.
     *
     * Get the options attached to the command so that any user provided values
     * are not lost. Check if the '--configuration' and '--filter-expression'
     * options exist, if they do and dont have a value set against them provide
     * a default.
     *
     * Rebuild the command creating an ArrayInput from the built $inputOptions
     * array (inputs can be provided from user or a method set default), add
     * these to the original command and rerun it.
     *
     * @param ConsoleCommandEvent $event
     *     The Event object attached to all Console Commands being ran.
     *
     * @return bool
     *     If the method intercepted a Doctrine Migrations command or not.
     */
    public function migrationCommandOptions(ConsoleCommandEvent $event){
        $command = $event->getCommand();

        if(strpos($command->getName(), 'migrations:') === FALSE){
            return FALSE;
        }

        $event->disableCommand();

        $inputOptions = $event->getInput()->getOptions();
        foreach($inputOptions as $inputKey => $inputValue){
            unset($inputOptions[$inputKey]);
            $inputOptions['--'.$inputKey] = $inputValue;
        }

        if($command->getDefinition()->hasOption('configuration') && empty($inputOptions['--configuration'])){
            $inputOptions['--configuration'] = RSCConfigConstants::RSC_DOCTRINE_MIGRATIONS_CONFIG_FILE_FINAL_LOCATION;
        }

        if($command->getDefinition()->hasOption('filter-expression') && empty($inputOptions['--filter-expression'])){
            $inputOptions['--filter-expression'] = '/^'.RSCConfigConstants::RSC_DOCTRINE_ENTITY_TABLE_PREFIX.'/';
        }

        $input = new ArrayInput($inputOptions);
        $command->run($input, $event->getOutput());
        return TRUE;
    }

}