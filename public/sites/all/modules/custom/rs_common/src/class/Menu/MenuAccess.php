<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Menu\MenuAccess Class.
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Menu;

/**
 * Class MenuAccess.
 *
 * Provides methods for use within menu definitions of Drupal modules.
 *
 * @package RuralStack\Common\Menu
 */
class MenuAccess {

    /**
     * Check whether the current user has the passed permissions.
     *
     * An unlimited number of parameters (in this case permission keys) can
     * be passed to this method which are fetched using the func_get_args()
     * call. For every permission passed a check is made against the current
     * user to see if they have the permission, if they dont, false is
     * returned and this method called is ended in a failed state. If they do
     * the however next permission is checked untill all have been checked.
     * If all permissions are checked and passed this method call passes and
     * true is returned to the caller.
     *
     * @return bool whether use required permissions
     */
    public static function userHasPermissions(){
        if(sizeof(func_get_args()) == 0){
            return FALSE;
        }
        foreach(func_get_args() as $permission){
            if(!user_access($permission)){
                return FALSE;
            }
        }
        return TRUE;
    }

}