<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Site\ActiveSiteTrait Trait.
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Site;

/**
 * Class ActiveSiteTrait.
 *
 * Get the current path to the active site within a multisite instance. If
 * used within a one site drupal core 'sites/default' will be returned.
 *
 * @package RuralStack\Common\Site
 */
trait ActiveSiteTrait {

    /**
     * Get the path to active site from the drupal web root directory.
     *
     * @return string
     */
    protected function getActiveSitePath(){
        return DRUPAL_WEB_ROOT.'/'.conf_path();
    }

}