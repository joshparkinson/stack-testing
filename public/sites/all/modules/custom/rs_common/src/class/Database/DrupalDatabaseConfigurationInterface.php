<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Database\DrupalDatabaseConfigurationInterface Interface.
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Database;

/**
 * Interface DrupalDatabaseConfigurationInterface
 *
 * Defines methods a drupal database configuration classes must implement to
 * extra the relevant configuration data.
 *
 * @package RuralStack\Common\Database
 */
interface DrupalDatabaseConfigurationInterface {

    /**
     * Get the database driver
     *
     * @return string
     */
    public function getDriver();

    /**
     * Get the database.
     *
     * Returns:
     * - A database name for 'mysql/pgsql' configurations
     * - Path to a database file for 'sqlite' configurations
     *
     * @return string
     */
    public function getDatabase();

    /**
     * Get the database username.
     *
     * Returns NULL for 'sqlite' configurations
     *
     * @return string|null
     */
    public function getUsername();

    /**
     * Get the database password.
     *
     * Returns NULL for 'sqlite' configurations
     *
     * @return string|null
     */
    public function getPassword();

    /**
     * Get the database host.
     *
     * Returns NULL for 'sqlite' configurations
     *
     * @return string|null
     */
    public function getHost();

    /**
     * Get the database port.
     *
     * Returns NULL for 'sqlite' configurations
     *
     * @return int|null
     */
    public function getPort();

    /**
     * Get the database table prefix value
     *
     * Returns NULL for 'sqlite' configurations
     *
     * If a specific prefix is requested but not found NULL should be
     * returned. If no specific prefix is requested return the default prefix
     * if its set else return NULL.
     *
     * @param string $prefixKey The specific prefix to get
     *
     * @return string|null
     */
    public function getPrefix($prefixKey = NULL);

    /**
     * Get the database charset
     *
     * Returns NULL for 'sqlite' configurations or 'mysql/pgsql'
     * configurations that dont define this value.
     *
     * @return string|null.
     */
    public function getCharset();

    /**
     * Get the database collation
     *
     * Returns NULL for 'sqlite' configurations or 'mysql/pgsql'
     * configurations that dont define this value.
     *
     * @return string|null.
     */
    public function getCollation();

}