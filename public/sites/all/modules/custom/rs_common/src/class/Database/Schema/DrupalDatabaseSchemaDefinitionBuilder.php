<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Database\Schema\DrupalDatabaseSchemaDefinitionBuilder Class.
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Database\Schema;

/**
 * Class DrupalDatabaseSchemaDefinitionBuilder
 *
 * @package RuralStack\Common\Database\Schema
 */
class DrupalDatabaseSchemaDefinitionBuilder implements DrupalSchemaDefinitionInterface {

    /**
     * Storage of DrupalSchemaDefinition objects for use in the generation of
     * a merged schema definiotion.
     *
     * @var DrupalSchemaDefinitionInterface[]
     */
    private $drupalSchemaDefinitions = array();

    /**
     * Add a DrupalSchemaDefintion object to be used in building a merged
     * schema definition.
     *
     * @param DrupalSchemaDefinitionInterface $drupalSchemaDefinition
     */
    public function addSchemaDefinition(DrupalSchemaDefinitionInterface $drupalSchemaDefinition){
        $this->drupalSchemaDefinitions[] = $drupalSchemaDefinition;
    }

    /**
     * Get the merged schema definition array.
     *
     * Loop over all of the DrupalSchemaDefinition objects extract their schema
     * definition and merging into a multitable schema definition array.
     *
     * Note: If multiple DrupalSchemaDefinition objects define the same table
     * name (they shouldn't!) then the last object processed using the duplicate
     * name will have it's definition defined whilst the others will be lost.
     *
     * @return array The table schema definition
     */
    public function getSchemaDefinition(){
        $schemaDefinition = array();
        foreach($this->drupalSchemaDefinitions as $drupalSchemaDefinition){
            $schemaDefinition = array_replace(
                $schemaDefinition,
                $drupalSchemaDefinition->getSchemaDefinition()
            );
        }
        return $schemaDefinition;
    }

}