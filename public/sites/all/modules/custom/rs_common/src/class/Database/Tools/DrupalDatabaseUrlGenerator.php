<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Database\Tools\DrupalDatabaseUrlGenerator Class
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Database\Tools;

use RuralStack\Common\Database\DrupalDatabaseConfigurationInterface;
use RuralStack\Common\Database\Exception\DrupalDatabaseConfigurationInvalidDatabaseDriverException;
use System\Database\Tools\DatabaseUrlGeneratorInterface;

/**
 * Class DrupalDatabaseUrlGenerator.
 *
 * Provides methods for generating a database url from a Drupal databse
 * configuration
 *
 * @package RuralStack\Database\Tools
 */
class DrupalDatabaseUrlGenerator implements DatabaseUrlGeneratorInterface {

    /**
     * The local DrupalDatabaseConfigurationInterface implementing object.
     *
     * @var DrupalDatabaseConfigurationInterface;
     */
    private $drupalDbConfig;

    /**
     * DrupalDatabaseUrlGenerator constructor.
     *
     * Initialise the generator, set the configuration object.
     *
     * @param DrupalDatabaseConfigurationInterface $drupalDbConfig
     */
    public function __construct(DrupalDatabaseConfigurationInterface $drupalDbConfig){
        $this->drupalDbConfig = $drupalDbConfig;
    }

    /**
     * Generate the database url.
     *
     * Depending on the configurations driver value fire the necessary driver
     * specific url builder.
     *
     * @return string The processed/built datbase url
     *
     * @throws DrupalDatabaseConfigurationInvalidDatabaseDriverException
     */
    public function generateUrl(){
        switch($this->drupalDbConfig->getDriver()){
            case 'mysql':
            case 'pgsql':
                return $this->generateMysqlPgsqlUrl();
            case 'sqlite':
                return $this->generateSqliteUrl();
            default:
                throw new DrupalDatabaseConfigurationInvalidDatabaseDriverException($this->drupalDbConfig->getDriver());
        }
    }

    /**
     * Generate a mysql/pgsql database url.
     *
     * Define a tokenised version of the connection url structure. Populate
     * the url using values from the Drupal database configuration. Add in a
     * emjoi friendly charset query parameter if required. Return built url
     * to generateUrl() method to return to user.
     *
     * @return string The built mysql/pgsql url
     */
    private function generateMysqlPgsqlUrl(){
        $tokenUrl = '[driver]://[username]:[password]@[host]:[port]/[databaseName]';

        $databaseUrl = strtr($tokenUrl, array(
            '[driver]'       => $this->drupalDbConfig->getDriver(),
            '[username]'     => $this->drupalDbConfig->getUsername(),
            '[password]'     => $this->drupalDbConfig->getPassword(),
            '[host]'         => $this->drupalDbConfig->getHost(),
            '[port]'         => $this->drupalDbConfig->getPort(),
            '[databaseName]' => $this->drupalDbConfig->getDatabase(),
        ));

        if(strtolower($this->drupalDbConfig->getCharset()) == 'utf8mb4'){
            $databaseUrl .= '?charset=UTF8MB4';
        }

        return $databaseUrl;
    }

    /**
     * Generate a sqlite database url.
     *
     * Define a tokenised version of the connection url structure. Return a
     * populated connection url using values from the Drupal database
     * configuration.
     *
     * @return string The built sqlite url
     */
    private function generateSqliteUrl(){
        $tokenUrl = '[driver]://[databaseFilePath]';

        return strtr($tokenUrl, array(
            '[driver]' => $this->drupalDbConfig->getDriver(),
            '[databaseFilePath]' => $this->drupalDbConfig->getDatabase()
        ));
    }


}