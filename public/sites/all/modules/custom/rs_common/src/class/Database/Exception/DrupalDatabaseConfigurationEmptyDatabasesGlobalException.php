<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Database\Exception\DrupalDatabaseConfigurationEmptyDatabasesGlobalException Exception.
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Database\Exception;

use Exception;

/**
 * Class DrupalDatabaseConfigurationEmptyDatabasesGlobalException.
 *
 * Thrown when the Drupal's database definition is attempted to be fetched
 * from the global variable $databases but could not be found during the
 * creation of a DrupalDatabaseConfiguration object.
 *
 * @package RuralStack\Common\Database\Exception
 */
class DrupalDatabaseConfigurationEmptyDatabasesGlobalException extends Exception { }