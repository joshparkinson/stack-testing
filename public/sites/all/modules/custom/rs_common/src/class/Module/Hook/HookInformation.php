<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Module\Hook\HookInformation Class.
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Module\Hook;

/**
 * Class HookInformation.
 *
 * Contains methods capable of extracting data from Drupal's hook api for the
 * current system.
 *
 * @package RuralStack\Common\Module\Hook
 */
class HookInformation implements HookInformationInterface {

    /**
     * Get all currently available hook's from the Drupal system.
     *
     * @return array
     *    An associative array whose keys are hook names and whose values are
     *    an associative array containing a group name
     */
    public function getHookInformation(){
        return module_hook_info();
    }

    /**
     * Get all Drupal modules currently implementing a passed hook.
     *
     * @param string $hookName
     *     The name of the hook.
     *
     * @return array
     *     An array with the names of the modules which are implementing this
     *     hook.
     */
    public function getHookImplementations($hookName){
        return module_implements($hookName);
    }

    /**
     * Update the cache in which Hook module implementation data is stored.
     *
     * This cache is used to produce the results of the getHookImplementations
     * method.
     *
     * @see HookInformation::getHookImplementations()
     *
     * @return $this
     */
    public function updateHookImplementationsCache(){
        module_implements_write_cache();

        return $this;
    }

}