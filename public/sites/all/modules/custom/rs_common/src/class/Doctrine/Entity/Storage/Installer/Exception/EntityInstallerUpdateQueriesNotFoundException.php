<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Doctrine\Entity\Storage\Query\EntityQueryGenerator Class
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Doctrine\Entity\Storage\Installer\Exception;

use Exception;

/**
 * Class EntityInstallerUpdateQueriesNotFoundException.
 *
 * Thrown when an attempt is made to update the data structure for Doctrine
 * Entities defined by Drupal Module's but no update queries could found.
 *
 * If thrown as part of a global Entity update this could mean that no currently
 * enabled Drupal Module's implement the Doctrine ORM package and do not provide
 * any Entities.
 *
 * @package RuralStack\Common\Doctrine\Entity\Storage\Installer\Exception
 */
class EntityInstallerUpdateQueriesNotFoundException extends Exception { }