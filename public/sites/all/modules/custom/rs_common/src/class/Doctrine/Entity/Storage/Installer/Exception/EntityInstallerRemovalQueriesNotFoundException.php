<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Doctrine\Entity\Storage\Installer\Exception\EntityInstallerRemovalQueriesNotFoundException Exception.
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Doctrine\Entity\Storage\Installer\Exception;

use Exception;

/**
 * Class EntityInstallerRemovalQueriesNotFoundException.
 *
 * Thrown when during an uninstallation action for a Drupal Module's Doctrine
 * Entities no Entity removal queries could be found that are associated
 * with it.
 *
 * @package RuralStack\Common\Doctrine\Entity\Storage\Installer\Exception
 */
class EntityInstallerRemovalQueriesNotFoundException extends Exception { }