<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Action\Exception\ModuleActionUnsupportedModuleException Exception.
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Action\Exception;

use Exception;
use RuralStack\Common\Module\ModuleInterface;
use System\Action\ActionInterface;

/**
 * Class ModuleActionUnsupportedModuleException.
 *
 * Thrown when a module action is constructed with an unsupported module.
 *
 * @package RuralStack\Common\Action\Exception
 */
class ModuleActionUnsupportedModuleException extends Exception{

    /**
     * ModuleActionUnsupportedModuleException constructor.
     *
     * Construct the exception message using the action attempted to be created.
     * The module passed to the action on construction/creation, and the actions
     * supported modules.
     *
     * @param ActionInterface $action           The action being created
     * @param ModuleInterface $recievedModule   The unsupported module
     * @param array           $supportedModules An array of supported module keys
     */
    public function __construct(ActionInterface $action, ModuleInterface $recievedModule, array $supportedModules){
        parent::__construct(
            strtr(
                'Could not create action :actionName. Unsupported module \':moduleKey\' used. Supported modules: \':supported\'',
                array(
                    ':actionName' => get_class($action),
                    ':moduleKey'  => $recievedModule->getMachineName(),
                    ':supported'  => implode(', ', $supportedModules)
                )
            )
        );
    }

}