<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Module\Hook\HookInformationInterface Interface.
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Module\Hook;


/**
 * Interface HookInformationInterface.
 *
 * Defines methods that object retreiving hook information from Drupal must
 * implement.
 *
 * @package RuralStack\Common\Module\Hook
 */
interface HookInformationInterface {

    /**
     * Get all currently available hook's from the Drupal system.
     *
     * @return array
     *    An associative array whose keys are hook names and whose values are
     *    an associative array containing a group name
     */
    public function getHookInformation();

    /**
     * Get all Drupal modules currently implementing a passed hook.
     *
     * @param string $hookName
     *     The name of the hook.
     *
     * @return array
     *     An array with the names of the modules which are implementing this
     *     hook.
     */
    public function getHookImplementations($hookName);
}