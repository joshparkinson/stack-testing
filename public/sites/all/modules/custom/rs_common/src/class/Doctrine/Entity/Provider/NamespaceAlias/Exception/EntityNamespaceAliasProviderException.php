<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Doctrine\Entity\Provider\NamespaceAlias\Exception\EntityNamespaceAliasProviderException Class.
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Doctrine\Entity\Provider\NamespaceAlias\Exception;

use Exception;

/**
 * Class EntityNamespaceAliasProviderException
 *
 * Thrown when an EntityNamespaceAliasProvider is unable to provide
 * Entity Namespace Aliases using one of it's access methods.
 *
 * @package RuralStack\Common\Doctrine\Entity\Provider\Path\Exception
 */
class EntityNamespaceAliasProviderException extends Exception { }