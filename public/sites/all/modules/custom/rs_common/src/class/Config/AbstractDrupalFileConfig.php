<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Config\AbstractDrupalFileConfig.
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Config;

use RuralStack\Common\Site\ActiveSiteTrait;
use System\Config\AbstractFileConfig;

/**
 * Class AbstractDrupalFileConfig.
 *
 * Extends the AbstractFileConfig class to provide a Drupal specific context
 * to configuration file loading. This class allows Drupal sites to contain
 * their own site specific configuration files and Drupal specific configuation
 * classed to provide coniguration resource file paths relative to a Drupal
 * active site directory. This is achieved by implementing the
 * getResourceFilePath method directly and appending the currently active
 * Drupal sites directory to the path recieved by the extending Drupal specific
 * configuration class (as set via the abstract method
 * getDrupalSiteConfigFilePath method).
 *
 * Uses the ActiveSiteTrait to enable usage of methods to get information on
 * the currently active Drupal site.
 *
 * @package RuralStack\Common\Config
 */
abstract class AbstractDrupalFileConfig extends AbstractFileConfig {

    use ActiveSiteTrait;

    /**
     * Return the full resource file path back to the parent AbstractFileConfig
     * class.
     *
     * This method appends the currently active Drupal site's file path to the
     * recieved configuration file path from the extending class.
     *
     * Example:
     *  - Active site directory: mysite.com
     *  - Received file path:    config/configFile.yml
     *  - Path sent to parent:   /sites/mysite.com/config/configFile.yml
     *
     * @return string
     */
    protected function getResourceFilePath(){
        return $this->getActiveSitePath().'/'.$this->getDrupalSiteConfigFilePath();
    }

    /**
     * Get the configuration file path relative to a Drupal site installation
     * path.
     *
     * @return string
     */
    abstract protected function getDrupalSiteConfigFilePath();

}