<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\RSC\Config\RSCConfigConstants Class.
 *
 * Note: Classes under the RuralStack\Common\RSC base namepsaces are not intended for
 * use outside of this module. They are specific to this module and are to be used
 * within it only.
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\RSC\Config;

/**
 * Class RSCConfigConstants.
 *
 * Contains all configuration constants required in the functionality
 * of the module and the classes provided by Rural Stack: Common
 *
 * This class can not and does not need to be instantiated.
 *
 * @package RuralStack\Common\RSC\Config
 */
final class RSCConfigConstants {

    /**
     * Remove automatically created files on module uninstall.
     *
     * The following constant defines the variable key/configuration setting that
     * dictates whether this module should remove it's automatically created files
     * during it's uninstall process.
     */
    const RSC_VAR_DELETE_FILES_UNINSTALL_SETTING = 'rs_common_delete_files_uninstall';

    /**
     * Use the Doctrine package in development mode.
     *
     * The following constant defines the variable key/configuration setting that
     * dicates whether this module should run it's Doctrine package in development
     * mode or not.
     */
    const RSC_VAR_DOCTRINE_DEV_MODE_SETTING = 'rs_common_doctrine_dev_mode';

    /**
     * Remove Doctrine Migration classes on module uninstall.
     *
     * The following constant defines the variable key/configuration setting that
     * dictates whether or not this module should remove any Migration classes
     * created by the Doctrine package during this module's uninstall process.
     */
    const RSC_VAR_DOCTRINE_REMOVE_MIGRATIONS_UNINSTALL_SETTING = 'rs_common_doctrine_remove_migrations_uninstall';

    /**
     * Remove Doctrine Proxy classes on module uninstall.
     *
     * The following constant defines the variable key/configuration setting that
     * dictates whether or not this module should remove any Proxy classes
     * created by the Doctrine package during this module's uninstall process.
     */
    const RSC_VAR_DOCTRINE_REMOVE_PROXIES_UNINSTALL_SETTING = 'rs_common_doctrine_remove_proxies_uninstall';

    /**
     * Doctrine Entity Path Definition Hook Name.
     *
     * The following constant defines the name of the hook used by this module
     * to gather Doctrine Entity path definitions from any modules that
     * implement it.
     */
    const RSC_HOOK_DOCTRINE_ENTITY_PATHS = 'rs_doctrine_entity_paths';

    /**
     * Doctrine Enity Namespace Definition Hook Name.
     *
     * The following constant defines the name of the hook used by this module
     * to gather Doctrine Namespace definitions from any modules that implement
     * it.
     */
    const RSC_HOOK_DOCTRINE_ENTITY_NAMESPACES = 'rs_doctrine_entity_namespaces';

    /**
     * Doctrine Entity Remove Query Storage Database Table: Table Name.
     *
     * The following constant defines the name of the database table that is
     * used to store the removal queries for the Entities defined (created) by
     * Modules that implement the Doctrine ORM package.
     */
    const RSC_DOCTRINE_ENTITY_REMOVAL_QUERIES_TABLE_NAME = 'rscd_entity_removal';

    /**
     * Doctrine Enity Remove Query Storage Database Table: Module Column Name
     *
     * The following constant defines the name fo the column that is used to
     * store the Drupal Module machine name against the removal queries for the
     * Entities defined (created) by it.
     */
    const RSC_DOCTRINE_ENTITY_REMOVAL_QUERIES_TABLE_MODULE_COLUMN_NAME = 'module';

    /**
     * Doctrine Enity Remove Query Storage Database Table: Queries Column Name
     *
     * The following constant defines the name fo the column that is used to
     * store the removal queries against the Drupal Module machine name which
     * defined (created) the Entites they belong to.
     */
    const RSC_DOCTRINE_ENTITY_REMOVAL_QUERIES_TABLE_QUERIES_COLUMN_NAME = 'queries';

    /**
     * Doctrine Entity Table Prefix.
     *
     * The following constant defines the prefix that is silently applied to
     * all Doctrine Entity database table definitions. For example this setting
     * is used to narrow (or seperate) Entity data tables from Drupal core
     * and contrib tables during the generation of Doctrine Migration classes.
     */
    const RSC_DOCTRINE_ENTITY_TABLE_PREFIX = 'rscd_et_';

    /**
     * Default Doctrine CLI Entry Point file location.
     *
     * The following constant defines the path to the default Doctrine CLI
     * Entry point file relative to the base directory of this module.
     */
    const RSC_DOCTRINE_CLI_FILE_DEFAULT_LOCATION = 'files/doctrine-cli.default.php';

    /**
     * Final Doctrine CLI Entry Point file location.
     *
     * The following constant defines the final path of the Doctrine CLI Entry
     * point file relative to the installing 'sites' directory.
     *
     * Example: /sites/default => sites/default/doctrine-cli.php
     */
    const RSC_DOCTRINE_CLI_FILE_FINAL_LOCATION = 'doctrine-cli.php';

    /**
     * Default Doctrine Entity Configuration file location.
     *
     * The following constant defines the default path to the Doctrine Entity
     * Configuration file relative to this module's base directory.
     */
    const RSC_DOCTRINE_ENTITY_CONFIG_FILE_DEFAULT_LOCATION = 'files/doctrine.entities.default.yml';

    /**
     * Final Doctrine Entity Configuration file location.
     *
     * The following constant defines the final path to the Doctrine Entity
     * Configuration file relative to the install 'sites' directory.
     *
     * Example: /sites/default => /sites/default/config/doctrine.entities.yml
     */
    const RSC_DOCTRINE_ENTITY_CONFIG_FILE_FINAL_LOCATION = 'config/doctrine.entities.yml';

    /**
     * Default Doctrine Migrations Configuration file location.
     *
     * The following constant defines the default path to the Doctrine Migrations
     * Configuration file relative to this module's base directory.
     */
    const RSC_DOCTRINE_MIGRATIONS_CONFIG_FILE_DEFAULT_LOCATION = 'files/doctrine.migrations.default.yml';

    /**
     * Final Doctrine Migrations Configuration file location.
     *
     * The following constant defines the final path to the Doctrine Migrations
     * Configuration file relative to the install 'sites' directory.
     *
     * Example: /sites/default => /sites/default/config/doctrine.migrations.yml
     */
    const RSC_DOCTRINE_MIGRATIONS_CONFIG_FILE_FINAL_LOCATION = 'config/doctrine.migrations.yml';

    /**
     * Doctrine Migration Class storage directory.
     *
     * The following constant defines the path to the directory which will store
     * the generated Doctrine Migration classes relative to the install
     * 'sites' directory.
     *
     * Example: /sites/default => /sites/default/doctrine/migrations
     */
    const RSC_DOCTRINE_MIGRATIONS_CLASS_STORAGE_DIR = 'doctrine/migrations';

    /**
     * Doctrine Proxy Class storage directory.
     *
     * The following constant defines the path to the directory which will store
     * the generated Doctrine Proxy classes relative to the install
     * 'sites' directory.
     *
     * Example: /sites/default => /sites/default/doctrine/proxies
     */
    const RSC_DOCTRINE_PROXY_CLASS_STORAGE_DIR = 'doctrine/proxies';

}