<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Doctrine\Entity\Provider\EntityClass\EntityClassProviderInterface Interface
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Doctrine\Entity\Provider\EntityClass;

use RuralStack\Common\Doctrine\Entity\Provider\NamespaceAlias\Exception\EntityNamespaceAliasProviderException;
use RuralStack\Common\Module\ModuleInterface;

/**
 * Interface EntityClassProviderInterface.
 *
 * Defines methods that must be implemented by class that provide loadable
 * Entity FQCN's to the caller.
 *
 * @package RuralStack\Common\Doctrine\Entity\Provider\EntityClass
 */
interface EntityClassProviderInterface {

    /**
     * Get the Doctrine ORM Entity Class FQCN's.
     *
     * @return array
     */
    public function getEntityClasses();

    /**
     * Get the Doctrine ORM Entity Class FQCN's from the passed Module
     *
     * @param ModuleInterface $module
     *
     * @return array
     *
     * @throws EntityNamespaceAliasProviderException
     */
    public function getEntityClassesFromModule(ModuleInterface $module);

}