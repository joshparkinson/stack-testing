<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Doctrine\Entity\Provider\NamespaceAlias\EntityNamespaceAliasProviderInterface Interface
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Doctrine\Entity\Provider\NamespaceAlias;

use RuralStack\Common\Doctrine\Entity\Provider\NamespaceAlias\Exception\EntityNamespaceAliasProviderException;
use RuralStack\Common\Module\ModuleInterface;

/**
 * Interface EntityNamespaceAliasProviderInterface.
 *
 * Defines methods that must be implemented by classed that provided Entity
 * Namespace Aliases for Doctrine ORM Entities.
 *
 * @package RuralStack\Common\Doctrine\Entity\Provider\NamespaceAlias
 */
interface EntityNamespaceAliasProviderInterface {

    /**
     * Get the Namespace Aliases.
     *
     * This method must return an array of full namespace values key'd by the
     * alias in which they can be accessed from also.
     *
     * @return array
     */
    public function getNamespaceAliases();

    /**
     * Get the Namespace Aliases from a Drupal Module
     *
     * This method must return an array of full namespace values key'd by the
     * alias in which they can be accessed from also.
     *
     * @param ModuleInterface $module
     *
     * @return array
     *
     * @throws EntityNamespaceAliasProviderException
     */
    public function getNamespaceAliasesFromModule(ModuleInterface $module);

}