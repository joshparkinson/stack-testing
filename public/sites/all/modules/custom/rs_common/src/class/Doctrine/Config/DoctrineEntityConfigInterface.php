<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Doctrine\Config\DoctrineEntityConfigInterface Interface.
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Doctrine\Config;

use Doctrine\Common\Cache\Cache;

/**
 * Interface DoctrineEntityConfigInterface
 *
 * Defines required method a DoctrineEntityConfig class must implement
 * to extract it's configuration data.
 *
 * @package RuralStack\Common\Doctrine\Config
 */
interface DoctrineEntityConfigInterface {

    /**
     * Get the Doctrine metadate cache driver.
     *
     * Must return fully qualified class name of a class that implements the
     * Doctrine\Common\Cache\Cache Interface.
     *
     * @return Cache
     */
    public function getCacheMetadata();

    /**
     * Get the Doctrine query cache driver.
     *
     * Must return fully qualified class name of a class that implements the
     * Doctrine\Common\Cache\Cache Interface.
     *
     * @return Cache
     */
    public function getCacheQuery();

    /**
     * Get the Doctrine result cache driver.
     *
     * Must return fully qualified class name of a class that implements the
     * Doctrine\Common\Cache\Cache Interface.
     *
     * @return Cache
     */
    public function getCacheResult();

    /**
     * Get the path to the directory that will act as storage for the
     * generated Doctrine proxy classes.
     *
     * @return string
     */
    public function getProxyDir();

    /**
     * Get the namespace to store the generated Doctrine proxy classes under.
     *
     * @return string
     */
    public function getProxyNamespace();

}