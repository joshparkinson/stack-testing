<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Doctrine\Extensions\TablePrefixExtension Class
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Doctrine\Extensions;

use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use RuralStack\Common\RSC\Config\RSCConfigConstants;

/**
 * Class TablePrefixExtension.
 *
 * Provides an Extenstion that can be used to automatically add a database
 * table name prefix to defined Doctrine Entities. Triggered on the Event of
 * getting Class metadata from the Doctrine EntityManager class.
 *
 * @package RuralStack\Common\Doctrine\Extensions
 */
class TablePrefixExtension {

    /**
     * The table prefix value
     *
     * @var string
     */
    protected $prefix;

    /**
     * TablePrefixExtension constructor.
     *
     * Set the table prefix value to the object. This Module (rs_common)
     * provides a default hard set prefix that will be used but if needed by
     * the caller an extension of this can be provided as construction
     * parameter which will be appended to the default provided.
     *
     * @param string|null $prefix A prefix extension if required
     */
    public function __construct($prefix = NULL){
        $this->prefix = RSCConfigConstants::RSC_DOCTRINE_ENTITY_TABLE_PREFIX.$prefix;
    }

    /**
     * Load Class Metadat Event Listener.
     *
     * Attaches the defined table name prefix to the metadata for the Entity
     * class.
     *
     * @param LoadClassMetadataEventArgs $eventArgs
     */
    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs){
        /* @var ClassMetadata $classMetadata */
        $classMetadata = $eventArgs->getClassMetadata();

        if(!$classMetadata->isInheritanceTypeSingleTable() || $classMetadata->getName() === $classMetadata->rootEntityName){
            $classMetadata->setPrimaryTable(array('name' => $this->prefix.$classMetadata->getTableName()));
        }

        foreach($classMetadata->getAssociationMappings() as $fieldName => $mapping){
            if($mapping['type'] == ClassMetadataInfo::MANY_TO_MANY && $mapping['isOwningSide']){
                $mappedTableName = $mapping['joinTable']['name'];
                $classMetadata->associationMappings[$fieldName]['joinTable']['name'] = $this->prefix.$mappedTableName;
            }
        }
    }

}