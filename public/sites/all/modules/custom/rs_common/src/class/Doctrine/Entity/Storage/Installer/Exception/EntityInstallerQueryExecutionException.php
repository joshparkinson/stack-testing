<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Doctrine\Entity\Storage\Installer\Exception\EntityInstallerQueryExecutionException Exception.
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Doctrine\Entity\Storage\Installer\Exception;

use Exception;

/**
 * Class EntityInstallerQueryExecutionException.
 *
 * Thrown when during an installation action for a Drupal Modules's Doctine
 * Entities database queries are attempted to be run but did not complete
 * successfully.
 *
 * @package RuralStack\Common\Doctrine\Entity\Storage\Installer\Exception
 */
class EntityInstallerQueryExecutionException extends Exception { }