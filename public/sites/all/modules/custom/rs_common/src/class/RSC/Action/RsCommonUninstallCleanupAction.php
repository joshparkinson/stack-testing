<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\RSC\Action\RsCommonUninstallCleanupAction Class
 *
 * Note: Classes under the RuralStack\Common\RSC base namepsaces are not intended for
 * use outside of this module. They are specific to this module and are to be used
 * within it only.
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\RSC\Action;

use RuralStack\Common\RSC\Config\RSCConfigConstants;
use RuralStack\Common\Site\ActiveSiteTrait;
use System\Action\AbstractFileAction;

/**
 * Class RsCommonUninstallCleanupAction.
 *
 * Contains the definition for the Rural Stack: Common uninstallation
 * cleanup action.
 *
 * Uses the ActiveSiteTrait to enable usage of methods to get information on
 * the currently active Drupal site.
 *
 * @package RuralStack\Common\RSC\Action
 */
class RsCommonUninstallCleanupAction extends AbstractFileAction {

    use ActiveSiteTrait;

    /**
     * Messages specific to each action carried out in the execution method.
     * Used in the build of the final returned action message.
     *
     * @var array
     */
    private $actionMessages = array();

    /**
     * Define the unique key for this action.
     *
     * @return string
     */
    public function key(){
        return 'rs_common_remove_auto_generated_files';
    }

    /**
     * Execute the cleanup action.
     *
     * The following action will clean up some of the files automatically
     * created during the Rural Stack: Common module installation.
     *
     * Action elements:
     * - Store and CHMOD active site directory to writeable 755
     * - Remove Doctrine CLI Entry Point, create action message
     * - Remove Doctrine Entity Configuration file, create action message
     * - Remove Doctrine Migrations Configuration file, create action message
     * - Remove active sites config directroy if empty, create action message
     * - Remove active sites doctrine direction if empty, create action message
     * - Revert active site directory chmod to original value.
     * - Remove Drupal variables associated with this action
     *
     * @return bool True on successful execution.
     */
    public function execute(){
        $origChmod = $this->fileSystem->getChmod($this->getActiveSitePath());
        $this->fileSystem->chmod($this->getActiveSitePath(), 0755);

        $this->fileSystem->remove($this->getActiveSitePath().'/'.RSCConfigConstants::RSC_DOCTRINE_CLI_FILE_FINAL_LOCATION);
        $this->actionMessages[] = 'Removed Doctrine CLI entry point file: <strong>'.RSCConfigConstants::RSC_DOCTRINE_CLI_FILE_FINAL_LOCATION.'</strong> from: '.$this->getActiveSitePath();

        $this->fileSystem->remove($this->getActiveSitePath().'/'.RSCConfigConstants::RSC_DOCTRINE_ENTITY_CONFIG_FILE_FINAL_LOCATION);
        $this->actionMessages[] = 'Removed Doctrine Entity Configuration file: <strong>'.RSCConfigConstants::RSC_DOCTRINE_ENTITY_CONFIG_FILE_FINAL_LOCATION.'</strong> from: '.$this->getActiveSitePath();

        $this->fileSystem->remove($this->getActiveSitePath().'/'.RSCConfigConstants::RSC_DOCTRINE_MIGRATIONS_CONFIG_FILE_FINAL_LOCATION);
        $this->actionMessages[] = 'Removed Doctrine Migrations Configuration file: <strong>'.RSCConfigConstants::RSC_DOCTRINE_MIGRATIONS_CONFIG_FILE_FINAL_LOCATION.'</strong> from: '.$this->getActiveSitePath();

        if($this->fileSystem->directoryIsEmpty($this->getActiveSitePath().'/config')){
            $this->fileSystem->remove($this->getActiveSitePath().'/config');
            $this->actionMessages[] = 'Removed empty directory: <strong>'.$this->getActiveSitePath().'/config</strong>';
        }

        if($this->fileSystem->directoryIsEmpty($this->getActiveSitePath().'/doctrine')){
            $this->fileSystem->remove($this->getActiveSitePath().'/doctrine');
            $this->actionMessages[] = 'Removed empty directory: <strong>'.$this->getActiveSitePath().'/doctrine</strong>';
        }

        $this->fileSystem->chmod($this->getActiveSitePath(), $origChmod);

        variable_del(RSCConfigConstants::RSC_VAR_DELETE_FILES_UNINSTALL_SETTING);
        variable_del(RSCConfigConstants::RSC_VAR_DOCTRINE_DEV_MODE_SETTING);
        variable_del(RSCConfigConstants::RSC_VAR_DOCTRINE_REMOVE_MIGRATIONS_UNINSTALL_SETTING);
        variable_del(RSCConfigConstants::RSC_VAR_DOCTRINE_REMOVE_PROXIES_UNINSTALL_SETTING);
        $this->actionMessages[] = 'Removed associated Drupal variable definitions';

        return TRUE;
    }

    /**
     * Return the success message for this action.
     *
     * @return string The action message
     */
    public function message(){
        return 'Cleaned up module files. <ul><li>'.implode('</li><li>', $this->actionMessages).'</li></ul>';
    }

}