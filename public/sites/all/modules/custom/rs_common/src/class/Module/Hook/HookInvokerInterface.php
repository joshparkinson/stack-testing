<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Module\Hook\HookInvokerInterface Interface.
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Module\Hook;

use RuralStack\Common\Module\ModuleCollectionInterface;
use RuralStack\Common\Module\ModuleInterface;
use RuralStack\Common\Module\Hook\Exception\HookInvocationException;

/**
 * Interface HookInvokerInterface.
 *
 * Defines methods that hook invoking classes must implement.
 *
 * @package RuralStack\Common\Module\Hook
 */
interface HookInvokerInterface {

    /**
     * Invoke a hook against a passed Module object.
     *
     * @param string $hookName
     *     The name of the hook to invoke.
     * @param ModuleInterface $module
     *     The Module object to invoke thie hook against.
     *
     * @return mixed
     *     The return value of the hook implementation or NULL if the passed
     *     Module does not implement the hook.
     */
    public function invoke($hookName, ModuleInterface $module);

    /**
     * Strictly invoke a hook against a passed Module object.
     *
     * If the passed Module object does not implement the hook to be invoked an
     * Exception (HookInvocationException) must be thrown.
     *
     * @param string $hookName
     *     The name of the hook to invoke.
     * @param ModuleInterface $module
     *     The Module object to invoke the hook against.
     *
     * @return mixed
     *     The return value of the hook implementation or NULL if the passed
     *     Module does not implement the hook.
     *
     * @throws HookInvocationException
     *     Thrown when the passed Module does not implement the hook.
     */
    public function invokeStrict($hookName, ModuleInterface $module);

    /**
     * Invoke a hook aginst a passed ModuleCollection.
     *
     * Invokes the passed hook against all Modules stored within a
     * ModuleCollection returning an array of merged hook invocation results.
     * If any of the Modules stored within the passed ModuleCollection do
     * not implement the passed hook they will not contribute anything to the
     * set of returned results. If non of the Modules stored within the passed
     * ModuleCollection implement the passed hook an empty array
     *
     * @param string $hookName
     *     The name of the hook to invoke.
     * @param ModuleCollectionInterface $moduleCollection
     *     The collection of Module's to invoke the hook against.
     *
     * @return mixed
     *     The return value of the hook implementation or an empty array if non
     *     of the Modules within the passed ModuleCollection implement this
     *     hook.
     */
    public function invokeMultiple($hookName, ModuleCollectionInterface $moduleCollection);

    /**
     * Strictly invoke a hook aginst a passed ModuleCollection.
     *
     * Invokes the passed hook against all Modules stored within a
     * ModuleCollection returning an array of merged hook invocation results.
     * If any of the Modules stored within the passed ModuleCollection do not
     * implement the passed hook an Exception (HookInvocationException) must be
     * thrown.
     *
     * @param string $hookName
     *     The name of the hook to invoke.
     * @param ModuleCollectionInterface $moduleCollection
     *     The collection of Module's to invoke the hook against.
     *
     * @return mixed
     *     The merged return values of the hook implementation for all of the
     *     Modules within the passed ModuleCollection.
     *
     * @throws HookInvocationException
     *     Thrown when the passed Module does not implement the hook.
     */
    public function invokeMultipleStrict($hookName, ModuleCollectionInterface $moduleCollection);

    /**
     * Invoke a hook for all enabled/implementing Modules.
     *
     * Invoke the passed hook against all enabled Modules that implement it.
     * If no Modules implement the passed hook then an empty array will be
     * returned by this method.
     *
     * @param string $hookName
     *     The name of the hook to invoke.
     *
     * @return array
     *     The merged return values for all the Modules that implement the
     *     passed hook or an empty array if no currently enabled Modules
     *     implement the hook.
     */
    public function invokeAll($hookName);

    /**
     * Strictly invoke a hook for all enabled/implement Modules.
     *
     * Invoke the passed hook against all enabled Modules that implement it.
     * If no Modules implement the passed hook then an Exception
     * (HookInvocationException) must be thrown.
     *
     * @param string $hookName
     *     The name of the hook to invoke.
     *
     * @return array
     *     The merged return values for all the Modules that implement the
     *     passed hook.
     *
     * @throws HookInvocationException
     *     Thrown when the passed Module does not implement the hook.
     */
    public function invokeAllStrict($hookName);

}