<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the RuralStack\Common\Database\DrupalDatabaseConfigurationInterface Interface.
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Common\Database;

use RuralStack\Common\Database\Exception\DrupalDatabaseConfigurationEmptyDatabasesGlobalException;
use RuralStack\Common\Database\Exception\DrupalDatabaseConfigurationInvalidDatabaseDriverException;
use System\Utility\Filter\ArrayFilter;

/**
 * Class DrupalDatabaseConfiguration
 *
 * Contains methods for extracting data from a specific Drupal database
 * configuration array as defined in the settings.php file.
 *
 * @package RuralStack\Common\Database
 */
class DrupalDatabaseConfiguration implements DrupalDatabaseConfigurationInterface {

    /**
     * The default myql port to use if no port is defined against a mysql
     * configuration.
     */
    const DEFAULT_MYSQL_PORT = 3306;

    /**
     * The default pgsql port to use if no port is defined against a pgsql
     * configuration.
     */
    const DEFAULT_PGSQL_PORT = 5432;

    /**
     * The local ArrayFilter object.
     *
     * @var ArrayFilter
     */
    private $arrayFilter;

    /**
     * The database driver.
     *
     * @var string
     */
    private $driver;

    /**
     * The database name or the path to the database file for 'sqlite'
     * configurations.
     *
     * @var string
     */
    private $database;

    /**
     * The database username.
     *
     * NULL for 'sqlite' configurations
     *
     * @var string|null
     */
    private $username = NULL;

    /**
     * The database password.
     *
     * NULL for 'sqlite' configurations
     *
     * @var string|null
     */
    private $password = NULL;

    /**
     * The database port.
     *
     * NULL for 'sqlite' configurations
     *
     * @var int|null
     */
    private $port = NULL;

    /**
     * The database host.
     *
     * NULL for 'sqlite' configurations
     *
     * @var string|null
     */
    private $host = NULL;

    /**
     * The database prefix definition/defintions
     *
     * NULL for 'sqlite' configurations
     *
     * @var array|string|null
     */
    private $prefix = NULL;

    /**
     * The database charset.
     *
     * NULL for 'sqlite' configurations
     *
     * @var string|null
     */
    private $charset = NULL;

    /**
     * The database collation.
     *
     * NULL for 'sqlite' configurations
     *
     * @var string|null
     */
    private $collation = NULL;

    /**
     * DrupalDatabaseConfiguration constructor.
     *
     * Prepare the configuation object. First a check is made that the drupal
     * $databases global exists and has been populated (this should always
     * pass in a bootstrapped enviroment). If the $databases global does not
     * exist an exception is throw.
     *
     * Store the array filter and use it to get the database configuration
     * from the $database global using the provided $databaseKey string. If
     * a configuaration array could not be found under the specified key the
     * ArrayFilter will throw an exception.
     *
     * Initial checks have passed so the fetched database configuration array
     * is passed to the validator for further processing. If checks pass here
     * the build helper method is used to populate the object based on the
     * database's driver.
     *
     * @param ArrayFilter $arrayFilter
     * @param string      $databaseKey
     *
     * @throws DrupalDatabaseConfigurationEmptyDatabasesGlobalException
     */
    public function __construct(ArrayFilter $arrayFilter, $databaseKey = 'default.default'){
        global $databases;
        if(empty($databases)){
            throw new DrupalDatabaseConfigurationEmptyDatabasesGlobalException('Could not fetch databases from $databases global. Is drupal bootstrapped?');
        }

        $this->arrayFilter = $arrayFilter;
        $dbConfigArray = $this->arrayFilter->getNestedValueFromKeyString($databases, $databaseKey);

        $this->validateConfiguration($dbConfigArray);
        $this->buildConfiguration($dbConfigArray);
    }

    /**
     * Database configuration array validator.
     *
     * The following method will check the contents of a database
     * configuration are valid for the driver specified.
     *
     * Firstly check the database configuration array has the globally
     * required 'driver' and 'database' keys using the ArrayFilter object, if
     * these keys dont exist the ArrayFilter will throw an exception.
     *
     * Globally required 'driver' and 'database' values are present so ensure
     * the driver is valid for a Drupal install. Drupal only supports mysql,
     * pgsql and sqlite drivers so ensure the value for the driver in the
     * configuration array is one of these, if not throw an exception.
     *
     * Sqlite config definitions need to do define nothing more than a driver
     * and database, these elements have been validated so if a sqlite
     * configuration array has been passed this validation call can be ended.
     *
     * Mysql,Pgsql configurations require a 'username','password' and 'host'
     * to be defined so check that they have been. If one or any of these
     * definitions is missing the ArrayFilter will throw an exception.
     *
     * Finally if a prefix is defined within the configuration array and this
     * definition is itself an array, then ensure it has the 'default'
     * definition stored within it. Use the ArrayFilter for this, again if
     * they key is not found, throw an Exception.
     *
     * @param array $dbConfigArray The parsed drupal datbase configuration array
     *
     * @return bool Was the validation sucessful
     *
     * @throws DrupalDatabaseConfigurationInvalidDatabaseDriverException
     */
    private function validateConfiguration(array $dbConfigArray){
        $this->arrayFilter->checkArrayHasKeys(array('driver', 'database'), $dbConfigArray);

        $dbDriver = $dbConfigArray['driver'];
        if(!in_array($dbDriver, array('mysql','pgsql','sqlite'))){
            throw new DrupalDatabaseConfigurationInvalidDatabaseDriverException($dbDriver);
        }

        if($dbDriver == 'sqlite'){
            return TRUE;
        }

        $this->arrayFilter->checkArrayHasKeys(array('username', 'password', 'host'), $dbConfigArray);

        if(!empty($dbConfigArray['prefix']) && is_array($dbConfigArray['prefix'])){
            $this->arrayFilter->checkArrayHasKeys('default', $dbConfigArray['prefix']);
        }

        return TRUE;
    }

    /**
     * Database configuration array => object builder.
     *
     * The following method is used to convert the received database
     * configuration array into a usable DrupalDatabaseConfiguration object.
     * At this stage a fully validated drupal database configuration array is
     * received no validation has to take place here, only the checking that
     * (optional) definitions exist.
     *
     * Firstly store the globally required 'driver' and 'database'
     * configurations to the object. If the configuration received belongs to
     * a 'sqlite' database there is nothing more to do so build call can be
     * ended here.
     *
     * Not dealing with a 'sqlite' database so continue building. Store the
     * 'mysql/pgsql' required values for 'username', 'password' and 'host'.
     *
     * If a database connection port is defined set it, if not get the
     * default depending on the driver (mysql or pgsql).
     *
     * If a prefix is defined set it.
     *
     * If a charset is defined set it.
     *
     * If a collation is defined set it.
     *
     * @param array $dbConfigArray The parsed drupal datbase configuration array
     *
     * @return bool Was the build sucessful
     */
    private function buildConfiguration(array $dbConfigArray){
        //Set global values
        $this->driver = $dbConfigArray['driver'];
        $this->database = $dbConfigArray['database'];

        //Nothing more to do if sqlite
        if($dbConfigArray['driver'] == 'sqlite'){
            return TRUE;
        }

        //Set mysql/pgsql required values
        $this->username = $dbConfigArray['username'];
        $this->password = $dbConfigArray['password'];
        $this->host = $dbConfigArray['host'];

        //Set port, else revert to mysql/pgsql defaults
        if(!empty($dbConfigArray['port'])){
            $this->port = $dbConfigArray['port'];
        }
        else if($dbConfigArray['driver'] == 'mysql'){
            $this->port = self::DEFAULT_MYSQL_PORT;
        }
        else if($dbConfigArray['driver'] == 'pgsql'){
            $this->port = self::DEFAULT_PGSQL_PORT;
        }

        //Set prefix if set
        if(!empty($dbConfigArray['prefix'])){
            $this->prefix = $dbConfigArray['prefix'];
        }

        //Set charset if set
        if(!empty($dbConfigArray['charset'])){
            $this->charset = $dbConfigArray['charset'];
        }

        //Set collation if set
        if(!empty($dbConfigArray['collation'])){
            $this->collation = $dbConfigArray['collation'];
        }

        return TRUE;
    }

    /**
     * Get the database driver
     *
     * @return string
     */
    public function getDriver(){
        return $this->driver;
    }

    /**
     * Get the database.
     *
     * Returns:
     * - A database name for 'mysql/pgsql' configurations
     * - Path to a database file for 'sqlite' configurations
     *
     * @return string
     */
    public function getDatabase(){
        return $this->database;
    }

    /**
     * Get the database username.
     *
     * Returns NULL for 'sqlite' configurations
     *
     * @return string|null
     */
    public function getUsername(){
        return $this->username;
    }

    /**
     * Get the database password.
     *
     * Returns NULL for 'sqlite' configurations
     *
     * @return string|null
     */
    public function getPassword(){
        return $this->password;
    }

    /**
     * Get the database host.
     *
     * Returns NULL for 'sqlite' configurations
     *
     * @return string|null
     */
    public function getHost(){
        return $this->host;
    }

    /**
     * Get the database port.
     *
     * Returns NULL for 'sqlite' configurations
     *
     * @return int|null
     */
    public function getPort(){
        return $this->port;
    }

    /**
     * Get the database table prefix value
     *
     * Returns NULL for 'sqlite' configurations
     *
     * If a specific prefix is requested but not found NULL is returned. If
     * no specific prefix is requested return the default prefix.
     *
     * @param string $prefixKey The specific prefix to get
     *
     * @return string|null
     */
    public function getPrefix($prefixKey = NULL){
        if(!empty($prefixKey)){
            return (!is_array($this->prefix) || empty($this->prefix[$prefixKey])) ? NULL : $this->prefix[$prefixKey];
        }
        return (!is_array($this->prefix)) ? $this->prefix : $this->prefix['default'];
    }

    /**
     * Get the database charset
     *
     * Returns NULL for 'sqlite' configurations or 'mysql/pgsql'
     * configurations that dont define this value.
     *
     * @return string|null.
     */
    public function getCharset(){
        return $this->charset;
    }

    /**
     * Get the database collation
     *
     * Returns NULL for 'sqlite' configurations or 'mysql/pgsql'
     * configurations that dont define this value.
     *
     * @return string|null.
     */
    public function getCollation(){
        return $this->collation;
    }


}