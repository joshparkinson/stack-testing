<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains menu hooks.
 *
 * @author Josh Parkinson
 */

/**
 * Define menu callbacks for the Rural Stack: Common module.
 *
 * Implements hook_menu().
 */
function rs_common_menu(){
    $items = array();

    //Base config page
    $items['admin/config/rs'] = array(
        'title' => 'Rural Stack',
        'description' => 'Rural Stack configuration entry page.',
        'page callback' => 'system_admin_menu_block_page',
        'access callback' => 'RuralStack\Common\Menu\MenuAccess::userHasPermissions',
        'access arguments' => array('access administration pages', 'administer rs_common'),
        'file' => 'system.admin.inc',
        'file path' => drupal_get_path('module', 'system'),
    );

    //Development configuration page
    $items['admin/config/rs/common'] = array(
        'title' => 'Rural Stack: Common',
        'description' => 'Configure the Rural Stack: Common module.',
        'page callback' => 'system_admin_menu_block_page',
        'access callback' => 'RuralStack\Common\Menu\MenuAccess::userHasPermissions',
        'access arguments' => array('access administration pages', 'administer rs_common'),
        'file' => 'system.admin.inc',
        'file path' => drupal_get_path('module', 'system'),
    );

    //Doctrine configuration page
    $items['admin/config/rs/common/doctrine'] = array(
        'title' => 'Doctrine',
        'description' => 'Configure the Rural Stack: Common module\'s Doctrine Implementation.',
        'page callback' => 'system_admin_menu_block_page',
        'access callback' => 'RuralStack\Common\Menu\MenuAccess::userHasPermissions',
        'access arguments' => array('access administration pages', 'administer rs_common'),
        'file' => 'system.admin.inc',
        'file path' => drupal_get_path('module', 'system'),
    );

    $items['admin/config/rs/common/doctrine/entities'] = array(
        'title' => 'Entities',
        'description' => 'Manage Doctrine ORM Entities',
        'page callback' => 'drupal_get_form',
        'page arguments' =>  array('rs_common_configuration_doctrine_entities_form'),
        'file' => 'src/forms/rs_common.form.configuration.doctrine.entities.inc',
        'access callback' => 'RuralStack\Common\Menu\MenuAccess::userHasPermissions',
        'access arguments'  => array('access administration pages', 'administer rs_common'),
        'type' => MENU_NORMAL_ITEM | MENU_LOCAL_TASK,
        'weight' => -10
    );

    $items['admin/config/rs/common/doctrine/settings'] = array(
        'title' => 'Settings',
        'description' => 'Configure the Rural Stack: Common Doctrine implementation.',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('rs_common_configuration_doctrine_settings_form'),
        'file' => 'src/forms/rs_common.form.configuration.doctrine.settings.inc',
        'access callback' => 'RuralStack\Common\Menu\MenuAccess::userHasPermissions',
        'access arguments' => array('access administration pages', 'administer rs_common'),
        'type' => MENU_NORMAL_ITEM | MENU_LOCAL_TASK,
        'weight' => -9
    );

    //Doctrine configuration page
    $items['admin/config/rs/common/settings'] = array(
        'title' => 'Settings',
        'description' => 'Configure the Rural Stack: Common module.',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('rs_common_configuration_settings_form'),
        'file' => 'src/forms/rs_common.form.configuration.settings.inc',
        'access callback' => 'RuralStack\Common\Menu\MenuAccess::userHasPermissions',
        'access arguments' => array('access administration pages', 'administer rs_common'),
    );

    return $items;
}