<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains schema hooks.
 *
 * @author Josh Parkinson
 */

/**
 * Alter the schema data mapping types to include datetime column types.
 *
 * Implements hook_schmea_field_type_map_alter().
 *
 * @param array               $map
 * @param \DatabaseSchema     $schema
 * @param \DatabaseConnection $connection
 */
function rs_common_schema_field_type_map_alter(array &$map, DatabaseSchema $schema, DatabaseConnection $connection) {
    $connectionOptions = $connection->getConnectionOptions();
    switch($connectionOptions['driver']){
        case 'mysql':
            $map['datetime:normal'] = 'DATETIME';
            break;
        case 'pgsql':
            $map['datetime:normal'] = 'timestamp without time zone';
            break;
        case 'sqlite':
            $map['datetime:normal'] = 'VARCHAR';
            break;
    }
}