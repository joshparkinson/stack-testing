<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains permission hooks.
 *
 * @author Josh Parkinson
 */

/**
 * Define permissions created by the Rural Stack: Common module.
 *
 * Implements hook_permission().
 */
function rs_common_permission(){
    $permissions = array();

    $permissions['administer rs_common'] = array(
        'title' => 'Administer Rural Stack: Common',
        'description' => 'Access the Rural Stack: Common administration pages'
    );

    return $permissions;
}