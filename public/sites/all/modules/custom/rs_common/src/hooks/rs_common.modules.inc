<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains modules install/uninstall hooks.
 * These hooks differ from the hook_install() as they
 * give this module a chance to trigger functionality
 * when other modules are installed/uninstalled within
 * the system.
 *
 * @author Josh Parkinson
 */

use Doctrine\DBAL\Schema\Comparator;
use Doctrine\ORM\Tools\SchemaTool;
use RuralStack\Common\Doctrine\Entity\Provider\EntityClass\EntityClassProvider;
use RuralStack\Common\Doctrine\Entity\Provider\Path\EntityPathProvider;
use RuralStack\Common\Doctrine\Entity\Storage\Installer\EntityInstaller;
use RuralStack\Common\Doctrine\Entity\Storage\Installer\Exception\EntityInstallerQueryExecutionException;
use RuralStack\Common\Doctrine\Entity\Storage\Installer\Exception\EntityInstallerRemovalQueriesNotFoundException;
use RuralStack\Common\Doctrine\Entity\Storage\Query\EntityQueryGenerator;
use RuralStack\Common\Doctrine\RSEntityManager;
use RuralStack\Common\Module\Hook\HookInvoker;
use RuralStack\Common\Module\ModuleCollectionFactory;
use RuralStack\Common\RSC\Config\RSCConfigConstants;
use System\Utility\Filter\ArrayFilter;

/**
 * Intercept the installation of other Drupal Modules.
 *
 * Implements hook_modules_installed().
 *
 * Install Module Doctrine Entities:
 * - Create a ModuleCollection object of Modules implement the Doctrine Entity
 *   Paths hook from the keys of the Modules currently being installed.
 * - If some of the Module's being installed provide Doctrine Entities (ie/ the
 *   created ModuleCollection object is not empty create the EntityInstaller
 *   class and all of it's dependents.
 * - Foreach of the installed Module's that provide Doctrine Entities attempt to
 *   install the Entities that they provide if any of the installations throw
 *   Exceptions catch the Exception and show an error message to the caller
 *   using Drupal's core drupal_set_message function. Move on to processing the
 *   next Module after display the error message.
 *
 * @param array $modules
 *     An array of Module keys for Module's being installed.
 */
function rs_common_modules_installed($modules){
    $installedDoctrineModules = ModuleCollectionFactory::createFromModuleKeysImplementingHook(
        $modules,
        RSCConfigConstants::RSC_HOOK_DOCTRINE_ENTITY_PATHS
    );

    if(!$installedDoctrineModules->isEmpty()){
        $arrayFilter = new ArrayFilter();
        $hookInvoker = new HookInvoker();
        $entityPathProvider = new EntityPathProvider($hookInvoker);
        $entityClassProvider = new EntityClassProvider($entityPathProvider, $arrayFilter);

        $entityManager = RSEntityManager::create();
        $comparator = new Comparator();
        $schemaTool = new SchemaTool($entityManager);
        $entityQueryGenerator = new EntityQueryGenerator($entityManager, $comparator, $schemaTool);

        $entityInstaller = new EntityInstaller($entityClassProvider, $entityQueryGenerator);

        foreach($installedDoctrineModules as $installedDoctrineModule){
            try{
                $entityInstaller->installModuleEntities($installedDoctrineModule);
            }
            catch(EntityInstallerQueryExecutionException $e){
                drupal_set_message(strtr(
                    'Could not install Entity structure for Module: :moduleKey.
                    Please reinstall this module to reattempt building of the
                    Entity strcuture.',
                    array(':moduleKey' => $installedDoctrineModule->getMachineName())
                ), 'error');

                continue;
            }
        }
    }
}

/**
 * Intercept the uninstallation of other Drupal Modules.
 *
 * Implements hook_modules_uninstalled().
 *
 * Uninstall Module Doctrine Entities:
 * - Create a ModuleCollection object of all the Module's being unsintalled
 *   using the array keys passed this hook.
 * - Foreach of these Modules attempt to remove the Entities attached to them
 *   using the EntityUninstaller object.
 * - If an Uninstallation request throws an
 *   EntityRemovalQueriesNotFoundException it is assumed that this Module does
 *   not provide Entity classes and is therefore caught and skipped silently.
 *
 * @param array $modules
 *     An array of Module keys for Module's being installed.
 */
function rs_common_modules_uninstalled($modules){
    $uninstalledModulesCollection = ModuleCollectionFactory::createFromModuleKeys($modules);

    $arrayFilter = new ArrayFilter();
    $hookInvoker = new HookInvoker();
    $entityPathProvider = new EntityPathProvider($hookInvoker);
    $entityClassProvider = new EntityClassProvider($entityPathProvider, $arrayFilter);

    $entityManager = RSEntityManager::create();
    $comparator = new Comparator();
    $schemaTool = new SchemaTool($entityManager);
    $entityQueryGenerator = new EntityQueryGenerator($entityManager, $comparator, $schemaTool);

    $entityInstaller = new EntityInstaller($entityClassProvider, $entityQueryGenerator);

    foreach($uninstalledModulesCollection as $uninstalledModule){
        try{
            $entityInstaller->uninstallModuleEntities($uninstalledModule);
        }
        catch(EntityInstallerQueryExecutionException $e){
            drupal_set_message(strtr(
                'Could not uninstall Entity structure for Module :moduleKey.
                Please remove the datatables associated to this module
                manually using the command line tools.',
                array(':moduleKey' => $uninstalledModule->getMachineName())
            ), 'error');

            continue;
        }
        catch(EntityInstallerRemovalQueriesNotFoundException $e){
            continue;
        }
    }
}