<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains xautoload hooks.
 * Also includes xautoload initialisation via hook_boot.
 *
 * @see xAutoload Early Loading: https://www.drupal.org/node/1976210
 *
 * @author Josh Parkinson
 */

use Drupal\xautoload\Adapter\LocalDirectoryAdapter;

/**
 * Add the RuralStack\Common namespace to the system during boot.
 *
 * Hard add namepsace so it is available to OTHER modules during
 * install/uninstall process. It will also be available in the .module file
 * outside of hook context.
 *
 * If you wish to use this modules classes inside its own install/uninstall
 * process or inside of its .module file outside of hook context you must
 * include this call to xautoload in the relevant file (.module, .install).
 *
 * Note:
 * The path definition may differ between this call and the one in the
 * .module or .install file depending on the files location.
 *
 * Note:
 * The namespace file path defined here MUST be absolute.
 *
 * Implements hook_boot().
 */
function rs_common_boot(){
    xautoload()->finder->addPsr4('RuralStack\\Common\\', realpath(__DIR__.'/../class'));
}

/**
 * Add the RuralStack\Common namespace to the system using the xautoload hook.
 *
 * Use the recieved adapted to register this modules namespace to the system
 * for use.
 *
 * Note:
 * The namespace file path defined here must be relative from this modules
 * base directory.
 *
 * @param LocalDirectoryAdapter $adapter
 */
function rs_common_xautoload($adapter){
    $adapter->addPsr4('RuralStack\\Common\\', 'src/class');
}