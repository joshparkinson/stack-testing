<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the Doctrine Entities form definition.
 *
 * @author Josh Parkinson
 */

use Doctrine\DBAL\Schema\Comparator;
use Doctrine\ORM\Tools\SchemaTool;
use RuralStack\Common\Doctrine\Entity\Provider\EntityClass\EntityClassProvider;
use RuralStack\Common\Doctrine\Entity\Provider\Path\EntityPathProvider;
use RuralStack\Common\Doctrine\Entity\Storage\Installer\EntityInstaller;
use RuralStack\Common\Doctrine\Entity\Storage\Installer\Exception\EntityInstallerQueryExecutionException;
use RuralStack\Common\Doctrine\Entity\Storage\Installer\Exception\EntityInstallerUpdateQueriesNotFoundException;
use RuralStack\Common\Doctrine\Entity\Storage\Query\EntityQueryGenerator;
use RuralStack\Common\Doctrine\RSEntityManager;
use RuralStack\Common\Form\SystemSettingsForm;
use RuralStack\Common\Module\Hook\HookInformation;
use RuralStack\Common\Module\Hook\HookInvoker;
use RuralStack\Common\Module\ModuleCollectionFactory;
use RuralStack\Common\Output\Builder\Table\TableBuilder;
use RuralStack\Common\RSC\Config\RSCConfigConstants;
use System\Utility\Filter\ArrayFilter;
use System\Utility\Filter\StringFilter;

/**
 * Rural Stack: Common Doctrine Entities Settings Form
 *
 * The following function defines the Doctrine Entities status, breakdown and
 * update form. The form is implemented (or defined) using Drupal's From API.
 * The rendering of this form is deicated by the menu item definition as
 * defined in the '_menu' hook of this Module.
 *
 * Generate, instantiate all of the necessary classes required to render this
 * form and the functionality contained within it.
 *
 * Fetch all defined Doctrine Entity paths provided by the enabled Drupal
 * Modules that implement the Doctrine hooks provided by this Module. Build a
 * table with the paths fetched, store the output, reset the builder.
 *
 * Fetch all defined Doctrine Entity classes provided by the enabled Drupal
 * Modules that implement the Doctrine hooks provided by this Module. Build a
 * table with the classes fetched, store the output, reset the builder.
 *
 * Run a dry of the the EntityInstaller classes updateEntities method to
 * determine if an update of the defined Doctrine Entities data structure is
 * required. Depending on the result of this store the relevant status
 * variables used for output later.
 *
 * Build the 3 fieldsets required for this form:
 * - Entity Status
 * - Entity Paths
 * - Entity Classes
 *
 * Leverage the SystemSettingsForm to build the form providing validation and
 * submission handler overrides aswell updated submit button text.
 *
 * @param array $form
 *     The Drupal form definition array
 * @param array $form_state
 *     The Drupal form state array
 *
 * @return array
 *     The built Drupal form definition ready for rendering.
 */
function rs_common_configuration_doctrine_entities_form($form, &$form_state){
    $arrayFilter = new ArrayFilter();
    $stringFilter = new StringFilter();

    $hookInvoker = new HookInvoker();
    $entityPathProvider = new EntityPathProvider($hookInvoker);
    $entityClassProvider = new EntityClassProvider($entityPathProvider, $arrayFilter);

    $entityManager = RSEntityManager::create();
    $comparator = new Comparator();
    $schemaTool = new SchemaTool($entityManager);
    $entityQueryGenerator = new EntityQueryGenerator($entityManager, $comparator, $schemaTool);

    $entityInstaller = new EntityInstaller($entityClassProvider, $entityQueryGenerator);

    $tableBuilder = new TableBuilder($arrayFilter, $stringFilter);
    foreach($entityPathProvider->getEntityPaths() as $entityPath){
        $tableBuilder->setRow(array(
            array('data' => $entityPath)
        ));
    }
    $entityPathTable = $tableBuilder->build();
    $tableBuilder->reset();

    foreach($entityClassProvider->getEntityClasses() as $entityClass){
        $tableBuilder->setRow(array(
            array('data' => $entityClass)
        ));
    }
    $entityClassTable = $tableBuilder->build();
    $tableBuilder->reset();

    if($entityInstaller->entityUpdatesRequired()){
        $updateStatus = 'Entity Updates Required';
        $updateClass = 'updates-required';
        $submitDisabled = FALSE;
    }
    else{
        $updateStatus = 'No Entity Updates Required';
        $updateClass = 'updates-not-required';
        $submitDisabled = TRUE;
    }

    $form['entity_status'] = array(
        '#type' => 'fieldset',
        '#title' => 'Entity Status',
        '#collapsible' => FALSE,
        '#collapsed' => FALSE,
        'status' => array(
            '#type' => 'markup',
            '#markup' => strtr(
                '<div class=":updateClass">:updateStatus</div>',
                array(
                    ':updateClass' => $updateClass,
                    ':updateStatus' => $updateStatus
                )
            )
        )
    );

    $form['entity_paths'] = array(
        '#type' => 'fieldset',
        '#title' => 'Entity Paths',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        'table' => array(
            '#type' => 'markup',
            '#markup' => $entityPathTable
        )
    );

    $form['entity_classes'] = array(
        '#type' => 'fieldset',
        '#title' => 'Entity Classes',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        'table' => array(
            '#type' => 'markup',
            '#markup' => $entityClassTable
        )
    );

    /**
     * Form buid/finalisation.
     *
     * Build and return the finalised SystemSettingsForm to the caller.
     */
    $systemSettingsForm = new SystemSettingsForm($form, $arrayFilter);
    $systemSettingsForm->setValidationHandlers('rs_common_configuration_doctrine_entities_form_validate');
    $systemSettingsForm->setSubmissionHandlers('rs_common_configuration_doctrine_entities_form_submit');
    $systemSettingsForm->setSubmitButtonText('Run updates');
    $form =  $systemSettingsForm->build();

    if($submitDisabled){
        $form['actions']['submit'] = array(
            '#type' => 'markup',
            '#markup' => 'Nothing to do.'
        );
    }

    return $form;
}

/**
 * Rural Stack: Common Doctrine Entities Settings Form Validation Handler
 *
 * Check Entity data strcutre updates are required before final processing.
 *
 * Throw a form error if no updates are required.
 *
 * @param array $form
 *     The Drupal form definition array
 * @param array $form_state
 *     The Drupal form state array
 */
function rs_common_configuration_doctrine_entities_form_validate($form, &$form_state){
    $arrayFilter = new ArrayFilter();

    $hookInvoker = new HookInvoker();
    $entityPathProvider = new EntityPathProvider($hookInvoker);
    $entityClassProvider = new EntityClassProvider($entityPathProvider, $arrayFilter);

    $entityManager = RSEntityManager::create();
    $comparator = new Comparator();
    $schemaTool = new SchemaTool($entityManager);
    $entityQueryGenerator = new EntityQueryGenerator($entityManager, $comparator, $schemaTool);

    $entityInstaller = new EntityInstaller($entityClassProvider, $entityQueryGenerator);

    if(!$entityInstaller->entityUpdatesRequired()){
        form_set_error('no_updates_required', 'No Entity Updates Required.');
    }
}

/**
 * Rural Stack: Common Doctrine Entities Settings Form Submission Handler
 *
 * Create a ModuleCollection object for the Drupal Module's that provide
 * Doctrine ORM Entities.
 *
 * Attempt to update the Entity data structure for each of the Module's within
 * the generated ModuleCollection object catching any Exceptions thrown.
 *
 * If during the Entity Update process a
 * EntityInstallerUpdateQueriesNotFoundException is thrown it is assumped that
 * no updates are required for this Module and the update process is skipped
 * gracefully.
 *
 * @param array $form
 *     The Drupal form definition array
 * @param array $form_state
 *     The Drupal form state array
 */
function rs_common_configuration_doctrine_entities_form_submit($form, &$form_state){
    $arrayFilter = new ArrayFilter();

    $hookInvoker = new HookInvoker();
    $entityPathProvider = new EntityPathProvider($hookInvoker);
    $entityClassProvider = new EntityClassProvider($entityPathProvider, $arrayFilter);

    $entityManager = RSEntityManager::create();
    $comparator = new Comparator();
    $schemaTool = new SchemaTool($entityManager);
    $entityQueryGenerator = new EntityQueryGenerator($entityManager, $comparator, $schemaTool);

    $entityInstaller = new EntityInstaller($entityClassProvider, $entityQueryGenerator);

    $hookInformation = new HookInformation();

    $doctrineModules = ModuleCollectionFactory::createFromModuleKeys(
        $hookInformation->getHookImplementations(RSCConfigConstants::RSC_HOOK_DOCTRINE_ENTITY_PATHS)
    );

    foreach($doctrineModules as $doctrineModule){
        try{
            $entityInstaller->updateModuleEntities($doctrineModule);
        }
        catch(EntityInstallerQueryExecutionException $e){
            drupal_set_message(strtr(
                'Could not update Entites for Module :moduleKey',
                array(':moduleKey' => $doctrineModule->getMachineName())
            ), 'error');

            continue;
        }
        catch(EntityInstallerUpdateQueriesNotFoundException $e){
            continue;
        }
    }

    drupal_set_message('Entity Update Completed', 'success');
}