<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the settings form definition.
 *
 * @author Josh Parkinson
 */

use RuralStack\Common\Form\SystemSettingsForm;
use RuralStack\Common\RSC\Config\RSCConfigConstants;
use System\Utility\Filter\ArrayFilter;

//todo: recomment

/**
 * Rural Stack: Common Configuration Settings Form.
 *
 * The following function defines the configuration form for use with the
 * Rural Stack: Common Module. The form is implemented (or defined) using
 * Drupal's Form API. The rending of this form is dicated by the menu item
 * definition as definined in the '_menu' hook of this Module.
 *
 * Each section of the form should be contained within its own collapsible
 * fieldset all settings relevant to that section should be contained within it.
 *
 * Note: This form is finalised through the form 'build' method, therefore
 * the input keys used within this form definition will take the name of the
 * variable that input values is saved under within the Druapl database.
 *
 * @see rs_common_menu()
 * @see SystemSettingsForm
 *
 * @param $form
 * @param $form_state
 *
 * @return array
 */
function rs_common_configuration_settings_form($form, &$form_state){

    /**
     * General Settings.
     *
     * Fields:
     * - Remove automatically installed files on module uninstall.
     */
    $form['general'] = array(
        '#type' => 'fieldset',
        '#title' => 'General Settings',
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
    );

    $form['general'][RSCConfigConstants::RSC_VAR_DELETE_FILES_UNINSTALL_SETTING] = array(
        '#type' => 'checkbox',
        '#title' => t('Delete the automatically created files on uninstall'),
        '#description' => t('Enable this setting to remove any of the automatically created files/directories/etc when this module was installed'),
        '#default_value' => variable_get(RSCConfigConstants::RSC_VAR_DELETE_FILES_UNINSTALL_SETTING, FALSE)
    );

    /**
     * Form buid/finalisation.
     *
     * Build and return the finalised SystemSettingsForm to the caller.
     */
    $arrayFilter = new ArrayFilter();
    $systemSettingsForm = new SystemSettingsForm($form, $arrayFilter);
    return $systemSettingsForm->build();
}