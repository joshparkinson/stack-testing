<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_common
 *
 * Contains the Doctrine settings form definition.
 *
 * @author Josh Parkinson
 */

use RuralStack\Common\Form\SystemSettingsForm;
use RuralStack\Common\RSC\Config\RSCConfigConstants;
use System\Utility\Filter\ArrayFilter;

//todo: comment
function rs_common_configuration_doctrine_settings_form($form, &$form_state){

    /**
     * Doctrine Settings.
     *
     * Fields:
     * - Enable/disable development mode
     * - Remove doctrine migration classes on module uninstall
     * - Remove doctrin proxy classed on module uninstall
     */
    $form['doctrine'] = array(
        '#type' => 'fieldset',
        '#title' => 'Doctrine Settings',
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
    );

    $form['doctrine'][RSCConfigConstants::RSC_VAR_DOCTRINE_DEV_MODE_SETTING] = array(
        '#type' => 'checkbox',
        '#title' => t('Enable Development Mode'),
        '#description' => t(
            'Enabling development mode may overrite one or all of the configuration settings for the
             Doctrine Entity Manager.'
        ),
        '#default_value' => variable_get(RSCConfigConstants::RSC_VAR_DOCTRINE_DEV_MODE_SETTING, FALSE)
    );

    $form['doctrine'][RSCConfigConstants::RSC_VAR_DOCTRINE_REMOVE_MIGRATIONS_UNINSTALL_SETTING] = array(
        '#type' => 'checkbox',
        '#title' => t('Remove Doctrine Migrations on uninstall'),
        '#description' => t('Enable this setting to remove the Doctrine Migration classes and database data on install.'),
        '#default_value' => variable_get(RSCConfigConstants::RSC_VAR_DOCTRINE_REMOVE_MIGRATIONS_UNINSTALL_SETTING, FALSE)
    );

    $form['doctrine'][RSCConfigConstants::RSC_VAR_DOCTRINE_REMOVE_PROXIES_UNINSTALL_SETTING] = array(
        '#type' => 'checkbox',
        '#title' => t('Remove Doctrine Proxies on uninstall'),
        '#description' => t('Enable this setting to remove the Doctrine Proxy classes on uninstall.'),
        '#default_value' => variable_get(RSCConfigConstants::RSC_VAR_DOCTRINE_REMOVE_PROXIES_UNINSTALL_SETTING, FALSE)
    );

    /**
     * Form buid/finalisation.
     *
     * Build and return the finalised SystemSettingsForm to the caller.
     */
    $arrayFilter = new ArrayFilter();
    $systemSettingsForm = new SystemSettingsForm($form, $arrayFilter);
    return $systemSettingsForm->build();

}