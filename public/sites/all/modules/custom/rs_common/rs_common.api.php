<?php
/**
 * @file
 * Rural Stack Custom Module API File.
 * Module: rs_common
 *
 * Outlines usages of hooks created by this module.
 *
 * @author Josh Parkinson
 */

use RuralStack\Common\Module\Module;

/**
 * Define paths for use within the Rural Stack Doctrine Entity Manager.
 *
 * This hook should be implemented by any modules that wish to leverage the
 * functionality of the Rural Stack Doctrine Entity Manager to manipulate a
 * sites database via the use of the Doctrine ORM.
 *
 * For the Rural Stack Doctrine Entity Manager to work with the Entity
 * classes defined within the implementing module it needs to know about them.
 * This hook provides a way for modules to notify the Entity Manager of it's
 * Entity classes as and when it is created.
 *
 * The entity paths returned by this hook MUST be absolute paths to the
 * containing directories. This enables the Entity Manager to work correctly
 * within a sites controllers and also within a command line entry point. To
 * extract the absolute path's to a module it is recommended to create a
 * module object: RuralStack\Common\Module\Module and use its helper method
 * Module::getPath with the absolute flag set to true.
 *
 * @see RSEntityManager::getEntityManager()
 *
 * @return array The module directories containg the Entity classes.
 */
function hook_rs_doctrine_entity_paths(){
    //Using the module class, get the module and it's absolute path.
    $module = new Module('my_module');
    $modulePath = $module->getPath(TRUE);

    //Send the absolute paths of the entity directories to the entity manager.
    return array(
        $modulePath.'/src/class/Fictional/Product/Entity',
        $modulePath.'/src/class/Fictional/User/Entity'
    );
}

/**
 * Define Entity namespace alias for use within the Rural Stack Doctrine
 * Entity Manager.
 *
 * This hook should be implemented by any modules that wish to leverage the
 * functionality of the Rural Stack Doctrine Entity Manager to manipulate a
 * sites database via the use of the Doctrine ORM.
 *
 * This hook will add any namespace aliases to the Rural Stack Doctrine
 * Entity Manager. These aliases are not a requirement however and the Entity
 * classes can be accessed through the Entity Manager using their fully
 * qualified classname.
 *
 * Namespaces should be returned to the entity manager as an array of
 * namespaces key'd by their alias.
 *
 * Note:
 * Any classes defined under these alias will be accessable through the
 * entity manager using the notation. Alias:ClassName.
 *
 * Example:
 * $entityManager->find('Product:CarProduct', $productId);
 *
 * @return array Namespaces aliases. Alias => Namespace
 */
function hook_rs_doctrine_entity_namespaces(){
    return array(
        'Product' => 'RuralStack\\Common\\Fictional\\Product\\Entity',
        'User' => 'RuralStack\\Common\\Fictional\\User\\Entity'
    );
}