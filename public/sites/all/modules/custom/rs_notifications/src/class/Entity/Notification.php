<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_notifications
 *
 * Contains RuralStack\Notifications\Entity\Notification Entity Class.
 *
 * @author Josh Parkinson
 */

namespace RuralStack\Notifications\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Notification
 *
 * The Notification Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="rs_notifications", options={"collate":"utf8mb4_general_ci", "charset":"utf8mb4", "comment":"Notification entries."})
 * @ORM\HasLifecycleCallbacks
 *
 * @package RuralStack\Notifications\Entity
 */
class Notification {

    /**
     * The unique id of the notification.
     *
     * This value can not be set manually. It is set as part of the Entities
     * persistance.
     *
     * @ORM\Id
     * @ORM\Column(type="integer", options={"comment":"The unique id of the notification."})
     * @ORM\GeneratedValue
     *
     * @var int
     */
    protected $id;

    /**
     * The notification title.
     *
     * @ORM\Column(type="string", length=140, options={"comment":"The notification title."})
     *
     * @var string
     */
    protected $title;

    /**
     * The notification body.
     *
     * @ORM\Column(type="text", nullable=true, options={"comment":"The notification body."})
     *
     * @var string
     */
    protected $body;

    /**
     * The path of the page this notification links to.
     *
     * @ORM\Column(type="string", length=150, options={"comment":"The path of the page this notificaitons links to."})
     *
     * @var string
     */
    protected $path;

    /**
     * The date time of when this notification was created.
     *
     * This value can not be set manually. It is set as part of a LifeCycle
     * callback.
     *
     * @ORM\Column(type="datetime", options={"comment":"The date time of when this notification was created."})
     *
     * @var \DateTime
     */
    protected $created;

    /**
     * The scrope of this notification. global or specific.
     *
     * Defines the notification recepient information. If a notification is
     * to be sent to everyone set this value to 'global'. If there are
     * specified recipients set this value to 'specific'.
     *
     * Notifications with a 'specific' scope will contain entries in the
     * 'rs_notifications_recipients' table linking the notification to the
     * user who it has been sent to. A notification can have an unlimited number
     * of specific receipients.
     *
     * This value can not be set manually. It is set as part of a LifeCycle
     * callback.
     *
     * @ORM\Column(type="string", length=10, options={"comment":"The scrope of this notification. global or specific.", "default":"global"})
     *
     * @var string
     */
    protected $scope;

    /**
     * The module that created this notification.
     *
     * @ORM\Column(type="string", length=50, options={"comment":"The module that created this notification."})
     *
     * @var string
     */
    protected $module;

    /**
     * The notification key.
     *
     * Value can be used to group notifications by type.
     *
     * @ORM\Column(type="string", length=50, options={"comment":"The notification key"})
     *
     * @var string
     */
    protected $key;

    /**
     * Any extra classes for this notifications output.
     *
     * Multiple classes can be added seperated by a space.
     *
     * @ORM\Column(name="class_string", type="string", length=100, options={"comment":"Any extra classes for this notifications output."})
     *
     * @var string
     */
    protected $classString;

}