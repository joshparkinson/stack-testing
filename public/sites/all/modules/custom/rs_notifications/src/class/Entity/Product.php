<?php

namespace RuralStack\Notifications\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Product.
 *
 * @ORM\Entity @ORM\Table(name="notif_products")
 */
class Product {

    /**
     * @ORM\Id() @ORM\Column(type="integer") @ORM\GeneratedValue()
     *
     * @var int
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    protected $name;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    protected $test;

    /**
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(){
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Product
     */
    public function setName($name){
        $this->name = $name;
        return $this;
    }

}