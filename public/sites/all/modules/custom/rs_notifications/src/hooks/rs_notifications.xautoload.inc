<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_notifications
 *
 * Contains xautoload hooks.
 * Also includes xautoload initialisation via hook_boot.
 *
 * @see xAutoload Early Loading: https://www.drupal.org/node/1976210
 *
 * @author Josh Parkinson
 */

use Drupal\xautoload\Adapter\LocalDirectoryAdapter;

/**
 * Add the RuralStack\Common namespace to the system during boot.
 *
 * Hard add namepsace so it is available to OTHER modules during
 * install/uninstall process. It will also be available in the .module file
 * outside of hook context.
 *
 * Note:
 * The namespace file path defined here MUST be absolute.
 *
 * Implements hook_boot().
 */
function rs_notifications_boot(){
    xautoload()->finder->addPsr4('RuralStack\\Notifications\\', realpath(__DIR__.'/../class'));
}

/**
 * Add the RuralStack\Common namespace to the system using the xautoload hook.
 *
 * Use the recieved adapted to register this modules namespace to the system
 * for use.
 *
 * Note:
 * The namespace file path defined here must be relative from this modules
 * base directory.
 *
 * @param LocalDirectoryAdapter $adapter
 */
function rs_notifications_xautoload($adapter){
    $adapter->addPsr4('RuralStack\\Notifications\\', 'src/class');
}