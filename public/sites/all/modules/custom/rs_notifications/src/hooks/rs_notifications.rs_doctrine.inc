<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_notifications
 *
 * Contains Rural Stack Doctrine hooks.
 *
 * @author Josh Parkinson
 */

use RuralStack\Common\Module\Module;

/**
 * Define and return the absolute paths to this modules Doctrine Entity
 * containing directories for use within the Rural Stack Doctrine Entity
 * Manager.
 *
 * Implements hook_rs_doctrine_entity_paths().
 *
 * @see \RuralStack\Common\Doctrine\RSEntityManager
 * @see rs_commmon.api.php
 */
function rs_notifications_rs_doctrine_entity_paths(){
    $module = new Module('rs_notifications');

    return array(
        $module->getPath(TRUE).'/src/class/Entity',
    );
}

/**
 * Define and return any namespace aliases for this module to the Rural Stack
 * Doctrine Entity Manager
 *
 * Implements hook_rs_doctrine_entity_namespace().
 *
 * @see \RuralStack\Common\Doctrine\RSEntityManager
 * @see rs_commmon.api.php
 */
function rs_notifications_rs_doctrine_entity_namespaces(){
    return array(
        'RSNotifications' => 'RuralStack\Notifications\Entity'
    );
}