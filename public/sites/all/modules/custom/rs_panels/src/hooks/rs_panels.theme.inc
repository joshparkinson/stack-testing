<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_panels
 *
 * Contains theme hooks.
 *
 * @author Josh Parkinson
 */

/**
 * Define this modules theme implementations.
 *
 * Implements hook_theme().
 *
 * @see https://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_theme/7.x
 *
 * @param array  $existing
 *     An array of existing implementations that may be used for override
 *     purposes. This is primarily useful for themes that may wish to examine
 *     existing implementations to extract data (such as arguments) so that it
 *     may properly register its own, higher priority implementations.
 * @param string $type
 *     Whether a theme, module, etc. is being processed. This is primarily
 *     useful so that themes tell if they are the actual theme being called
 *     or a parent theme. May be one of:
 *     - module: A module is being checked for theme implementations.
 *     - base_theme_engine: A theme engine is being checked for a theme that is
 *                          a parent of the actual theme being used.
 *     - theme_engine: A theme engine is being checked for the actual theme
 *                     being used.
 *     - base_theme: A base theme is being checked for theme implementations.
 *     - theme: The actual theme in use is being checked.
 * @param string $theme
 *     The actual name of theme, module, etc. that is being being processed.
 * @param string $path
 *     The directory path of the theme or module, so that it doesn't need to be
 *     looked up.
 *
 * @return array
 *     An associative array of theme hook information.
 */
function rs_panels_theme($existing, $type, $theme, $path){
    return array(
        'bootstrap_navbar_dropdown' => array(
            'file' => 'src/theme/rs_panels.bootstrap_navbar_dropdown.inc',
            'function' => 'rs_panels_bootstrap_navbar_dropdown',
            'render element' => 'element'
        )
    );
}

/**
 * todo: comment
 *
 * @param $theme_registry
 */
function rs_panels_theme_registry_alter(&$theme_registry) {
    // Defined path to the current module.
    $module_path = drupal_get_path('module', 'rs_panels');
    // Find all .tpl.php files in this module's folder recursively.
    $template_file_objects = drupal_find_theme_templates($theme_registry, '.tpl.php', $module_path);
    // Iterate through all found template file objects.
    foreach ($template_file_objects as $key => $template_file_object) {
        // If the template has not already been overridden by a theme.
        if (!isset($theme_registry[$key]['theme path']) || !preg_match('#/themes/#', $theme_registry[$key]['theme path'])) {
            // Alter the theme path and template elements.
            $theme_registry[$key]['theme path'] = $module_path;
            $theme_registry[$key] = array_merge($theme_registry[$key], $template_file_object);
            $theme_registry[$key]['type'] = 'module';
        }
    }
}