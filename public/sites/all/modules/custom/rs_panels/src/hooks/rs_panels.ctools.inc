<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_panels
 *
 * Contains ctools hooks.
 *
 * @author Josh Parkinson
 */

/**
 * Implements hook_ctools_plugin_directory().
 *
 * todo: comment
 */
function rs_panels_ctools_plugin_directory($owner, $plugin_type){
    if ($owner == 'ctools' && $plugin_type == 'content_types'){
        return 'plugins/content_types';
    }
}