<?php
/**
 * @file
 * Rural Stack Custom Module File.
 * Module: rs_panels
 *
 * Contains the bootstrap_navbar_dropdown theme function
 *
 * @author Josh Parkinson
 */

function rs_panels_bootstrap_navbar_dropdown($variables){
    $element = $variables['element'];
    $sub_menu = '';

    if (!empty($element['#below'])) {
        // Wrap in dropdown-menu.
        unset($element['#below']['#theme_wrappers']);
        $sub_menu = '<ul class="dropdown-menu">' . drupal_render($element['#below']) . '</ul>';
        $element['#localized_options']['attributes']['class'][] = 'dropdown-toggle';
        $element['#localized_options']['html'] = TRUE;

        // Check if element is nested.
        if ((!empty($element['#original_link']['depth'])) && ($element['#original_link']['depth'] > 1)) {
            $element['#attributes']['class'][] = 'dropdown-submenu';
            $element['#title'] .= ' <span class="fa fa-caret-right"></span>';
        }
        else {
            $element['#attributes']['class'][] = 'dropdown';
            $element['#title'] .= ' <span class="fa fa-caret-down"></span>';

            $element['#localized_options']['attributes']['data-toggle'] = 'dropdown';
            $element['#localized_options']['attributes']['data-target'] = '#';
        }
    }

    // Fix for active class.
    if (($element['#href'] == current_path() || ($element['#href'] == '<front>' && drupal_is_front_page())) && (empty($element['#localized_options']['language']) || $element['#localized_options']['language']->language == $language_url->language)) {
        $element['#attributes']['class'][] = 'active';
    }

    // Add active class to li if active trail.
    if (in_array('active-trail', $element['#attributes']['class'])) {
        $element['#attributes']['class'][] = 'active';
    }

    // Add a unique class using the title.
    $title = strip_tags($element['#title']);
    $element['#attributes']['class'][] = 'menu-link-' . drupal_html_class($title);

    $output = l($element['#title'], $element['#href'], $element['#localized_options']);
    return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}