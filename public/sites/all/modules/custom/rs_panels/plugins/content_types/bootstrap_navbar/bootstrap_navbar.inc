<?php

$plugin = array(
    'title' => t('Bootstrap Navbar'),
    'description' => t('Formats a give menu to the Bootstrap Navbar HTML structure'),
    'category' => t('Rural Stack'),
    'content_types' => array('rs_panels_bootstrap_navbar'),
    'single' => TRUE,
    'edit form' => 'rs_panels_bootstrap_navbar_edit_form',
    'render callback' => 'rs_panels_bootstrap_navbar_render_callback',
    'admin title' => 'rs_panels_bootstrap_navbar_admin_title',
    'admin info' => 'rs_panels_bootstrap_navbar_admin_info',
    'defaults' => array(),
    'all contexts' => TRUE,
);

function rs_panels_bootstrap_navbar_admin_title($subtype, $config, $context = NULL){
    return "Rural Stack: Bootstrap Navbar";
}

function rs_panels_bootstrap_navbar_admin_info($subtype, $config, $context = NULL){
    $menuSource = menu_get_menus()[$config['menu']];

    $output = new stdClass();
    $output->title = $menuSource;
    $output->content = theme_item_list(array(
        'title' => NULL,
        'items' => array(
            'Menu source: '.$menuSource,
            'Minium Depth: 1',
            'Maximum Depth: '.$config['max_depth']
        ),
        'type' => 'ul',
        'attributes' => array()
    ));

    return $output;
}

function rs_panels_bootstrap_navbar_edit_form($form, &$form_state){
    $conf = $form_state['conf'];

    $form['menu'] = array(
        '#title' => 'Menu',
        '#description' => 'The menu chosen here will be used to populate the navbar',
        '#type' => 'select',
        '#options' => menu_get_menus(),
        '#required' => TRUE,
        '#default_value' => $conf['menu']
    );

    $form['max_depth'] = array(
        '#title' => 'Maximum Depth',
        '#description' => 'Maximum depth of menu links to render',
        '#type' => 'textfield',
        '#required' => TRUE,
        '#default_value' => (!empty($conf['max_depth'])) ? $conf['max_depth'] : 1,
    );

    $form['info'] = array(
        '#type' => 'fieldset',
        '#title' => 'Please note',
        'info_copy' => array(
            '#type' => 'markup',
            '#markup' => 'The following pane whilst structurally sound will only 
                work/display correctly in a bootstrapped environment, ie/ the 
                bootstrap css and javascript libraries have been included on the 
                page. Bootstrap can be downloaded from: 
                <a href="http://getbootstrap.com/">http://getbootstrap.com</a>'
        ),
    );

    return $form;
}

function rs_panels_bootstrap_navbar_edit_form_validate($form, &$form_state){
    if(!array_key_exists($form_state['values']['menu'], menu_get_menus())){
        form_set_error('menu', 'Invalid menu chosen');
    }
    if(!is_numeric($form_state['values']['max_depth']) || $form_state['values']['max_depth'] < 1){
        form_set_error('max_depth', 'Max depth must be numberic and not less than 1');
    }
}

function rs_panels_bootstrap_navbar_edit_form_submit($form, &$form_state){
    $form_state['conf']['menu'] = $form_state['values']['menu'];
    $form_state['conf']['max_depth'] = $form_state['values']['max_depth'];
}

function rs_panels_bootstrap_navbar_render_callback($subtype, $config, $args, $context){
    $activeTrail = array();
    foreach(menu_get_active_trail() as $key => $item){
        $activeTrail[$key] = isset($item['mlid']) ? $item['mlid'] : 0;
    }

    $menuOutput = menu_tree_output(menu_build_tree($config['menu'], array(
        'min_depth' => 1,
        'max_depth' => (int)$config['max_depth'],
        'active_trail' => $activeTrail,
    )));
    $menuOutput['#theme_wrappers'] = array();

    foreach(element_children($menuOutput) as $mlid){
        $menuOutput[$mlid]['#theme'] = 'bootstrap_navbar_dropdown';
    }

    $output = new stdClass();
    $output->content = render($menuOutput);

    return $output;
}

