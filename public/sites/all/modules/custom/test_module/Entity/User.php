<?php

namespace Test;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class User
 * @ORM\Entity @ORM\Table(name="users")
 */
class User {

    /**
     * @ORM\Id() @ORM\GeneratedValue() @ORM\Column(type="integer")
     * @var int
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    protected $name;

    /**
     * @ORM\Column(type="string")
     *
     * @var string
     */
    protected $brokerName;

    /**
     * @ORM\OneToMany(targetEntity="Test\Bug", mappedBy="reporter")
     * @var Bug[]
     */
    protected $reportedBugs = null;

    /**
     * @ORM\OneToMany(targetEntity="Test\Bug", mappedBy="engineer")
     * @var Bug[]
     */
    protected $assignedBugs = null;

    public function __construct(){
        $this->reportedBugs = new ArrayCollection();
        $this->assignedBugs = new ArrayCollection();
    }

    public function addReportedBug(Bug $bug){
        $this->reportedBugs[] = $bug;
    }

    public function assignedToBug(Bug $bug){
        $this->assignedBugs[] = $bug;
    }

    /**
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(){
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return User
     */
    public function setName($name){
        $this->name = $name;
        return $this;
    }



}