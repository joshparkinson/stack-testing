<?php

namespace Test;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Bug
 *
 * @ORM\Entity @ORM\Table(name="bugs")
 */
class Bug {

    /**
     * @ORM\Id() @ORM\Column(type="integer") @ORM\GeneratedValue()
     * @var int
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $description;

    /**
     * @ORM\Column(type="datetime")
     * @var DateTime
     */
    protected $created;

    /**
     * @ORM\Column(type="string")
     * @var string
     */
    protected $status;

    /**
     * @ORM\ManyToOne(targetEntity="Test\User", inversedBy="assignedBugs")
     *
     * @var \Test\User
     */
    protected $engineer;

    /**
     * @ORM\ManyToOne(targetEntity="Test\User", inversedBy="reportedBugs")
     *
     * @var \Test\User
     */
    protected $reporter;

    /**
     * @ORM\ManyToMany(targetEntity="Product")
     *
     * @var \Doctrine\Common\Collections\ArrayCollection
     */
    protected $products;

    public function __construct(){
        $this->products = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDescription(){
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return Bug
     */
    public function setDescription($description){
        $this->description = $description;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(){
        return $this->created;
    }

    /**
     * @param \DateTime $created
     *
     * @return Bug
     */
    public function setCreated(DateTime $created){
        $this->created = $created;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(){
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return Bug
     */
    public function setStatus($status){
        $this->status = $status;
        return $this;
    }

    public function getEngineer(){
        return $this->engineer;
    }

    public function setEngineer(User $engineer){
        $engineer->assignedToBug($this);
        $this->engineer = $engineer;
    }

    public function getReporter(){
        return $this->reporter;
    }

    public function setReporter(User $reporter){
        $reporter->addReportedBug($this);
        $this->reporter = $reporter;
    }

    public function assignToProduct(Product $product){
        $this->products[] = $product;
    }

    public function getProducts(){
        return $this->products;
    }




}