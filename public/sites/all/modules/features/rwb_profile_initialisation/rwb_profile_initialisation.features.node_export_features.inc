<?php
/**
 * @file
 * rwb_profile_initialisation.features.node_export_features.inc
 */

/**
 * Implements hook_node_export_features_default().
 */
function rwb_profile_initialisation_node_export_features_default() {
  $node_export = array(
  'code_string' => 'array(
  (object) array(
      \'body\' => array(
        \'und\' => array(
          array(
            \'value\' => "<p>\\r\\n\\tAccess denied…</p>\\r\\n\\r\\n<p>\\r\\n\\t<a href=\\"/\\">Click here to return to homepage</a></p>\\r\\n",
            \'summary\' => \'\',
            \'format\' => \'ckeditor\',
            \'safe_value\' => "<p>\\n\\tAccess denied…</p>\\n\\n<p>\\n\\t<a href=\\"/\\">Click here to return to homepage</a></p>\\n",
            \'safe_summary\' => \'\',
          ),
        ),
      ),
      \'changed\' => NULL,
      \'comment\' => \'0\',
      \'created\' => NULL,
      \'data\' => NULL,
      \'files\' => array(),
      \'language\' => \'und\',
      \'last_comment_timestamp\' => NULL,
      \'log\' => \'Edited by Josh Parkinson.\',
      \'menu\' => NULL,
      \'name\' => \'Anonymous\',
      \'nid\' => NULL,
      \'node_export_drupal_version\' => \'7\',
      \'path\' => NULL,
      \'picture\' => \'0\',
      \'promote\' => \'0\',
      \'revision_timestamp\' => NULL,
      \'revision_uid\' => 1,
      \'status\' => \'1\',
      \'sticky\' => \'0\',
      \'title\' => \'403 Access Denied\',
      \'tnid\' => \'0\',
      \'translate\' => \'0\',
      \'type\' => \'page\',
      \'uid\' => \'0\',
      \'uuid\' => \'5ade6c38-61c8-486d-b1f4-25dde111a9a6\',
      \'vid\' => NULL,
      \'vuuid\' => \'b25ca9da-ba39-4d3c-8bba-f99c75486a2b\',
    ),
  (object) array(
      \'body\' => array(
        \'und\' => array(
          array(
            \'value\' => "<p>The page you are looking for could not be found…</p>\\r\\n<a href=\\"/\\">Click here to return to homepage</a>",
            \'summary\' => \'\',
            \'format\' => \'ckeditor\',
            \'safe_value\' => "<p>The page you are looking for could not be found…</p>\\n<a href=\\"/\\">Click here to return to homepage</a>",
            \'safe_summary\' => \'\',
          ),
        ),
      ),
      \'changed\' => NULL,
      \'comment\' => \'0\',
      \'created\' => NULL,
      \'data\' => NULL,
      \'files\' => array(),
      \'language\' => \'und\',
      \'last_comment_timestamp\' => NULL,
      \'log\' => \'\',
      \'menu\' => NULL,
      \'name\' => \'Anonymous\',
      \'nid\' => NULL,
      \'node_export_drupal_version\' => \'7\',
      \'path\' => NULL,
      \'picture\' => \'0\',
      \'promote\' => \'0\',
      \'revision_timestamp\' => NULL,
      \'revision_uid\' => 1,
      \'status\' => \'1\',
      \'sticky\' => \'0\',
      \'title\' => \'404 Page Not Found\',
      \'tnid\' => \'0\',
      \'translate\' => \'0\',
      \'type\' => \'page\',
      \'uid\' => \'0\',
      \'uuid\' => \'83618074-5c6b-4bb1-9977-5e36b79ecfc2\',
      \'vid\' => NULL,
      \'vuuid\' => \'fa9094b2-b561-4eb2-9e94-f9e06e6adcae\',
    ),
)',
);
  return $node_export;
}
