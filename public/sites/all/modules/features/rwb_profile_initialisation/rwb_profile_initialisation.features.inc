<?php
/**
 * @file
 * rwb_profile_initialisation.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function rwb_profile_initialisation_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_default_views_alter().
 */
function rwb_profile_initialisation_views_default_views_alter(&$data) {
  if (isset($data['admin_views_user'])) {
    $data['admin_views_user']->display['default']->display_options['empty']['area_text_custom']['content'] = 'No users found.'; /* WAS: 'No users available.' */
    $data['admin_views_user']->display['default']->display_options['filters']['field_first_name_value'] = array(
      'id' => 'field_first_name_value',
      'table' => 'field_data_field_first_name',
      'field' => 'field_first_name_value',
      'operator' => 'contains',
      'group' => 1,
      'exposed' => TRUE,
      'expose' => array(
        'operator_id' => 'field_first_name_value_op',
        'label' => 'First name',
        'operator' => 'field_first_name_value_op',
        'identifier' => 'field_first_name_value',
        'remember' => TRUE,
        'remember_roles' => array(
          2 => 2,
          1 => 0,
          3 => 0,
        ),
      ),
    ); /* WAS: '' */
    $data['admin_views_user']->display['default']->display_options['filters']['field_last_name_value'] = array(
      'id' => 'field_last_name_value',
      'table' => 'field_data_field_last_name',
      'field' => 'field_last_name_value',
      'operator' => 'contains',
      'group' => 1,
      'exposed' => TRUE,
      'expose' => array(
        'operator_id' => 'field_last_name_value_op',
        'label' => 'Last name',
        'operator' => 'field_last_name_value_op',
        'identifier' => 'field_last_name_value',
        'remember' => TRUE,
        'remember_roles' => array(
          2 => 2,
          1 => 0,
          3 => 0,
        ),
      ),
    ); /* WAS: '' */
    unset($data['admin_views_user']->display['default']->display_options['fields']['views_bulk_operations']['vbo_operations']['action::system_block_ip_action']);
    unset($data['admin_views_user']->display['default']->display_options['fields']['views_bulk_operations']['vbo_operations']['action::system_goto_action']);
    unset($data['admin_views_user']->display['default']->display_options['fields']['views_bulk_operations']['vbo_operations']['action::system_message_action']);
    unset($data['admin_views_user']->display['default']->display_options['fields']['views_bulk_operations']['vbo_operations']['action::user_block_user_action']);
    unset($data['admin_views_user']->display['default']->display_options['fields']['views_bulk_operations']['vbo_operations']['action::views_bulk_operations_argument_selector_action']);
    unset($data['admin_views_user']->display['default']->display_options['fields']['views_bulk_operations']['vbo_operations']['action::views_bulk_operations_delete_item']);
    unset($data['admin_views_user']->display['default']->display_options['fields']['views_bulk_operations']['vbo_operations']['action::views_bulk_operations_script_action']);
    unset($data['admin_views_user']->display['default']->display_options['filters']['name']);
  }
}

/**
 * Implements hook_node_info().
 */
function rwb_profile_initialisation_node_info() {
  $items = array(
    'page' => array(
      'name' => t('Page'),
      'base' => 'node_content',
      'description' => t('Create a basic page.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
