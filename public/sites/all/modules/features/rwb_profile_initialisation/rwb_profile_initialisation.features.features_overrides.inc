<?php
/**
 * @file
 * rwb_profile_initialisation.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function rwb_profile_initialisation_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: views_view
  $overrides["views_view.admin_views_user.display|default|display_options|empty|area_text_custom|content"] = 'No users found.';
  $overrides["views_view.admin_views_user.display|default|display_options|fields|views_bulk_operations|vbo_operations|action::system_block_ip_action"]["DELETED"] = TRUE;
  $overrides["views_view.admin_views_user.display|default|display_options|fields|views_bulk_operations|vbo_operations|action::system_goto_action"]["DELETED"] = TRUE;
  $overrides["views_view.admin_views_user.display|default|display_options|fields|views_bulk_operations|vbo_operations|action::system_message_action"]["DELETED"] = TRUE;
  $overrides["views_view.admin_views_user.display|default|display_options|fields|views_bulk_operations|vbo_operations|action::user_block_user_action"]["DELETED"] = TRUE;
  $overrides["views_view.admin_views_user.display|default|display_options|fields|views_bulk_operations|vbo_operations|action::views_bulk_operations_argument_selector_action"]["DELETED"] = TRUE;
  $overrides["views_view.admin_views_user.display|default|display_options|fields|views_bulk_operations|vbo_operations|action::views_bulk_operations_delete_item"]["DELETED"] = TRUE;
  $overrides["views_view.admin_views_user.display|default|display_options|fields|views_bulk_operations|vbo_operations|action::views_bulk_operations_script_action"]["DELETED"] = TRUE;
  $overrides["views_view.admin_views_user.display|default|display_options|filters|field_first_name_value"] = array(
    'id' => 'field_first_name_value',
    'table' => 'field_data_field_first_name',
    'field' => 'field_first_name_value',
    'operator' => 'contains',
    'group' => 1,
    'exposed' => TRUE,
    'expose' => array(
      'operator_id' => 'field_first_name_value_op',
      'label' => 'First name',
      'operator' => 'field_first_name_value_op',
      'identifier' => 'field_first_name_value',
      'remember' => TRUE,
      'remember_roles' => array(
        2 => 2,
        1 => 0,
        3 => 0,
      ),
    ),
  );
  $overrides["views_view.admin_views_user.display|default|display_options|filters|field_last_name_value"] = array(
    'id' => 'field_last_name_value',
    'table' => 'field_data_field_last_name',
    'field' => 'field_last_name_value',
    'operator' => 'contains',
    'group' => 1,
    'exposed' => TRUE,
    'expose' => array(
      'operator_id' => 'field_last_name_value_op',
      'label' => 'Last name',
      'operator' => 'field_last_name_value_op',
      'identifier' => 'field_last_name_value',
      'remember' => TRUE,
      'remember_roles' => array(
        2 => 2,
        1 => 0,
        3 => 0,
      ),
    ),
  );
  $overrides["views_view.admin_views_user.display|default|display_options|filters|name"]["DELETED"] = TRUE;

 return $overrides;
}
